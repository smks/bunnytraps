BunnyTrap - Client
=====

Bunnytrap, a new real time game that allows players to play against each other by planting bunnies and traps on a 6x5 grid. If someone places a trap where you placed a bunny, the bunny gets catapulted towards the screen.

![Characters](bunnies.jpg)

## Character IDs
```
bunny 1 - momo (1,4)
bunny 2 - blueberry (1,1)
bunny 3 - melon (1,2)
bunny 4 - pear (2,1)
bunny 5 - strawberry (2,2)
bunny 6 - blackberry (1,3)
bunny 7 - watermelon (2,2)
bunny 8 - tangerine (2,3)
```

## Assets
https://drive.google.com/drive/folders/0B9kB68DXa5ndMTRZNXZSdGU5cWc

## Firebase
```javascript
<script src="https://www.gstatic.com/firebasejs/3.6.6/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAO14DZbnQR3GsSlhlu-b2a-tgqEFHIm9A",
    authDomain: "bunnytrap-9d685.firebaseapp.com",
    databaseURL: "https://bunnytrap-9d685.firebaseio.com",
    storageBucket: "bunnytrap-9d685.appspot.com",
    messagingSenderId: "917136247841"
  };
  firebase.initializeApp(config);
</script>
```

## Requirements

* [Node.js](http://www.nodejs.org)
* [http-server](https://www.npmjs.com/package/http-server)
* [TypeScript](http://www.typescriptlang.org/)
* [Gulp](http://gulpjs.com/)

## Subscribe to Firebase

<script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>

```
var config = {
    apiKey: "AIzaSyCmV47bt6pB6_ixV5okB3WKQrTgdpJ31hA",
    authDomain: "bunnybombs-cdfd2.firebaseapp.com",
    databaseURL: "https://bunnybombs-cdfd2.firebaseio.com",
    storageBucket: "bunnybombs-cdfd2.appspot.com",
    messagingSenderId: "978640014563"
};
firebase.initializeApp(config);
```