<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.1</string>
        <key>fileName</key>
        <string>A:/Development/Web/bunnytraps/src/assets/Bunnies.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename>Bunnies.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>Bunnies.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">single/bunnies/1/body.png</key>
            <key type="filename">single/bunnies/7/body.png</key>
            <key type="filename">single/bunnies/8/body.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>163,128,326,255</rect>
                <key>scale9Paddings</key>
                <rect>163,128,326,255</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/brow.png</key>
            <key type="filename">single/bunnies/2/brow.png</key>
            <key type="filename">single/bunnies/3/brow.png</key>
            <key type="filename">single/bunnies/4/brow.png</key>
            <key type="filename">single/bunnies/5/brow.png</key>
            <key type="filename">single/bunnies/6/brow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,6,24,11</rect>
                <key>scale9Paddings</key>
                <rect>12,6,24,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/cheek.png</key>
            <key type="filename">single/bunnies/2/cheek.png</key>
            <key type="filename">single/bunnies/3/cheek.png</key>
            <key type="filename">single/bunnies/4/cheek.png</key>
            <key type="filename">single/bunnies/5/cheek.png</key>
            <key type="filename">single/bunnies/6/cheek.png</key>
            <key type="filename">single/bunnies/7/cheek.png</key>
            <key type="filename">single/bunnies/8/cheek.png</key>
            <key type="filename">single/bunnies/8/face.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,9,192,17</rect>
                <key>scale9Paddings</key>
                <rect>96,9,192,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/ear.png</key>
            <key type="filename">single/bunnies/7/ear.png</key>
            <key type="filename">single/bunnies/8/ear.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,88,95,176</rect>
                <key>scale9Paddings</key>
                <rect>48,88,95,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/face.png</key>
            <key type="filename">single/bunnies/2/face.png</key>
            <key type="filename">single/bunnies/3/face.png</key>
            <key type="filename">single/bunnies/4/face.png</key>
            <key type="filename">single/bunnies/5/face.png</key>
            <key type="filename">single/bunnies/6/face.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,3,58,5</rect>
                <key>scale9Paddings</key>
                <rect>29,3,58,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/foot.png</key>
            <key type="filename">single/bunnies/7/foot.png</key>
            <key type="filename">single/bunnies/8/foot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,48,71,96</rect>
                <key>scale9Paddings</key>
                <rect>36,48,71,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/1/paw.png</key>
            <key type="filename">single/bunnies/8/paw.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,22,32,43</rect>
                <key>scale9Paddings</key>
                <rect>16,22,32,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/2/body.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>652,510,1304,1021</rect>
                <key>scale9Paddings</key>
                <rect>652,510,1304,1021</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/2/ear.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>190,351,379,701</rect>
                <key>scale9Paddings</key>
                <rect>190,351,379,701</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/2/foot.png</key>
            <key type="filename">single/bunnies/3/foot.png</key>
            <key type="filename">single/bunnies/4/foot.png</key>
            <key type="filename">single/bunnies/5/foot.png</key>
            <key type="filename">single/bunnies/6/foot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,15,22,29</rect>
                <key>scale9Paddings</key>
                <rect>11,15,22,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/2/paw.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,86,126,172</rect>
                <key>scale9Paddings</key>
                <rect>63,86,126,172</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/3/body.png</key>
            <key type="filename">single/bunnies/4/body.png</key>
            <key type="filename">single/bunnies/5/body.png</key>
            <key type="filename">single/bunnies/6/body.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,38,98,77</rect>
                <key>scale9Paddings</key>
                <rect>49,38,98,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/3/ear.png</key>
            <key type="filename">single/bunnies/4/ear.png</key>
            <key type="filename">single/bunnies/5/ear.png</key>
            <key type="filename">single/bunnies/6/ear.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,26,29,53</rect>
                <key>scale9Paddings</key>
                <rect>14,26,29,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/3/paw.png</key>
            <key type="filename">single/bunnies/4/paw.png</key>
            <key type="filename">single/bunnies/5/paw.png</key>
            <key type="filename">single/bunnies/6/paw.png</key>
            <key type="filename">single/bunnies/7/paw.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/7/brow.png</key>
            <key type="filename">single/bunnies/8/brow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,19,80,37</rect>
                <key>scale9Paddings</key>
                <rect>40,19,80,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/7/face.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,50,157,100</rect>
                <key>scale9Paddings</key>
                <rect>78,50,157,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/Sweat1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,33,87,66</rect>
                <key>scale9Paddings</key>
                <rect>44,33,87,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/Sweat2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,30,106,60</rect>
                <key>scale9Paddings</key>
                <rect>53,30,106,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/Sweat3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,47,141,93</rect>
                <key>scale9Paddings</key>
                <rect>70,47,141,93</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/eyes-closed.png</key>
            <key type="filename">single/bunnies/eyes-happy.png</key>
            <key type="filename">single/bunnies/eyes-hit.png</key>
            <key type="filename">single/bunnies/eyes-open.png</key>
            <key type="filename">single/bunnies/eyes-sad.png</key>
            <key type="filename">single/bunnies/eyes-worry.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,57,253,115</rect>
                <key>scale9Paddings</key>
                <rect>126,57,253,115</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/eyes-roll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,134,253,267</rect>
                <key>scale9Paddings</key>
                <rect>126,134,253,267</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">single/bunnies/mouth-closed.png</key>
            <key type="filename">single/bunnies/mouth-fear-1.png</key>
            <key type="filename">single/bunnies/mouth-fear-2.png</key>
            <key type="filename">single/bunnies/mouth-hit-2.png</key>
            <key type="filename">single/bunnies/mouth-hit.png</key>
            <key type="filename">single/bunnies/mouth-open.png</key>
            <key type="filename">single/bunnies/mouth-panic-closed.png</key>
            <key type="filename">single/bunnies/mouth-panic.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>127,83,253,165</rect>
                <key>scale9Paddings</key>
                <rect>127,83,253,165</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>single/bunnies</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
