/**
 * Created by shaun on 11/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Signal = Phaser.Signal;
import Bunny from "../components/Bunny/Sprite/Bunny";
import Group = Phaser.Group;
import Assets from "../Assets";

export default class BunnyPlacer implements Placer {
    private game: Game;
    private sprite: Sprite;
    private pressedTile: Signal;
    private bunnyPlacementComplete: Signal;
    private bunnyId: number;
    private bunnies = [];
    private placements = [];
    private currentIndex = 0;

    constructor(game: Game, pressedTile: Signal, bunnyId: number, bunnyPlacementComplete?: Signal) {
        this.game = game;
        this.pressedTile = pressedTile;
        this.bunnyId = bunnyId;
        this.bunnyPlacementComplete = bunnyPlacementComplete;
    }

    listen() {
        this.pressedTile.add(this.addBunny, this);
    }

    hasPlantedAll() {
        return this.placements.length == 5;
    }

    getPlantedCoordinates() {
        return this.placements;
    }

    addBunny(options) {

        const {columnIndex, rowIndex, tileXPos, tileYPos} = options;

        if (this.hasAlreadyPlacedHere(columnIndex, rowIndex)) {
            console.log('Placed here already!');
            return;
        }

        if (this.bunnies.length > 4) {
            this.moveExisting(tileXPos, tileYPos, columnIndex, rowIndex);
            return;
        }

        this.createNewBunny(tileXPos, tileYPos, columnIndex, rowIndex);

        if (this.bunnies.length > 4 && this.bunnyPlacementComplete) {
            this.bunnyPlacementComplete.dispatch(this.placements);
        }
    }

    private createNewBunny(tileXPos: any, tileYPos: any, columnIndex: any, rowIndex: any) {
        const bunny = new Bunny(this.game, this.bunnyId);
        bunny.create(tileXPos, (tileYPos - 10), 0.2);
        bunny.dropDown(() => {
            bunny.standAnimation(true);
        });

        this.bunnies.push(bunny);
        this.placements.push({columnIndex, rowIndex});
    }

    private moveExisting(tileXPos: any, tileYPos: any, columnIndex: any, rowIndex: any) {

        const bunny: Bunny = this.bunnies[this.currentIndex];
        const bunnySprite: Group = bunny.getSprite();
        bunnySprite.x = tileXPos;
        bunnySprite.y = (tileYPos - 10);

        this.game.world.bringToTop(bunnySprite);

        bunny.dropDown(() => {
            bunny.standAnimation();
        })

        this.placements[this.currentIndex].columnIndex = columnIndex;
        this.placements[this.currentIndex].rowIndex = rowIndex;
        this.currentIndex++;
        if (this.currentIndex > 4) {
            this.currentIndex = 0;
        }
    }

    ignore() {
        this.pressedTile.remove(this.addBunny, this);
    }

    destroy() {
        this.sprite.destroy(true);
    }

    hasAlreadyPlacedHere(columnIndex: number, rowIndex: number) {

        for (let i = 0; i < this.placements.length; i++) {
            const placement = this.placements[i];
            const placementColumn = placement.columnIndex;
            const placementRow = placement.rowIndex;
            if (columnIndex === placementColumn && placementRow === rowIndex) {
                return true;
            }
        }

        return false;
    }

    shootBunnyAway(onHit: Function, onComplete: Function) {
        if (this.bunnies.length > 0) {
            const bunny: Bunny = this.bunnies.pop();
            bunny.shootToScreen(onHit, onComplete);
        }
    }

    survived() {
        if (this.bunnies.length > 0) {
            const bunny: Bunny = this.bunnies.pop();

            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_CROWD_CHEERING, 1, false)
                .play();

            bunny.jumpFade(() => {
                bunny.destroy();
            });
        }
    }

    clear() {
        this.bunnies.map((bunny: Bunny) => {
            bunny.jumpFade(() => {
                bunny.destroy();
            });
        })
    }
}