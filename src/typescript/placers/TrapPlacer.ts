/**
 * Created by shaun on 11/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Signal = Phaser.Signal;
import Group = Phaser.Group;
import Assets from "../Assets";

export default class TrapPlacer implements Placer {
    private game: Game;
    private sprite: Sprite;
    private pressedTile: Signal;
    private trapPlacementComplete: Signal;
    private traps = [];
    private placements = [];
    private currentIndex = 0;

    constructor(game: Game, pressedTile: Signal,  trapPlacementComplete?: Signal) {
        this.game = game;
        this.pressedTile = pressedTile;
        this.trapPlacementComplete = trapPlacementComplete;
    }

    listen() {
        this.pressedTile.add(this.addTrap, this);
    }

    hasPlantedAll() {
        return this.placements.length == 5;
    }

    getPlantedCoordinates() {
        return this.placements;
    }

    addTrap(options) {

        const {columnIndex, rowIndex, tileXPos, tileYPos} = options;

        if (this.hasAlreadyPlacedHere(columnIndex, rowIndex)) {
            console.warn('Placed here already!');
            return;
        }

        if (this.traps.length > 4) {
            this.moveExisting(tileXPos, tileYPos, columnIndex, rowIndex);
            return;
        }

        this.createNewTrap(tileXPos, tileYPos, columnIndex, rowIndex);

        if (this.traps.length > 4 && this.trapPlacementComplete) {
            this.trapPlacementComplete.dispatch(this.placements);
        }
    }

    private createNewTrap(tileXPos: any, tileYPos: any, columnIndex: any, rowIndex: any) {
        const trap = this.game.add.sprite(tileXPos, tileYPos, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_BUNNIES_GRID_CROSS);
        trap.anchor.set(0.5, 0.5);
        this.animateIn(trap);
        this.traps.push(trap);
        this.placements.push({columnIndex, rowIndex});
        this.playSound();
    }

    private moveExisting(tileXPos: any, tileYPos: any, columnIndex: any, rowIndex: any) {

        const trap: Sprite = this.traps[this.currentIndex];
        trap.x = tileXPos;
        trap.y = (tileYPos - 0);

        this.animateIn(trap);

        this.playSound();

        this.placements[this.currentIndex].columnIndex = columnIndex;
        this.placements[this.currentIndex].rowIndex = rowIndex;
        this.currentIndex++;

        if (this.currentIndex > 4) {
            this.currentIndex = 0;
        }
    }

    ignore() {
        this.pressedTile.remove(this.addTrap, this);
    }

    destroy() {
        this.sprite.destroy(true);
    }

    hasAlreadyPlacedHere(columnIndex: number, rowIndex: number) {

        for (let i = 0; i < this.placements.length; i++) {
            const placement = this.placements[i];
            const placementColumn = placement.columnIndex;
            const placementRow = placement.rowIndex;
            if (columnIndex === placementColumn && placementRow === rowIndex) {
                return true;
            }
        }

        return false;
    }

    private animateIn(trap: Phaser.Sprite) {
        trap.scale.set(3, 3);
        this.game.add.tween(trap.scale)
            .to({x: 1, y: 1}, 1000, Phaser.Easing.Elastic.Out, true);

        trap.alpha = 0;
        this.game.add.tween(trap)
            .to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
    }

    clear() {
        this.traps.map((trap: Sprite) => {

            trap.scale.set(1, 1);
            const tween = this.game.add.tween(trap.scale)
                .to({x: 1.5, y: 1.5}, 250, Phaser.Easing.Linear.None, false);

            tween.onComplete.add(() => {
                trap.destroy(true);
            })

            trap.alpha = 1;
            this.game.add.tween(trap)
                .to({alpha: 0}, 250, Phaser.Easing.Linear.None, true);

            tween.start();
        })
    }

    private playSound() {
        this.game.add.sound(Assets.AUDIO_KEY_AUDIO_SET_TRAP, 1, false)
            .play();
    }
}