export default class Assets {

    private static assetAtlasPath: string = 'assets/';

    // Lobby Sprite-sheet
    public static ATLAS_LOBBY: string = Assets.assetAtlasPath + 'Lobby.png';
    public static ATLAS_LOBBY_JSON: string = Assets.assetAtlasPath + 'Lobby.json';

    // Game Sprite-sheet
    public static ATLAS_GAME: string = Assets.assetAtlasPath + 'Game.png';
    public static ATLAS_GAME_JSON: string = Assets.assetAtlasPath + 'Game.json';

    // Bunny Sprite-sheet
    public static ATLAS_BUNNIES: string = Assets.assetAtlasPath + 'Bunnies.png';
    public static ATLAS_BUNNIES_JSON: string = Assets.assetAtlasPath + 'Bunnies.json';

    // Bush Sprite-sheet
    public static ATLAS_BUSH: string = Assets.assetAtlasPath + 'Bush.png';
    public static ATLAS_BUSH_JSON: string = Assets.assetAtlasPath + 'Bush.json';

    // Backgrounds
    public static LOADING_BG: string = Assets.assetAtlasPath + 'backgrounds/loading.jpg';
    public static LOBBY_BG: string = Assets.assetAtlasPath + 'backgrounds/lobby.png';
    public static GAME_BG: string = Assets.assetAtlasPath + 'backgrounds/game.jpg';

    // Graphics
    public static LOBBY_ASSETS = 'lobby';
    public static GAME_ASSETS = 'game';
    public static BUNNIES_ASSETS = 'bunnies';
    public static BUSH_ASSETS = 'bush';


    // Music
    public static AUDIO_MAIN_THEME_01 = Assets.assetAtlasPath + 'audio/Main_Theme_01.mp3';
    public static AUDIO_IN_GAME_01 = Assets.assetAtlasPath + 'audio/In_Game_01.mp3';
    public static AUDIO_THEME_02 = Assets.assetAtlasPath + 'audio/Theme_02.mp3';
    public static AUDIO_AMBIENCE_LOOP = Assets.assetAtlasPath + 'audio/Ambience_Loop.wav';
    public static AUDIO_BUNNY_DROP = Assets.assetAtlasPath + 'audio/Bunny_Drop.wav';
    public static AUDIO_SET_TRAP = Assets.assetAtlasPath + 'audio/Set_Trap.wav';
    public static AUDIO_BUNNY_HITS_SCREEN = Assets.assetAtlasPath + 'audio/Bunny_Hits_Screen.wav';
    public static AUDIO_BUNNY_SLIDE = Assets.assetAtlasPath + 'audio/Bunny_Slide.wav';
    public static AUDIO_BUNNY_SOUND_01 = Assets.assetAtlasPath + 'audio/Bunny_Sound_01.wav';
    public static AUDIO_BUNNY_SOUND_02 = Assets.assetAtlasPath + 'audio/Bunny_Sound_02.wav';
    public static AUDIO_BUNNY_SOUND_03 = Assets.assetAtlasPath + 'audio/Bunny_Sound_03.wav';
    public static AUDIO_BUNNY_SOUND_04 = Assets.assetAtlasPath + 'audio/Bunny_Sound_04.wav';
    public static AUDIO_BUNNY_SOUND_05 = Assets.assetAtlasPath + 'audio/Bunny_Sound_05.wav';
    public static AUDIO_BUNNY_FLYING_01 = Assets.assetAtlasPath + 'audio/Bunny_Flying_01.wav';
    public static AUDIO_BUNNY_FLYING_02 = Assets.assetAtlasPath + 'audio/Bunny_Flying_02.wav';
    public static AUDIO_BUNNY_FLYING_03 = Assets.assetAtlasPath + 'audio/Bunny_Flying_03.wav';
    public static AUDIO_BUNNY_FLYING_04 = Assets.assetAtlasPath + 'audio/Bunny_Flying_04.wav';
    public static AUDIO_BUNNY_FLYING_05 = Assets.assetAtlasPath + 'audio/Bunny_Flying_05.wav';
    public static AUDIO_BUSH_COLLIDE = Assets.assetAtlasPath + 'audio/Bush_Collide.wav';
    public static AUDIO_BUSH_SEPARATE = Assets.assetAtlasPath + 'audio/Bush_Separate.wav';
    public static AUDIO_BUTTON_PRESS = Assets.assetAtlasPath + 'audio/Button_Press.wav';
    public static AUDIO_CHANGE_PLAYER = Assets.assetAtlasPath + 'audio/Change_Player.wav';
    public static AUDIO_CLOCK_TICK = Assets.assetAtlasPath + 'audio/Clock_Tick.wav';
    public static AUDIO_CROWD_CHEERING = Assets.assetAtlasPath + 'audio/Crowd_Cheering.wav';
    public static AUDIO_LOG_APPEAR = Assets.assetAtlasPath + 'audio/Log_Appear.wav';
    public static AUDIO_NEW_MESSAGE = Assets.assetAtlasPath + 'audio/New_Message.wav';
    public static AUDIO_REVEAL_CARD = Assets.assetAtlasPath + 'audio/Reveal_Card.wav';
    public static AUDIO_TILE_LAND = Assets.assetAtlasPath + 'audio/Tile_Land.wav';
    public static AUDIO_TILE_SPRING = Assets.assetAtlasPath + 'audio/Tile_Spring.wav';
    public static AUDIO_TIME_UP = Assets.assetAtlasPath + 'audio/Time_Up.wav';

    public static AUDIO_KEY_AUDIO_MAIN_THEME_01 = 'audio_main_theme_01';
    public static AUDIO_KEY_AUDIO_IN_GAME_01 = 'audio_in_game_01';
    public static AUDIO_KEY_AUDIO_THEME_02 = 'audio_theme_02';
    public static AUDIO_KEY_AUDIO_AMBIENCE_LOOP = 'audio_ambience_loop';
    public static AUDIO_KEY_AUDIO_BUNNY_DROP = 'audio_bunny_drop';
    public static AUDIO_KEY_AUDIO_SET_TRAP = 'audio_set_trap';
    public static AUDIO_KEY_AUDIO_BUNNY_HITS_SCREEN = 'audio_bunny_hits_screen';
    public static AUDIO_KEY_AUDIO_BUNNY_SLIDE = 'audio_bunny_slide';
    public static AUDIO_KEY_AUDIO_BUNNY_SOUND_01 = 'audio_bunny_sound_01';
    public static AUDIO_KEY_AUDIO_BUNNY_SOUND_02 = 'audio_bunny_sound_02';
    public static AUDIO_KEY_AUDIO_BUNNY_SOUND_03 = 'audio_bunny_sound_03';
    public static AUDIO_KEY_AUDIO_BUNNY_SOUND_04 = 'audio_bunny_sound_04';
    public static AUDIO_KEY_AUDIO_BUNNY_SOUND_05 = 'audio_bunny_sound_05';
    public static AUDIO_KEY_AUDIO_BUNNY_FLYING_01 = 'audio_bunny_flying_01';
    public static AUDIO_KEY_AUDIO_BUNNY_FLYING_02 = 'audio_bunny_flying_02';
    public static AUDIO_KEY_AUDIO_BUNNY_FLYING_03 = 'audio_bunny_flying_03';
    public static AUDIO_KEY_AUDIO_BUNNY_FLYING_04 = 'audio_bunny_flying_04';
    public static AUDIO_KEY_AUDIO_BUNNY_FLYING_05 = 'audio_bunny_flying_05';
    public static AUDIO_KEY_AUDIO_BUSH_COLLIDE = 'audio_bush_collide';
    public static AUDIO_KEY_AUDIO_BUSH_SEPARATE = 'audio_bush_separate';
    public static AUDIO_KEY_AUDIO_BUTTON_PRESS = 'audio_button_press';
    public static AUDIO_KEY_AUDIO_CHANGE_PLAYER = 'audio_change_player';
    public static AUDIO_KEY_AUDIO_CLOCK_TICK = 'audio_clock_tick';
    public static AUDIO_KEY_AUDIO_CROWD_CHEERING = 'audio_crowd_cheering';
    public static AUDIO_KEY_AUDIO_LOG_APPEAR = 'audio_log_appear';
    public static AUDIO_KEY_AUDIO_NEW_MESSAGE = 'audio_new_message';
    public static AUDIO_KEY_AUDIO_REVEAL_CARD = 'audio_reveal_card';
    public static AUDIO_KEY_AUDIO_TILE_LAND = 'audio_tile_land';
    public static AUDIO_KEY_AUDIO_TILE_SPRING = 'audio_tile_spring';
    public static AUDIO_KEY_AUDIO_TIME_UP = 'audio_time_up';

    public static GRAPHIC_BUNNIES_BUNNIES_1 = 'bunnies/1/bunny.png';
    public static GRAPHIC_BUNNIES_BUNNIES_2 = 'bunnies/2/bunny.png';
    public static GRAPHIC_BUNNIES_BUNNIES_3 = 'bunnies/3/bunny.png';
    public static GRAPHIC_BUNNIES_BUNNIES_4 = 'bunnies/4/bunny.png';

    // Bunny Body Parts

    // Eyes
    public static GRAPHIC_BUNNIES_BUNNY_EYES_HIT = 'eyes-hit.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_OPEN = 'eyes-open.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_HAPPY = 'eyes-happy.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_CLOSED = 'eyes-closed.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_ROLL = 'eyes-roll.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_SAD = 'eyes-sad.png';
    public static GRAPHIC_BUNNIES_BUNNY_EYES_WORRY = 'eyes-worry.png';

    // Mouth
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_OPEN = 'mouth-open.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_CLOSED = 'mouth-closed.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC = 'mouth-panic.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC_CLOSED = 'mouth-panic-closed.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_1 = 'mouth-fear-1.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_2 = 'mouth-fear-2.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_1 = 'mouth-hit.png';
    public static GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_2 = 'mouth-hit-2.png';

    public static GRAPHIC_BUNNIES_BUNNY_1_BODY = '1/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_1_FACE = '1/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_1_EAR = '1/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_1_EYEBROW = '1/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_1_PAW = '1/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_1_FOOT = '1/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_2_BODY = '2/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_2_FACE = '2/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_2_EAR = '2/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_2_EYEBROW = '2/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_2_PAW = '2/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_2_FOOT = '2/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_3_BODY = '3/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_3_FACE = '3/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_3_EAR = '3/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_3_EYEBROW = '3/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_3_PAW = '3/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_3_FOOT = '3/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_4_BODY = '4/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_4_FACE = '4/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_4_EAR = '4/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_4_EYEBROW = '4/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_4_PAW = '4/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_4_FOOT = '4/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_5_BODY = '5/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_5_FACE = '5/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_5_EAR = '5/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_5_EYEBROW = '5/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_5_PAW = '5/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_5_FOOT = '5/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_6_BODY = '6/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_6_FACE = '6/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_6_EAR = '6/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_6_EYEBROW = '6/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_6_PAW = '6/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_6_FOOT = '6/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_7_BODY = '7/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_7_FACE = '7/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_7_EAR = '7/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_7_EYEBROW = '7/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_7_PAW = '7/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_7_FOOT = '7/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_8_BODY = '8/body.png';
    public static GRAPHIC_BUNNIES_BUNNY_8_FACE = '8/face.png';
    public static GRAPHIC_BUNNIES_BUNNY_8_EAR = '8/ear.png';
    public static GRAPHIC_BUNNIES_BUNNY_8_EYEBROW = '8/eyebrow.png';
    public static GRAPHIC_BUNNIES_BUNNY_8_PAW = '8/paw.png';
    public static GRAPHIC_BUNNIES_BUNNY_8_FOOT = '8/foot.png';

    public static GRAPHIC_BUNNIES_BUNNY_SWEAT = 'sweat-1.png';
    public static GRAPHIC_BUNNIES_BUNNY_SWEAT_2 = 'sweat-2.png';
    public static GRAPHIC_BUNNIES_BUNNY_SWEAT_3 = 'sweat-3.png';

    // Bush
    public static GRAPHIC_BUSH: string = 'bush.png';
    public static GRAPHIC_BUSH_LEAF: string = 'bush-leaf-1.png';

    // Lobby
    public static GRAPHIC_LOBBY_BG = 'bg.jpg';
    public static GRAPHIC_LOBBY_READY_BUTTON = 'button/{id}.png';
    public static GRAPHIC_LOBBY_READY_BUTTON_ACTIVE = 'button/ready/{id}.png';
    public static GRAPHIC_LOBBY_LOBBY_STATUS_BAR = 'lobby-status-bar/bar.png';
    public static GRAPHIC_LOBBY_LOGO = 'logo.png';

    // Game
    public static GRAPHIC_GAME_BUNNY_CARD = 'bunny-card/card.png';
    public static GRAPHIC_GAME_BUTTONS_LARGE = 'buttons/large.png';
    public static GRAPHIC_GAME_BUTTONS_RESULTS = 'buttons/results.png';
    public static GRAPHIC_GAME_BUTTONS_SMALL = 'buttons/small.png';
    public static GRAPHIC_GAME_BUTTONS_SMALL_EXIT = 'buttons/exit.png';
    public static GRAPHIC_GAME_BUTTONS_SMALL_FULLSCREEN = 'buttons/fullscreen.png';
    public static GRAPHIC_GAME_BUTTONS_SMALL_SOUND = 'buttons/sound.png';
    public static GRAPHIC_GAME_GRID_TILE = 'grid/tile.png';
    public static GRAPHIC_GAME_BUNNIES_GRID_CROSS = 'grid/cross.png';
    public static GRAPHIC_GAME_INSTRUCTIONS_BANNER = 'instructions-banner/banner.png';
    public static GRAPHIC_GAME_LOGS_LOG = 'logs/log.png';
    public static GRAPHIC_GAME_TIMER_INDICATOR = 'timer/bar.png';
    public static GRAPHIC_GAME_POINTS_INDICATOR = 'points-bar/points-bar.png';
    public static GRAPHIC_GAME_GLASS_1 = 'glass/1.png';
    public static GRAPHIC_GAME_GLASS_2 = 'glass/2.png';
    public static GRAPHIC_GAME_GLASS_3 = 'glass/3.png';

    public static GRAPHIC_PLAYER_1_BUNNY: string = '';
    public static GRAPHIC_PLAYER_2_BUNNY: string = '';
    public static GRAPHIC_PLAYER_3_BUNNY: string = '';
    public static GRAPHIC_PLAYER_4_BUNNY: string = '';

    public static GRAPHIC_PLAY_BUTTON: string = 'play-now-button.png';
    public static GRAPHIC_PLAY_BUTTON_HOVER: string = 'play-now-button-over.png';

    // Fonts
    public static FONT: string = 'baseFont';
    public static FONT_INVERSE: string = 'baseFontInverse';
    public static FONT_LIGHT: string = 'baseFontLight';
    public static FONT_BUTTONS: string = 'buttonFont';
    public static FONT_LOGS: string = 'logFont';

    // Audio
}