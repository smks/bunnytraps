
import bunnyIdByPlayer from '../helpers/bunny-id-by-player';

/**
 * Created by shaun on 14/01/2017.
 */
export default class ResultsMoveBuilder {
    private results: {player1Results: {bunnyPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]; trapPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]}; player2Results: {bunnyPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]; trapPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]}; player3Results: {bunnyPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]; trapPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]}; player4Results: {bunnyPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]; trapPlacements: [{columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}, {columnIndex: number; rowIndex: number}]}};
    private player1Results: {playerId; bunnyPlacements; trapPlacements};
    private player2Results: {playerId; bunnyPlacements; trapPlacements};
    private player3Results?: {playerId; bunnyPlacements; trapPlacements};
    private player4Results?: {playerId; bunnyPlacements; trapPlacements};
    private moves: Array<{playerId; bunnyId; columnIndex; rowIndex; gotHitByPlayers: Array<number>}>;
    private numberOfPlayers: number =  2;
    private currentPlayerIndex = 1;

    constructor(results) {
        this.results = results;
        this.player1Results = results.player1Results;
        this.player2Results = results.player2Results;
        if (results.player3Results) {
            this.numberOfPlayers++;
            this.player3Results = results.player3Results;
        }

        if (results.player4Results) {
            this.numberOfPlayers++;
            this.player4Results = results.player4Results;
        }

        this.moves = [];
    }

    createMoves() {
        const placements = this.results[`player${this.numberOfPlayers}Results`].bunnyPlacements;
        const lengthOfResults = placements.length;

        while (placements.length > 0) {
            
            for (let i = 0; i < lengthOfResults; i++) {

                if (this.currentPlayerIndex > this.numberOfPlayers) {
                    this.currentPlayerIndex = 1;
                }

                const bunnyPositions = this[`player${this.currentPlayerIndex}Results`].bunnyPlacements;

                if (bunnyPositions.length == 0) {
                    continue;
                }

                const bunnyPosition:{columnIndex: number, rowIndex: number} = bunnyPositions.shift();

                let whoTrappedBunny = [];

                if (bunnyPosition) {
                    whoTrappedBunny = this.findPlayersWhoPlacedTrap(bunnyPosition);
                }

                const move = {
                    playerId: this.currentPlayerIndex,
                    bunnyId: bunnyIdByPlayer(this.currentPlayerIndex),
                    columnIndex: bunnyPosition.columnIndex,
                    rowIndex: bunnyPosition.rowIndex,
                    gotHitByPlayers: whoTrappedBunny
                }

                this.moves.push(move);

                this.currentPlayerIndex++;
            }
        }

        console.log(placements.length);
        console.log(this.moves);
    }

    getMoves() {
        return this.moves;
    }
    
    private findPlayersWhoPlacedTrap(bunnyPosition: {columnIndex: number; rowIndex: number}) {
        const playersWhoTrappedBunny = [];
        for (let key in this.results) {
            if (!this.results.hasOwnProperty(key)) {
                continue;
            }

            const playerDecisions = this.results[key];

            const playerId = playerDecisions.playerId;
            const trapPlacements = playerDecisions.trapPlacements;

            trapPlacements.map((trapPosition: {columnIndex; rowIndex}) => {
                if (
                    trapPosition.columnIndex == bunnyPosition.columnIndex &&
                    trapPosition.rowIndex == bunnyPosition.rowIndex)
                {
                    playersWhoTrappedBunny.push(playerId);
                }
            })
        }

        console.log(`Bunny Position: col - ${bunnyPosition.columnIndex} row - ${bunnyPosition.rowIndex}`);
        console.log('playersWhoTrappedBunny');
        console.log(playersWhoTrappedBunny);

        return playersWhoTrappedBunny;
    }
}