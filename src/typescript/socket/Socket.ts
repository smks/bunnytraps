/// <reference path='../../../tsDefinitions\socket.io-client\socket.io-client.d.ts' />

import * as socket from 'socket.io-client';
import SocketEvents from './SocketEvents';

class Socket {

    // Events to listen for
    public onLobbyJoin:Phaser.Signal = new Phaser.Signal();
    public onRoomJoined:Phaser.Signal = new Phaser.Signal();
    public onGameStarted:Phaser.Signal = new Phaser.Signal();

    public onPlayerReady:Phaser.Signal = new Phaser.Signal();
    public onPlacedBunny:Phaser.Signal = new Phaser.Signal();
    public onPlacedTrap:Phaser.Signal = new Phaser.Signal();
    public onPlacementComplete:Phaser.Signal = new Phaser.Signal();
    public onRoomFinished:Phaser.Signal = new Phaser.Signal();
    public onRoundComplete:Phaser.Signal = new Phaser.Signal();
    public onGameSessionComplete:Phaser.Signal = new Phaser.Signal();
    public onDisconnect:Phaser.Signal = new Phaser.Signal();
    public onEnteredLobby:Phaser.Signal = new Phaser.Signal();

    private socket;

    constructor() {
        this.socket = this.initSocket();
    }

    init() {
        this.socket.on(SocketEvents.LOBBY_JOIN, (data) => {
            console.log('socket event player joined ' + data.username);
            this.onLobbyJoin.dispatch(data);
        });
        this.socket.on(SocketEvents.ROOM_JOIN, (data) => {
            console.log('socket event player joined ' + data.username);
            this.onRoomJoined.dispatch(data);
        });
        this.socket.on(SocketEvents.GAME_START, (data) => {
            console.log('game started');
            this.onGameStarted.dispatch(data);
        });
        this.socket.on(SocketEvents.PLAYER_READY, (data) => {
            console.log('player ready');
            this.onPlayerReady.dispatch(data);
        });
        this.socket.on(SocketEvents.PLACEMENT_BUNNY, (data) => {
            this.onPlacedBunny.dispatch(data);
        });
        this.socket.on(SocketEvents.PLACEMENT_TRAP, (data) => {
            this.onPlacedTrap.dispatch(data);
        });
        this.socket.on(SocketEvents.PLACEMENT_COMPLETE, (data) => {
            this.onPlacementComplete.dispatch(data);
        });
        this.socket.on(SocketEvents.ROOM_FINISHED, (data) => {
            this.onRoomFinished.dispatch(data);
        });
        this.socket.on(SocketEvents.ROUND_COMPLETE, (data) => {
            this.onRoundComplete.dispatch(data);
        });
        this.socket.on(SocketEvents.GAME_COMPLETE, (data) => {
            this.onGameSessionComplete.dispatch(data);
        });
        this.socket.on(SocketEvents.DISCONNECT, (data) => {
            this.onDisconnect.dispatch(data);
        });
    }

    initSocket() {
        return socket('localhost:5000');
    }

    /**
     *
     * @param event
     * @param data
     */
    emit(event, data) {
        console.log('sending event: ' + event);
        this.socket.emit(event, data);
    }
};

const gameSocket = new Socket();

export default gameSocket;