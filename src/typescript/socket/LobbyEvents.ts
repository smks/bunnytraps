import Game = Phaser.Game;
import Socket from '../socket/Socket';
import SocketEvents from "./SocketEvents";

class LobbyEvents {
    private game: Game;

    constructor(game: Game) {
        this.game = game;
    }

    onLobbyJoin(data) {
        console.log('on lobby join event called');
        console.log(data);

        const username = data.username;
        const room = data.room;

        Socket.emit(SocketEvents.ROOM_JOIN, {
            username: username,
            room: room
        });
    }

    playerReady(username: string) {
        Socket.emit(SocketEvents.PLAYER_READY, {username});
    }
}

export default LobbyEvents;