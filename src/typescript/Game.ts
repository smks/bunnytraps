/// <reference path='../../node_modules/phaser/typescript/phaser' />

import Game = Phaser.Game;

import {Boot} from "./states/Boot";
import {Preload} from "./states/Preload";
import {Menu} from "./states/Menu";
import Lobby from "./states/LobbyDemo";
// import {Strategy} from "./states/StrategyDemo";
import {Strategy} from "./states/Strategy";
import {ResultsDemo} from "./states/ResultsDemo";
import {Complete} from "./states/Complete";
import LobbyDemo from "./states/LobbyDemo";
import initGame from "./site/init";

export default class GameSession {
    game: Game;

    constructor() {
        this.game = new Game(
            1920,
            1080,
            Phaser.AUTO,
            'game', {
                create: this.create
            }
        );
    }

    create() {

        this.game.stage.setBackgroundColor('#cccccc');

        // Add game states here
        this.game.state.add('Boot', Boot, false);
        this.game.state.add('Preload', Preload, false);
        this.game.state.add('Lobby', LobbyDemo, false);
        this.game.state.add('Strategy', Strategy, false);
        this.game.state.add('Results', ResultsDemo, false);
        // this.game.state.add('Complete', Complete, false);

        // Boot up game
        this.game.state.start('Boot');
    }
}

initGame();