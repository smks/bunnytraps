/**
 * Created by shaun on 10/12/2016.
 */
const COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
}

export default COLORS;