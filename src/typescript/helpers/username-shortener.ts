/**
 * Created by shaun on 08/01/2017.
 */
const MAX_LENGTH = 5;
export default function(username) {
    let newUsername;
    if (username.length > MAX_LENGTH) {
        newUsername = username.substr(0, MAX_LENGTH) + '...';
    } else {
        newUsername = username;
    }
    return newUsername;
}