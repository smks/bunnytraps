export default function (playerID: number) {

    // getSprite saved data
    const mapper = {
        1: 1,
        2: 2,
        3: 3,
        4: 4
    }

    return mapper[playerID];
}