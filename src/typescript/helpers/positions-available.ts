export default function positionsAvailable() {

    const COLUMNS = 6;
    const ROWS = 5;

    let positionsAvailable = [];

    for (let columnIndex: number = 0; columnIndex < COLUMNS; columnIndex++) {

        for (let rowIndex: number = 0; rowIndex < ROWS; rowIndex++) {

            positionsAvailable.push({columnIndex: columnIndex, rowIndex: rowIndex});
        }
    }

    return positionsAvailable;
}