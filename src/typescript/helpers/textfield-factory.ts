import Assets from "../Assets";
import BitmapText = Phaser.BitmapText;
export default function (game, options) {
    let text =  game.add.bitmapText(
        options.xPos,
        options.yPos,
        options.font,
        options.content,
        options.fontSize,
        options.parent
    );
    text.anchor.set(options.anchorX, options.anchorY);
    return text;
}

export function getDefaultTextFieldOptions(content: string) {
    return {
        content: content,
        fontSize: 30,
        font: Assets.FONT,
        xPos: 0,
        yPos: 0,
        align: 'center',
        anchorX: 0.5,
        anchorY: 0.5,
        parent: undefined
    };
}