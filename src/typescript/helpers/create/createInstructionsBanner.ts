/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import InstructionsBanner from "../../components/InstructionsBanner/InstructionsBanner";
import ArrayUtils = Phaser.ArrayUtils;

export default function (game: Game, defaultMessage: string = '') {
    const instructionsBanner = new InstructionsBanner(game, 80, 40);

    // const messages = [
    //     'Place your bunnies',
    //     'Place your traps',
    //     'Hit the ready button',
    //     'Hurry! Time is running out',
    //     'Place bunnies on the grid',
    //     'Place traps on the grid'
    // ];

    //instructionsBanner.addMessages(messages);

    instructionsBanner.create(defaultMessage);

    return instructionsBanner;
};