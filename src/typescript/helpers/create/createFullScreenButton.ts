/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import FullScreenButton from "../../components/Buttons/FullScreenButton";
export default function (game: Game) {
    const fullScreenButton = new FullScreenButton(game, () => {
        if (game.scale.isFullScreen) {
            game.scale.stopFullScreen();
        } else {
            game.scale.startFullScreen(true);
        }
    });
    fullScreenButton.create(1415, 55);
    return fullScreenButton;
};