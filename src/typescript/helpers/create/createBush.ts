/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import Bush from "../../components/Cover/Bush";
import Assets from "../../Assets";
export default function (game: Game, open: boolean = true, onComplete?: Function) {
    const bush = new Bush(game);
    bush.create();
    if (open) {
        bush.open(() => {

            if (onComplete) {
                onComplete();
            }
        });
    }
    return bush;
};