/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import LargeReadyButton from "../../components/Buttons/LargeReadyButton";
export default function (game: Game, isEnabled: boolean = false) {
    const readyButton = new LargeReadyButton(game, () => {});
    readyButton.create(355, 540.5);
    readyButton.setEnabled(isEnabled);
    return readyButton;
};