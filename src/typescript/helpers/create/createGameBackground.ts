'use strict';
import Game = Phaser.Game;
export default function (game: Game) {
    const gameBg = game.add.sprite(game.world.centerX, game.world.centerY, 'game-bg');
    gameBg.anchor.set(0.5, 0.5);
    return gameBg;
};