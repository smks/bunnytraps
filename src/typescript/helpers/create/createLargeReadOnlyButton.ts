/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import LargeReadOnlyButton from "../../components/Buttons/LargeReadOnlyButton";
export default function (game: Game) {
    const readyButton = new LargeReadOnlyButton(game);
    readyButton.create(355, 540.5);
    return readyButton;
};