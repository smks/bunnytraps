/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import ExitButton from "../../components/Buttons/ExitButton";
export default function (game: Game) {
    const exitButton = new ExitButton(game, () => {
        alert('Pressed exit button');
    });
    exitButton.create(1787, 55);
    return exitButton;
};