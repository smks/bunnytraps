/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import BunnyTurnIndicator from "../../components/Bunny/BunnyTurnIndicator";

export default function (game: Game, xPos:number, yPos:number, bunnyId: number = 1, bunnyCount: number) {
    const bunnyIndicator = new BunnyTurnIndicator(game, bunnyId);
    bunnyIndicator.create(xPos, yPos, bunnyCount);
    return bunnyIndicator;
};