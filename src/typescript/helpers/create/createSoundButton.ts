/**
 * Created by shaun on 14/01/2017.
 */
'use strict';
import Game = Phaser.Game;
import SoundButton from "../../components/Buttons/SoundButton";
export default function (game: Game) {
    const soundButton = new SoundButton(game);
    soundButton.addOnPress(() => {
        if (!game.sound.mute) {
            game.sound.mute = true;
            soundButton.setOff();
        } else {
            game.sound.mute = false;
            soundButton.setOn();
        }
    });
    soundButton.create(1600, 55);
    return soundButton;
};