
import Socket from "../socket/Socket";

export class Menu extends Phaser.State {

    private menuText;
    private bg:Phaser.Sprite;
    private logo:Phaser.Sprite;
    private helpText:Phaser.Text;
    private playButton:Phaser.Sprite;

    constructor() {
        super();
    }

    create() {
        Socket.init();

        this.bg = this.game.add.sprite(0, 0, 'background');

        this.logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'sprites', 'logo.png');
        this.logo.alpha = 0;
        this.logo.anchor.set(0.5, 0.5);

        this.playButton = this.game.add.sprite(701, 648, 'sprites', 'play-button.png');
        this.playButton.inputEnabled = true;
        this.playButton.alpha = 0;

        let tween = this.game.add.tween(this.logo)
            .to({alpha: 1}, 1000, "Linear", false);

        tween.onComplete.add(() => {
            this.showPlayButton();
        });

        tween.start();
    }

    showPlayButton() {
        let tween = this.game.add.tween(this.playButton)
            .to({alpha: 1}, 1000, "Linear", false);

        tween.onComplete.add(() => {
            this.playButton.events.onInputDown.add(() => {
                this.game.state.start('Lobby', true);
                this.fadeOut();
            });
        });

        tween.start();
    }

    fadeOut() {

    }

    shutdown() {
        this.bg = null;
    }
}