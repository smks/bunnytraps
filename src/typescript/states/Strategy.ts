import Logs from "../components/Logs/Logs";
import InstructionsBanner from "../components/InstructionsBanner/InstructionsBanner";
import Grid from "../components/Grid/Grid";
import BunnyTurnIndicator from "../components/Bunny/BunnyTurnIndicator";
import FullScreenButton from "../components/Buttons/FullScreenButton";
import SoundButton from "../components/Buttons/SoundButton";
import ExitButton from "../components/Buttons/ExitButton";
import TimerIndicator from "../components/Timer/CountdownTimer";
import PointsIndicator from "../components/PointsIndicator/PointsIndicator";
import BunnyPlacer from "../placers/BunnyPlacer";
import TrapPlacer from "../placers/TrapPlacer";
import LargeReadyButton from "../components/Buttons/LargeReadyButton";
import Signal = Phaser.Signal;
import State = Phaser.State;
import Bush from "../components/Cover/Bush";
import Save from "../storage/save";

// Creators
import createGameBackground from '../helpers/create/createGameBackground';
import createInstructionsBanner from '../helpers/create/createInstructionsBanner';
import createBunnyIndicator from '../helpers/create/createBunnyIndicator';
import createLargeReadyButton from '../helpers/create/createLargeReadyButton';
import createFullScreenButton from '../helpers/create/createFullScreenButton';
import createSoundButton from '../helpers/create/createSoundButton';
import createExitButton from '../helpers/create/createExitButton';
import createBush from '../helpers/create/createBush';
import Assets from "../Assets";

export class Strategy extends State {

    private gameBg: Phaser.Sprite;
    private fullScreenButton: FullScreenButton;
    private soundButton: SoundButton;
    private exitButton: ExitButton;
    private instructionsBanner: InstructionsBanner;
    private grid: Grid;
    private logs: Logs;
    private timer: TimerIndicator;
    private pointsIndicator: PointsIndicator;
    private trapPlacement: TrapPlacer;
    private bunnyPlacement: BunnyPlacer;
    private readyButton: LargeReadyButton;
    private bunnyOne: BunnyTurnIndicator;
    private bunnyTwo: BunnyTurnIndicator;
    private bunnyThree: BunnyTurnIndicator;
    private bunnyFour: BunnyTurnIndicator;
    private bush: Bush;
    private placementTotal: number = 5;
    private save: Save;

    // Signals
    private placeTileSignal: Signal;
    private placementComplete: Signal;
    private trapPlacementComplete: Signal;
    private bunnyPlacementComplete: Signal;

    private currentPlayerIndex: number = 1;
    private playerScore: number = 0;
    private strategyMusic: Phaser.Sound;

    private hasPlacedAllBunnies: boolean = false;

    constructor() {
        super();
    }

    init() {
        this.currentPlayerIndex = 1;
        this.playerScore = 0;
    }

    create() {

        this.save = new Save();

        this.playMusic();

        // add assets
        this.addBackground();
        this.addInstructionsBanner();
        this.addBunnyTurnIndicators();
        this.addBigActionButton();
        this.addActionButton();
        this.addCountdownTimer();
        this.addPointsIndicator();
        this.addLogs();

        this.bush = createBush(this.game, true, () => {
            this.instructionsBanner.changeMessage(`Place ${this.placementTotal} bunnies on the grid`);
        });

        this.addListeners();

        this.addGrid();

        // add logic
        this.addBunnyPlacement();
        this.addTrapPlacement();

        this.listenForBunnyPlacement();
    }

    private addListeners() {
        this.addGridListeners();
        this.addBunnyPlacementListeners();
        this.addTrapPlacementListeners();
    }

    private addGridListeners() {
        this.placeTileSignal = new Phaser.Signal();

        this.placeTileSignal.add((params) => {

            let numbersLeft: number;
            let subject: string = '';
            let placements: number;

            if (this.hasPlacedAllBunnies === true) {
                placements = this.trapPlacement.getPlantedCoordinates().length;
                subject = (placements === (this.placementTotal - 2)) ? 'trap' : 'traps';
            } else {
                placements = this.bunnyPlacement.getPlantedCoordinates().length;
                subject = (placements === (this.placementTotal - 2)) ? 'bunny' : 'bunnies';
            }

            numbersLeft = this.placementTotal - (placements + 1);

            this.instructionsBanner.changeMessage(`${numbersLeft} ${subject} left`);

            console.log('Pressed tile');
            console.log(params);
        });
    }

    private addBunnyPlacementListeners() {
        this.bunnyPlacementComplete = new Signal();

        this.bunnyPlacementComplete.add(this.enableReadyButton, this);

        this.bunnyPlacementComplete.add(() => {
            
            this.instructionsBanner.changeMessage('Hit ready below');

            this.readyButton.setOnClick(this.enableTrapPlacement.bind(this));
        }, this);
    }

    private enableReadyButton(isEnabled: boolean = true) {
        this.readyButton.setEnabled(isEnabled);
    }

    private enableTrapPlacement() {
        this.hasPlacedAllBunnies = true;
        this.instructionsBanner.changeMessage('Add 5 traps to the grid');
        this.enableReadyButton(false);
        this.bunnyPlacement.ignore();
        this.bunnyPlacement.clear();
        this.listenForTrapPlacement();
    }

    private addTrapPlacementListeners() {
        this.trapPlacementComplete = new Signal();

        this.trapPlacementComplete.add(this.enableReadyButton.bind(this));

        this.trapPlacementComplete.add(() => {

            this.instructionsBanner.changeMessage('Hit ready below');

            this.readyButton.setOnClick(() => {

                this.enableReadyButton(false);

                this.disableTrapPlacement();

                this.removeAllTweens();

                this.switchStateWithBushAnimation();
            });
        });
    }

    private removeAllTweens() {
        this.game.tweens.removeAll();
    }

    private disableTrapPlacement() {
        this.trapPlacement.clear();
        this.trapPlacement.ignore();
    }

    private switchStateWithBushAnimation() {
        this.game.world.bringToTop(this.bush.getSprite());

        this.strategyMusic.fadeOut(500);

        this.storeResults();

        this.bush.close(() => {
            this.game.state.start('Results', true);
        });
    }

     private storeResults() {

        let decisions: { playerId; bunnyId, bunnyPlacements; trapPlacements } = {
            playerId: 1,
            bunnyId: 1,
            bunnyPlacements: this.bunnyPlacement.getPlantedCoordinates(),
            trapPlacements: this.trapPlacement.getPlantedCoordinates(),
        };
        
        this.save.storeDecisions(1, decisions);
    }

    render() {
        this.timer.render();
    }

    private playMusic() {
        this.strategyMusic = this.game.add.audio(Assets.AUDIO_KEY_AUDIO_IN_GAME_01, 1, true);
        this.strategyMusic.play();
    }

    private addBackground() {
        this.gameBg = createGameBackground(this.game);
    }

    private addInstructionsBanner() {
        this.instructionsBanner = createInstructionsBanner(this.game, 'Hello');
    }

    private addBunnyTurnIndicators() {

        this.bunnyOne = createBunnyIndicator(this.game, 160.5, 295, 1, this.save.getBunnyCount(1));
        this.bunnyTwo = createBunnyIndicator(this.game, 290.5, 295, 2, this.save.getBunnyCount(2));
        this.bunnyThree = createBunnyIndicator(this.game, 420.5, 295, 3, this.save.getBunnyCount(3));
        this.bunnyFour = createBunnyIndicator(this.game, 550.5, 295, 4, this.save.getBunnyCount(4));

        const bunnies: Array<BunnyTurnIndicator> = [];

        bunnies.push(this.bunnyOne);
        bunnies.push(this.bunnyTwo);
        bunnies.push(this.bunnyThree);
        bunnies.push(this.bunnyFour);

        bunnies.map((bunny, index) => {
            bunny.moveForward(this.save.getBunnyCount(index + 1));
        });
    }

    private addBigActionButton() {
        this.readyButton = createLargeReadyButton(this.game);
    }

    private addActionButton() {
        this.fullScreenButton = createFullScreenButton(this.game);
        this.soundButton = createSoundButton(this.game);
        this.exitButton = createExitButton(this.game);
    }

    private addGrid() {
        this.grid = new Grid(this.game, this.placeTileSignal, this.placementComplete);
        this.grid.create(856.5, 202.5);
        this.grid.addInteraction();
    }

    private addLogs() {
        this.logs = new Logs(this.game, 355, 620);
    }

    private addLogMessage(message: string) {
        this.logs.addLogMessage(message);
    }

    private addCountdownTimer() {
        this.timer = new TimerIndicator(this.game, 0, 20);
        this.timer.create(857, 55);
        this.timer.start();
        this.timer.addFinalSecondsCallback(this.onFinalSeconds.bind(this));
        this.timer.addFinalSecondIteration(this.onFinalSecondIteration.bind(this));
    }

    onFinalSeconds() {
        this.strategyMusic.fadeOut(200);
    }

    onFinalSecondIteration() {
        this.game.camera.flash(0xf73d2f, 200);
    }

    private addPointsIndicator() {
        this.pointsIndicator = new PointsIndicator(this.game);
        const played = parseInt(this.save.getRoomGamesPlayed(), 10) + 1;
        this.pointsIndicator.create(1136, 55, `Round ${played} of 5`);
    }

    private addBunnyPlacement() {
        const playerID = 1;
        this.bunnyPlacement = new BunnyPlacer(this.game, this.placeTileSignal, playerID, this.bunnyPlacementComplete);
        this.bunnyPlacement.ignore();
    }

    private addTrapPlacement() {
        this.trapPlacement = new TrapPlacer(this.game, this.placeTileSignal, this.trapPlacementComplete);
        this.trapPlacement.ignore();
    }

    private listenForBunnyPlacement() {
        this.bunnyPlacement.listen();
    }

    private listenForTrapPlacement() {
        this.trapPlacement.listen();
    }

    shutdown() {
        this.grid.destroy();
        this.instructionsBanner.destroy();
        this.logs.destroy();
        this.strategyMusic.destroy(true);
        this.timer.stopMusic();
        this.timer.destroy();
    }
}
