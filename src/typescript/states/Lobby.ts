import Assets from "../Assets";
import Socket from "../socket/Socket";
import SocketEvents from "../socket/SocketEvents";
import Save from "../storage/save";

const COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
}

export class Lobby extends Phaser.State {

    private bg: Phaser.Sprite;
    private playButton: Phaser.Sprite;
    private players: Array<any> = [];
    private myUsername;
    private logo: Phaser.Sprite;
    private lobbyStatusText: Phaser.Text;
    private countDownHandle;
    private secondsToWait: number;
    private isAboutToPlayGame: boolean;

    constructor() {
        super();
        this.setCountdown = this.setCountdown.bind(this);
    }

    preload() {
        Socket.onLobbyJoin.add(this.onLobbyJoin, this);
        Socket.onRoomJoined.add(this.onRoomJoin, this);
        Socket.onGameStarted.add(this.onGameStart, this);
        Socket.onDisconnect.add(this.onPlayerDisconnected, this);
        this.myUsername = Save.getUser();
    }

    create() {
        this.bg = this.game.add.sprite(0, 0, Assets.LOBBY_BG);

        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets.LOBBY_ASSETS, Assets.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);

        //this.lobbyStatusText = textField);
        this.lobbyStatusText.anchor.set(0.5, 0.5);

        let tween = this.game.add.tween(this.logo.scale)
            .to({x: 1, y: 1}, 1000, Phaser.Easing.Circular.InOut, false);

        tween.onComplete.add(() => {
            Socket.emit(SocketEvents.LOBBY_JOIN, {
                username: this.myUsername
            });
            this.lobbyStatusText.alpha = 1;
        });

        tween.start();
    }

    /**
     *
     * @SOCKET_EVENT: SocketEvents.LOBBY_JOINED
     */
    onLobbyJoin(data) {
        console.log('on lobby join event called');
        console.log(data);

        const username = data.username;
        const room = data.room;

        Socket.emit(SocketEvents.ROOM_JOIN, {
            username: username,
            room: room
        });
    }

    /**
     *
     * @SOCKET_EVENT: SocketEvents.GAME_START
     */
    onGameStart() {



        this.game.state.start('Strategy', true);
    }

    /**
     *
     * @SOCKET_EVENT: SocketEvents.ROOM_JOIN
     */
    onRoomJoin(data) {
        console.log('on room join called');

        let allPlayersData = data.players;
        const secondsToWait = data.secondsToWait;

        allPlayersData.map((player) => {

            const playerNumber = player.playerNumber;
            const username = player.username;

            for (let i = 0; i < this.players.length; i++) {
                const clientPlayer = this.players[i];
                console.log('Checking player: ' + clientPlayer.username);
                if (clientPlayer.username == username) {
                    console.log('player exists already, no need to add to lobby');
                    return;
                }
            }

            const isPlayerMe = player === this.myUsername;

            let newPlayer = {
                id: playerNumber,
                username: player.username,
                playerHead: null
            };

            this.players.push(newPlayer);

            this.lobbyStatusText.text = this.setLobbyStatus(this.players.length);
        });

        if (secondsToWait && !this.isAboutToPlayGame) {
            this.isAboutToPlayGame = true;
            this.secondsToWait = secondsToWait;
            this.countDownHandle = this.game.time.create(false);
            this.countDownHandle.loop(Phaser.Timer.SECOND, this.setCountdown, this);
        }
    }

    /**
     * Called every second to count down when game plays
     */
    private setCountdown() {
        if (this.secondsToWait > 1) {
            this.secondsToWait--;
            this.lobbyStatusText.text = `Playing in ${this.secondsToWait} seconds`;
        } else {
            this.lobbyStatusText.destroy(true);
            this.countDownHandle.destroy();
        }
    }

    /**
     *
     * @param id
     */
    onPlayerDisconnected(data) {

        const username = data.username;

        console.log(username + ' just left');

        for (var i = 0; i < this.players.length; i++) {

            const player = this.players[i];

            if (player.username == username) {
                player.playerHead.destroy(true);
                this.players.splice(i, 1);
                this.lobbyStatusText.text = this.setLobbyStatus(this.players.length);
                break;
            }
        }
    }

    /**
     *
     * @param totalPlayers
     * @param room
     * @returns {any}
     */
    setLobbyStatus(totalPlayers) {
        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }

        return 'WAITING FOR {n} PLAYERS'.replace('{n}', (4 - this.players.length).toString());
    }

    showPlayButton() {
        let tween = this.game.add.tween(this.playButton)
            .to({alpha: 1}, 1000, "Linear", false);

        tween.onComplete.add(() => {
            this.playButton.events.onInputDown.add(() => {
                this.game.state.start('Strategy', true);
                this.fadeOut();
            });
        });

        tween.start();
    }

    fadeOut() {

    }

    shutdown() {
        this.bg = null;
        Socket.onLobbyJoin.remove(this.onLobbyJoin, this);
        Socket.onRoomJoined.remove(this.onRoomJoin, this);
        Socket.onGameStarted.remove(this.onGameStart, this);
        Socket.onDisconnect.remove(this.onPlayerDisconnected, this);

        if (this.lobbyStatusText) {
            this.lobbyStatusText.destroy(true);
        }
        if (this.countDownHandle) {
            this.countDownHandle.destroy();
        }
    }
}