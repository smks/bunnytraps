import Assets from "../Assets";
export class Boot extends Phaser.State {

    preload() {
        this.game.load.image('loading-bg', Assets.LOADING_BG);
        this.game.load.bitmapFont(Assets.FONT, '/assets/fonts/FreckleFace.png', '/assets/fonts/FreckleFace.fnt');
    }

    create() {

        //  Unless you specifically need to support multitouch I would recommend setting this to 1
        this.input.maxPointers = 1;

        //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
        this.stage.disableVisibilityChange = true;

        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.windowConstraints.bottom = "visual";
        this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        if (this.game.device.desktop) {

        } else {
            //  Same goes for mobile settings.
        }

        // todo - remove
        //this.game.sound.mute = true;

        this.game.state.start('Preload', true, false);
    }
}