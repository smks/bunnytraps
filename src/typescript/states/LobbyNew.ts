import Assets from "../Assets";
import ReadyButton from "../components/Buttons/ReadyButton";
import LobbyStatusBar from "../components/Lobby/LobbyStatusBar";
import LobbyPlayerBar from "../components/Lobby/LobbyPlayerBar";
import Bunny from "../components/Bunny/Sprite/Bunny";
import FullScreenButton from "../components/Buttons/FullScreenButton";
import SoundButton from "../components/Buttons/SoundButton";
import ExitButton from "../components/Buttons/ExitButton";
import Bush from "../components/Cover/Bush";
import Save from "../storage/save";
import Socket from "../socket/Socket";
import LobbyEvents from "../socket/LobbyEvents";
import Timer = Phaser.Timer;
import State = Phaser.State;
import SocketEvents from "../socket/SocketEvents";
import createSoundButton from '../helpers/create/createSoundButton';

export default class LobbyNew extends State {

    private bg: Phaser.Image;
    private logo: Phaser.Sprite;
    private playerBar1: LobbyPlayerBar;
    private playerBar2: LobbyPlayerBar;
    private playerBar3: LobbyPlayerBar;
    private playerBar4: LobbyPlayerBar;
    private fullScreenButton: FullScreenButton;
    private soundButton: SoundButton;
    private exitButton: ExitButton;
    private bunnyOne: Bunny;
    private bunnyTwo: Bunny;
    private bunnyThree: Bunny;
    private bunnyFour: Bunny;
    private players: Array<{id: number; username: string; bunnySprite: Bunny}>;
    private lobbyStatusBar: LobbyStatusBar;
    private myUsername: string;
    private events: LobbyEvents;
    private secondsToWait: number;
    private countDownHandle: Timer;
    private isAboutToPlayGame: boolean = false;
    private readyButtons: Array<ReadyButton> = new Array();
    private lobbyMusic:Phaser.Sound;
    private bush: Bush;

    constructor() {
        super();
    }

    preload() {

        this.events = new LobbyEvents(this.game);
        this.players = new Array();

        Socket.onLobbyJoin.add(this.events.onLobbyJoin, this);
        Socket.onRoomJoined.add(this.onRoomJoin, this);
        Socket.onPlayerReady.add(this.onPlayerReady, this);
        Socket.onGameStarted.add(this.onGameStart, this);
        Socket.onDisconnect.add(this.onPlayerDisconnected, this);
        this.myUsername = Save.getUser();
    }

    onPlayerReady(data) {

        const usernameReady:string = data.player;
        let userId: number;

        for (var i = 0; i < this.players.length; i++) {

            const player = this.players[i];

            if (player.username == usernameReady) {
                userId = player.id;
            }
        }

        const readyButton = this.readyButtons[userId - 1];

        if (readyButton) {
            readyButton.setPressedState();
        }
    }

    onPlayerDisconnected(data) {

        const username = data.username;

        console.log(username + ' just left');

        for (var i = 0; i < this.players.length; i++) {

            const player = this.players[i];

            if (player.username == username) {
                player.bunnySprite.destroy();
                this.players.splice(i, 1);
                this.lobbyStatusBar.setText(this.setLobbyStatus(this.players.length));
                break;
            }
        }
    }

    /**
     *
     * this.players, this.myUsername, this.addPlayerHead, this.lobbyStatusText, this.secondsToWait
     *
     * @param data
     */
    onRoomJoin(data) {

        console.log('on room join called');

        let allPlayersData = data.players;
        const secondsToWait = data.secondsToWait;

        allPlayersData.map((player) => {

            const playerNumber = player.playerNumber;
            const bunnyId = player.bunnyId;
            const username = player.username;

            for (let i = 0; i < this.players.length; i++) {
                const clientPlayer = this.players[i];
                console.log('Checking player: ' + clientPlayer.username);
                if (clientPlayer.username == username) {
                    console.log('player exists already, no need to add to lobby');
                    return;
                }
            }

            const isPlayerMe = player === this.myUsername;

            let newPlayer = {
                id: playerNumber,
                username: player.username,
                bunnySprite: this.addBunny(playerNumber)
            };

            this.addLobbyPlayerBar(playerNumber, player.username);

            this.addReadyButton(playerNumber, player.username);

            this.players.push(newPlayer);

            if (!this.isAboutToPlayGame) {
                this.lobbyStatusBar.setText(this.setLobbyStatus(this.players.length));
            }
        });

        if (secondsToWait && !this.isAboutToPlayGame) {
            this.isAboutToPlayGame = true;
            this.secondsToWait = secondsToWait;
            this.countDownHandle = this.game.time.create(false);
            this.countDownHandle.loop(Phaser.Timer.SECOND, this.setCountdown, this);
            this.countDownHandle.start();
        }
    }

    /**
     * Called every second to count down when game plays
     */
    private setCountdown() {

        console.log('set countdown called');

        if (this.secondsToWait > 1) {
            this.secondsToWait--;
            this.lobbyStatusBar.setText(`Playing in ${this.secondsToWait} seconds`);
        } else {
            this.lobbyStatusBar.destroy();
            this.countDownHandle.destroy();
        }
    }

    create() {

        this.lobbyMusic = this.game.add.audio('main-music', 1, true);
        this.lobbyMusic.play();

        this.addBackground();
        this.addLogo();
        this.addLobbyStatusBar();
        // this.addLobbyPlayerBar();
        // this.addBunnies();
        this.addCustomButtons();

        // add bush overlay
        this.bush = new Bush(this.game);
        this.bush.create(0, 0);
        this.bush.open(() => {
            this.tweenInLogo();
        });

    }

    private tweenInLogo() {

        let tween = this.game.add.tween(this.logo.scale)
            .to({x: 1, y: 1}, 1000, Phaser.Easing.Circular.InOut, false);

        tween.onComplete.add(() => {
            Socket.emit(SocketEvents.LOBBY_JOIN, {
                username: this.myUsername
            });
        });

        tween.start();
    }

    addBackground() {

        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'lobby-bg');
        this.bg.anchor.set(0.5, 0.5);

    }

    addLogo() {

        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets.LOBBY_ASSETS, Assets.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);

    }

    addReadyButton(playerNumber, username) {

        const isMe = username === this.myUsername;

        const readyButton = new ReadyButton(this.game, playerNumber);

        readyButton.create();

        if (isMe) {
            readyButton.addOnPress(() => {
                readyButton.setPressedState();
                readyButton.setEnabled(false);
            });
        } else {
            readyButton.setEnabled(false);
            readyButton.setDisabled();
        }

        readyButton.movePositionByPlayerNumber(playerNumber);

        this.readyButtons[playerNumber] = readyButton;
    }

    addLobbyStatusBar() {

        this.lobbyStatusBar = new LobbyStatusBar(this.game);
        this.lobbyStatusBar.create(this.game.world.centerX, 950);
        this.lobbyStatusBar.setText(this.setLobbyStatus());

    }

    /**
     *
     * @param playerNumber
     * @param username
     */
    addLobbyPlayerBar(playerNumber: number, username: string) {

        switch (playerNumber) {
            case 1:
                this.playerBar1 = new LobbyPlayerBar(this.game, username);
                this.playerBar1.create(570.5, 850);
                break;
            case 2:
                this.playerBar2 = new LobbyPlayerBar(this.game, username);
                this.playerBar2.create(830.5, 850);
                break;
            case 3:
                this.playerBar3 = new LobbyPlayerBar(this.game, username);
                this.playerBar3.create(1090.5, 850);
                break;
            case 4:
                this.playerBar4 = new LobbyPlayerBar(this.game, username);
                this.playerBar4.create(1350.5, 850);
                break;
        }
    }

    /**
     *
     * @param totalPlayers
     * @param room
     * @returns {any}
     */
    setLobbyStatus(totalPlayers:number = 1): string {

        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }

        return 'WAITING FOR {n} PLAYERS'.replace('{n}', (4 - this.players.length).toString());

    }

    /**
     *
     * @param playerNumber
     * @param username
     * @param bunnyId
     */
    private addBunny(bunnyId): Bunny {

        const animate = (bunny: Bunny) => {
            bunny.standAnimation();
        };

        switch (bunnyId) {
            case 1:
                this.bunnyOne = new Bunny(this.game, bunnyId);
                this.bunnyOne.create(580, 700);
                this.bunnyOne.dropDown(() => {
                    animate(this.bunnyOne);
                }, 200);
                return this.bunnyOne;
            case 2:
                this.bunnyTwo = new Bunny(this.game, bunnyId);
                this.bunnyTwo.create(840, 700);
                this.bunnyTwo.dropDown(() => {
                    animate(this.bunnyTwo);
                }, 200);
                return this.bunnyTwo;
            case 3:
                this.bunnyThree = new Bunny(this.game, bunnyId);
                this.bunnyThree.create(1100, 700);
                this.bunnyThree.dropDown(() => {
                    animate(this.bunnyThree);
                }, 200);
                return this.bunnyThree;
            case 4:
                this.bunnyFour = new Bunny(this.game, bunnyId);
                this.bunnyFour.create(1360, 700);
                this.bunnyFour.dropDown(() => {
                    animate(this.bunnyFour);
                }, 200);
                return this.bunnyFour;
        }

    }

    fadeOut() {

    }

    private addCustomButtons() {

        this.fullScreenButton = new FullScreenButton(this.game, () => {
            if (this.game.scale.isFullScreen) {
                this.game.scale.stopFullScreen();
            } else {
                this.game.scale.startFullScreen(true);
            }
        });
        this.fullScreenButton.create(1415, 55);

        this.soundButton = createSoundButton(this.game);

        this.exitButton = new ExitButton(this.game, () => {
            alert('Pressed exit button');
        });
        this.exitButton.create(1787, 55);
    }

    onGameStart() {

        this.lobbyMusic.fadeOut(500);

        this.game.world.bringToTop(this.bush.getSprite());

        this.bush.close(() => {
            this.game.state.start('Strategy', true);
        });
    }

    shutdown() {

        this.bg.destroy(true);

        Socket.onLobbyJoin.remove(this.events.onLobbyJoin, this);
        Socket.onRoomJoined.remove(this.onRoomJoin, this);
        Socket.onGameStarted.remove(this.onGameStart, this);
        Socket.onDisconnect.remove(this.onPlayerDisconnected, this);

        if (this.lobbyStatusBar) {
            this.lobbyStatusBar.destroy();
        }

        if (this.countDownHandle) {
            this.countDownHandle.destroy();
        }

    }
}