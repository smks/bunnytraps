import Assets from '../Assets';
import Hints from '../components/Hints/Hints';
import Timer = Phaser.Timer;
import PercentageLoader from "../components/Loader/PercentageLoader";
import Socket from "./../socket/Socket";
import Bush from '../components/Cover/Bush';

export class Preload extends Phaser.State {

    private bg: Phaser.Image;
    private loading;
    private hints: Hints;
    private loader: PercentageLoader;

    preload() {

        this.addBackground();
        this.addPercentageLoader();
        this.addHint();

        /** Load Atlas */
        this.game.load.atlas(
            Assets.LOBBY_ASSETS,
            Assets.ATLAS_LOBBY,
            Assets.ATLAS_LOBBY_JSON,
            Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY
        );

        /** Load Atlas */
        this.game.load.atlas(
            Assets.GAME_ASSETS,
            Assets.ATLAS_GAME,
            Assets.ATLAS_GAME_JSON,
            Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY
        );

        /** Load Atlas */
        this.game.load.atlas(
            Assets.BUNNIES_ASSETS,
            Assets.ATLAS_BUNNIES,
            Assets.ATLAS_BUNNIES_JSON,
            Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY
        );

        /** Load Atlas */
        this.game.load.atlas(
            Assets.BUSH_ASSETS,
            Assets.ATLAS_BUSH,
            Assets.ATLAS_BUSH_JSON,
            Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY
        );

        this.game.load.image('lobby-bg', Assets.LOBBY_BG);
        this.game.load.image('game-bg', Assets.GAME_BG);

        this.game.load.bitmapFont(Assets.FONT_INVERSE, '/assets/fonts/FreckleFaceWhite.png', '/assets/fonts/FreckleFaceWhite.fnt');
        this.game.load.bitmapFont(Assets.FONT_BUTTONS, '/assets/fonts/DentOne.png', '/assets/fonts/DentOne.fnt');
        this.game.load.bitmapFont(Assets.FONT_LOGS, '/assets/fonts/Roboto.png', '/assets/fonts/Roboto.fnt');

        /** Load Audio */
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_MAIN_THEME_01, Assets.AUDIO_MAIN_THEME_01);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_IN_GAME_01, Assets.AUDIO_IN_GAME_01);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_THEME_02, Assets.AUDIO_THEME_02);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_AMBIENCE_LOOP, Assets.AUDIO_AMBIENCE_LOOP);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_DROP, Assets.AUDIO_BUNNY_DROP);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_SET_TRAP, Assets.AUDIO_SET_TRAP);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_HITS_SCREEN, Assets.AUDIO_BUNNY_HITS_SCREEN);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SLIDE, Assets.AUDIO_BUNNY_SLIDE);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SOUND_01, Assets.AUDIO_BUNNY_SOUND_01);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SOUND_02, Assets.AUDIO_BUNNY_SOUND_02);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SOUND_03, Assets.AUDIO_BUNNY_SOUND_03);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SOUND_04, Assets.AUDIO_BUNNY_SOUND_04);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_SOUND_05, Assets.AUDIO_BUNNY_SOUND_05);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_FLYING_01, Assets.AUDIO_BUNNY_FLYING_01);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_FLYING_02, Assets.AUDIO_BUNNY_FLYING_02);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_FLYING_03, Assets.AUDIO_BUNNY_FLYING_03);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_FLYING_04, Assets.AUDIO_BUNNY_FLYING_04);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUNNY_FLYING_05, Assets.AUDIO_BUNNY_FLYING_05);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUSH_COLLIDE, Assets.AUDIO_BUSH_COLLIDE);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUSH_SEPARATE, Assets.AUDIO_BUSH_SEPARATE);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_BUTTON_PRESS, Assets.AUDIO_BUTTON_PRESS);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_CHANGE_PLAYER, Assets.AUDIO_CHANGE_PLAYER);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_CLOCK_TICK, Assets.AUDIO_CLOCK_TICK);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_CROWD_CHEERING, Assets.AUDIO_CROWD_CHEERING);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_LOG_APPEAR, Assets.AUDIO_LOG_APPEAR);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_NEW_MESSAGE, Assets.AUDIO_NEW_MESSAGE);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_REVEAL_CARD, Assets.AUDIO_REVEAL_CARD);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_TILE_LAND, Assets.AUDIO_TILE_LAND);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_TILE_SPRING, Assets.AUDIO_TILE_SPRING);
        this.game.load.audio(Assets.AUDIO_KEY_AUDIO_TIME_UP, Assets.AUDIO_TIME_UP);
    }

    loadUpdate() {
        this.loader.setPercentage(this.game.load.progress);
    }

    addBackground() {
        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'loading-bg');
        this.bg.anchor.set(0.5, 0.5);
    }

    addHint() {
        const options = {
            xPos: 950,
            yPos: 935,
            fontSize: 60,
            font: Assets.FONT,
            align: 'center',
            anchorX: 0.5,
            anchorY: 0.5
        };

        const timeToWaitNextHint = Timer.SECOND * 4;

        this.hints = new Hints(
            this.game,
            options,
            timeToWaitNextHint
        );
    }

    private addPercentageLoader() {
        this.loader = new PercentageLoader(this.game);
        this.loader.create(this.game.world.centerX, 280);
        this.loader.setPercentage(this.game.load.progress);
    }

    update() {

    }

    create() {
       // Socket.init();
        // this.game.state.start('Lobby', true);

        const bush = new Bush(this.game);
        bush.create(0, 0);
        bush.close(() => {
            if (!window['showPreloader']) {
                this.game.state.start('Lobby', true, false);
                // this.game.state.start('Results', true, false);
            }
        });
    }

    shutdown() {
        this.bg.destroy(true);
        this.hints.destroy();
    }

}