import Assets from "../Assets";
import ReadyButton from "../components/Buttons/ReadyButton";
import LobbyStatusBar from "../components/Lobby/LobbyStatusBar";
import LobbyPlayerBar from "../components/Lobby/LobbyPlayerBar";
import Bunny from "../components/Bunny/Sprite/Bunny";
import FullScreenButton from "../components/Buttons/FullScreenButton";
import SoundButton from "../components/Buttons/SoundButton";
import ExitButton from "../components/Buttons/ExitButton";
import Bush from "../components/Cover/Bush";
import createSoundButton from '../helpers/create/createSoundButton';

const COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
}

export default class LobbyNew extends Phaser.State {

    private bg: Phaser.Image;
    private logo: Phaser.Sprite;
    private readyButtonOne: ReadyButton;
    private readyButtonTwo: ReadyButton;
    private readyButtonThree: ReadyButton;
    private readyButtonFour: ReadyButton;
    private playerBarOne: LobbyPlayerBar;
    private playerBarTwo: LobbyPlayerBar;
    private playerBarThree: LobbyPlayerBar;
    private playerBarFour: LobbyPlayerBar;
    private fullScreenButton: FullScreenButton;
    private soundButton: SoundButton;
    private exitButton: ExitButton;
    private bunnyOne: Bunny;
    private bunnyTwo: Bunny;
    private bunnyThree: Bunny;
    private bunnyFour: Bunny;
    private lobbyStatusBar: LobbyStatusBar;
    private lobbyMusic: Phaser.Sound;
    private bush: Bush;


    constructor() {
        super();
    }

    preload() {
    }

    create() {

        this.lobbyMusic = this.game.add.audio(Assets.AUDIO_KEY_AUDIO_MAIN_THEME_01, 1, true);
        this.lobbyMusic.play();

        this.addBackground();
        this.addLogo();
        this.addReadyButtons();
        this.addLobbyStatusBar();
        this.addLobbyPlayerBar();
        this.addBunnies();
        this.addCustomButtons();

        // add bush overlay
        this.bush = new Bush(this.game);
        this.bush.create(0, 0);
        this.bush.open(() => {
            this.tweenInLogo();
        });

    }

    private tweenInLogo() {
        let tween = this.game.add.tween(this.logo.scale)
            .to({x: 1, y: 1}, 1000, Phaser.Easing.Circular.InOut, false);

        tween.onComplete.add(() => {

        });

        tween.start();
    }

    addBackground() {
        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'lobby-bg');
        this.bg.anchor.set(0.5, 0.5);
    }

    addLogo() {
        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets.LOBBY_ASSETS, Assets.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);
    }

    addReadyButtons() {

        this.readyButtonOne = new ReadyButton(this.game, 1, () => {
            this.readyButtonOne.setPressedState();
            this.readyButtonOne.setEnabled(false);
            this.onGameStart();
        });
        this.readyButtonOne.create(570.5, 410);

        this.readyButtonTwo = new ReadyButton(this.game, 2);
        this.readyButtonTwo.create(830.5, 410);
        this.readyButtonTwo.setEnabled(false);
        this.readyButtonTwo.setDisabled();

        this.readyButtonThree = new ReadyButton(this.game, 3);
        this.readyButtonThree.create(1090.5, 410);
        this.readyButtonThree.setDisabled();

        this.readyButtonFour = new ReadyButton(this.game, 4);
        this.readyButtonFour.create(1350.5, 410);
        this.readyButtonFour.setEnabled(false);
        this.readyButtonFour.setDisabled();
    }

    onGameStart() {

        this.lobbyMusic.fadeOut(500);

        this.game.world.bringToTop(this.bush.getSprite());

        this.bush.close(() => {
            this.game.state.start('Strategy', true);
        });
    }

    addBunnies() {

        this.bunnyOne = new Bunny(this.game, 1);
        this.bunnyOne.create(580, 700);
        this.bunnyOne.standAnimation();

        this.bunnyTwo = new Bunny(this.game, 2);
        this.bunnyTwo.create(840, 700);
        this.bunnyTwo.standAnimation();

        this.bunnyThree = new Bunny(this.game, 3);
        this.bunnyThree.create(1100, 700);
        this.bunnyThree.standAnimation();

        this.bunnyFour = new Bunny(this.game, 4);
        this.bunnyFour.create(1360, 700);
        this.bunnyFour.standAnimation();
    }

    addLobbyStatusBar() {
        this.lobbyStatusBar = new LobbyStatusBar(this.game);
        this.lobbyStatusBar.create(this.game.world.centerX, 950);
        this.lobbyStatusBar.setText('Waiting for 3 players');
    }

    addLobbyPlayerBar() {
        this.playerBarOne = new LobbyPlayerBar(this.game, 'aaaaaaaaaaaaaaaaa');
        this.playerBarOne.create(570.5, 850);

        this.playerBarTwo = new LobbyPlayerBar(this.game, 'bbbbbbbbbbbbbbbbb');
        this.playerBarTwo.create(830.5, 850);

        this.playerBarThree = new LobbyPlayerBar(this.game, 'ccccccccccccccccc');
        this.playerBarThree.create(1090.5, 850);

        this.playerBarFour = new LobbyPlayerBar(this.game, 'ddddddddddddddddd');
        this.playerBarFour.create(1350.5, 850);
    }

    /**
     *
     * @param totalPlayers
     * @param room
     * @returns {any}
     */
    setLobbyStatus(totalPlayers) {
        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }

        //return 'WAITING FOR {n} PLAYERS'.replace('{n}', (4 - this.players.length).toString());
    }

    /**
     *
     * @param username
     * @param isOneself
     * @param color
     * @returns {Phaser.Group}
     */
    private addPlayer(username, playerNumber) {
        //return addPlayerHead(this.game, username, this.players, playerNumber);
    }

    fadeOut() {

    }

    shutdown() {

    }

    private addCustomButtons() {

        this.fullScreenButton = new FullScreenButton(this.game, () => {

            if (this.game.scale.isFullScreen) {
                this.game.scale.stopFullScreen();
            } else {
                this.game.scale.startFullScreen(true);
            }

        });
        this.fullScreenButton.create(1415, 55);

        this.soundButton = createSoundButton(this.game);
        this.soundButton.create(1600, 55);

        this.exitButton = new ExitButton(this.game, () => {
            alert('Pressed exit button');
        });
        this.exitButton.create(1787, 55);
    }
}