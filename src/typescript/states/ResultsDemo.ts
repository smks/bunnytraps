import Assets from "../Assets";
import Logs from "../components/Logs/Logs";
import InstructionsBanner from "../components/InstructionsBanner/InstructionsBanner";
import Grid from "../components/Grid/Grid";
import ReadyButton from "../components/Buttons/LargeReadyButton";
import ActionButton from "../components/Buttons/ActionButton";
import BunnyTurnIndicator from "../components/Bunny/BunnyTurnIndicator";
import RandomDataGenerator = Phaser.RandomDataGenerator;
import ScaleManager = Phaser.ScaleManager;
import FullScreenButton from "../components/Buttons/FullScreenButton";
import SoundButton from "../components/Buttons/SoundButton";
import ExitButton from "../components/Buttons/ExitButton";
import TimerIndicator from "../components/Timer/CountdownTimer";
import PointsIndicator from "../components/PointsIndicator/PointsIndicator";
import BunnyPlacer from "../placers/BunnyPlacer";
import TrapPlacer from "../placers/TrapPlacer";
import LargeReadyButton from "../components/Buttons/LargeReadyButton";
import LargeReadOnlyButton from "../components/Buttons/LargeReadOnlyButton";
import Signal = Phaser.Signal;
import Bush from "../components/Cover/Bush";
import bunnyIdByPlayer from '../helpers/bunny-id-by-player';

// Creators
import createGameBackground from '../helpers/create/createGameBackground';
import createInstructionsBanner from '../helpers/create/createInstructionsBanner';
import createBunnyIndicator from '../helpers/create/createBunnyIndicator';
import createLargeReadOnlyButton from '../helpers/create/createLargeReadOnlyButton';
import createFullScreenButton from '../helpers/create/createFullScreenButton';
import createSoundButton from '../helpers/create/createSoundButton';
import createExitButton from '../helpers/create/createExitButton';
import createBush from '../helpers/create/createBush';

import ResultsBuilder from '../builders/ResultsBuilder';
import CPU from '../decisions/CPU';
import Save from '../storage/save';

export class ResultsDemo extends Phaser.State {

    private gameBg: Phaser.Sprite;
    private fullScreenButton: FullScreenButton;
    private soundButton: SoundButton;
    private exitButton: ExitButton;
    private instructionsBanner: InstructionsBanner;
    private grid: Grid;
    private logs: Logs;

    // Remove after demo
    private tempInterval: number;
    private tempInterval2: number;
    private tempInterval3: number;
    private timer: TimerIndicator;
    private pointsIndicator: PointsIndicator;
    private placeTileSignal: Phaser.Signal;
    private readyButton: LargeReadyButton;
    private trapPlacementComplete: Signal;
    private bunny1: BunnyTurnIndicator;
    private bunny2: BunnyTurnIndicator;
    private bunny3: BunnyTurnIndicator;
    private bunny4: BunnyTurnIndicator;
    private bunnies: Array<BunnyTurnIndicator> = new Array();
    private bush: Bush;
    private largeReadOnlyButton: LargeReadOnlyButton;
    private resultsBuilder: ResultsBuilder;
    private moves: Array<{playerId; bunnyId; columnIndex; rowIndex; gotHitByPlayers: Array<number>}>;
    private bunnyPlacement1: BunnyPlacer;
    private bunnyPlacement2: BunnyPlacer;
    private bunnyPlacement3: BunnyPlacer;
    private bunnyPlacement4: BunnyPlacer;
    private save: Save;
    private latestPlayerIdTurn: number = 1;
    private totalMoveCount: number = 0;

    constructor() {
        super();
    }

    create() {

        this.save = new Save();

        // add assets
        this.addBackground();
        this.addInstructionsBanner();
        this.addBunnyTurnIndicators();
        this.addBigActionButton();
        this.addActionButton();
        this.addCountdownTimer();
        this.addMessageBoard();
        this.addLogs();
        this.addGrid();

        this.bush = createBush(this.game, true);

        this.addBunnyPlacers();

        let results = this.getResults();

        this.resultsBuilder = new ResultsBuilder(results);
        
        this.resultsBuilder.createMoves();

        this.moves = this.resultsBuilder.getMoves();

        this.totalMoveCount = this.moves.length;

        this.game.time.events.add(
            Phaser.Timer.SECOND, this.processMove, this
        );
    }

    private getResults() {

        const player1 = this.save.getDecisions(1);
        const cpu2 = new CPU().generateDecisions(2, 2);
        const cpu3 = new CPU().generateDecisions(3, 3);
        const cpu4 = new CPU().generateDecisions(4, 4);

         let results = {
            player1Results: player1,
            player2Results: cpu2,
            player3Results: cpu3,
            player4Results: cpu4,
        };

        console.log(JSON.stringify(results));

        return results;

    }

    private concludeResults() {

        this[`bunny${this.latestPlayerIdTurn}`].moveBackward();

        this.save.roomGameFinished();

        this.bush.close(() => {
            this.game.state.start('Strategy', true);
        });
    }

    private processMove() {

        if (this.moves.length == 0) {
            this.concludeResults();
            return;
        }

        this.pointsIndicator.setMessage(`Bunny ${(this.totalMoveCount - this.moves.length) + 1} / ${this.totalMoveCount}`);

        const move = this.moves.shift();
        const playerId = move.playerId;
        this.latestPlayerIdTurn = playerId;
        const columnIndex = move.columnIndex;
        const rowIndex = move.rowIndex;
        const playersWhoHitBunny: Array<number> = move.gotHitByPlayers;
        const isBeingLaunched: boolean = playersWhoHitBunny.length > 0;

        this.instructionsBanner.changeMessage(`Player ${playerId}'s turn`);

        const {tileXPos, tileYPos} = this.grid.getCoordinatesByIndex(columnIndex, rowIndex);
        this[`bunnyPlacement${playerId}`].addBunny({
            columnIndex,
            rowIndex,
            tileXPos,
            tileYPos
        });

        this.bunnies.map((bunny) => {
           bunny.moveBackward();
        });

        this[`bunny${playerId}`].moveForward(this.save.getBunnyCount(playerId));

        this.game.time.events.add(
            Phaser.Timer.SECOND * 4, 
            this.determineBunnyFate.bind(this, isBeingLaunched, columnIndex, rowIndex, playerId), 
            this
        );
    }

    addLogMessage(message: string) {
        this.logs.addLogMessage(message);
    }

    determineBunnyFate(isBeingLaunched: boolean, columnIndex: number, rowIndex: number, playerId: number) {
        if (isBeingLaunched) {
            this.grid.shootUpTile(columnIndex, rowIndex);

            this[`bunny${playerId}`].reduceCount();

            this.save.reduceBunnyCount(playerId);

            this[`bunnyPlacement${playerId}`].shootBunnyAway(() => {

                this.game.camera.flash(0xf73d2f, 200);

                this.game.time.events.add(
                    Phaser.Timer.SECOND, this.addLogMessage.bind(this, `Player ${playerId} just lost a bunny`), this
                );

                console.log('hit screen');
            }, () => {
                this.processMove();
            });
        } else {
            console.log('avoided trap!');
            this.addLogMessage(`Player ${playerId}'s bunny just survived`);
            this[`bunnyPlacement${playerId}`].survived();
            this.processMove();
        }
    }

    update() {

    }

    render() {
        this.timer.render();
    }

    private addBackground() {
        this.gameBg = createGameBackground(this.game);
    }

    private addInstructionsBanner() {
        this.instructionsBanner = createInstructionsBanner(this.game, 'Results');
    }

    private addBunnyTurnIndicators() {
        this.bunny1 = createBunnyIndicator(this.game, 160.5, 295, bunnyIdByPlayer(1), this.save.getBunnyCount(1));
        this.bunnies.push(this.bunny1);
        this.bunny2 = createBunnyIndicator(this.game, 290.5, 295, bunnyIdByPlayer(2), this.save.getBunnyCount(2));
        this.bunnies.push(this.bunny2);
        this.bunny3 = createBunnyIndicator(this.game, 420.5, 295, bunnyIdByPlayer(3), this.save.getBunnyCount(3));
        this.bunnies.push(this.bunny3);
        this.bunny4 = createBunnyIndicator(this.game, 550.5, 295, bunnyIdByPlayer(4), this.save.getBunnyCount(4));
        this.bunnies.push(this.bunny4);
    }

    private addBigActionButton() {
        this.largeReadOnlyButton = createLargeReadOnlyButton(this.game);
    }

    private addActionButton() {
        this.fullScreenButton = createFullScreenButton(this.game);
        this.soundButton = createSoundButton(this.game);
        this.exitButton = createExitButton(this.game);
    }

    private addGrid() {

        this.placeTileSignal = new Phaser.Signal();

        this.placeTileSignal.add((params) => {
            console.log('Pressed tile');
            console.log(params);
        })

        const placementComplete = new Phaser.Signal();

        placementComplete.add(() => {
            console.log('Tiles finished placement');
        })

        this.grid = new Grid(this.game, null, null, false);
        this.grid.create(856.5, 202.5);
    }

    private addLogs() {
        this.logs = new Logs(this.game, 355, 620);
    }

    private addCountdownTimer() {
        this.timer = new TimerIndicator(this.game, 0, 0);
        this.timer.create(857, 55);
    }

    private addMessageBoard() {
        this.pointsIndicator = new PointsIndicator(this.game);
        this.pointsIndicator.create(1136, 55, `Bunny 1 / ${this.totalMoveCount}`);
    }

    private addBunnyPlacers() {
        this.bunnyPlacement1 = new BunnyPlacer(this.game, null, bunnyIdByPlayer(1));
        this.bunnyPlacement2 = new BunnyPlacer(this.game, null, bunnyIdByPlayer(2));
        this.bunnyPlacement3 = new BunnyPlacer(this.game, null, bunnyIdByPlayer(3));
        this.bunnyPlacement4 = new BunnyPlacer(this.game, null, bunnyIdByPlayer(4));
    }

    shutdown() {

        this.grid.destroy();
        this.instructionsBanner.destroy();
        this.logs.destroy();
    }
}
