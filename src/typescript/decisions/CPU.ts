
import bunnyIdByPlayer from '../helpers/bunny-id-by-player';
import positionsAvailable from '../helpers/positions-available';

/**
 * Created by shaun on 14/01/2017.
 */
export default class CPU {
    private results: { playerId; bunnyId, bunnyPlacements; trapPlacements };
    private availablePositions = positionsAvailable();

    constructor() {
        return this;
    }

    generateDecisions(playerId: number, bunnyId: number) {

        let decisions: { playerId; bunnyId, bunnyPlacements; trapPlacements } = {
            playerId: playerId,
            bunnyId: bunnyId,
            bunnyPlacements: [],
            trapPlacements: [],
        };

        let i = 0;
        while (i < 5) {
            decisions.bunnyPlacements.push(this.getPosition());
            decisions.trapPlacements.push(this.getPosition());
            i++;
        }

        return decisions;
    }

    getPosition() {
        return Phaser.ArrayUtils.removeRandomItem(this.availablePositions);
    }
}