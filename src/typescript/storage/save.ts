export default class Save {

    public static SEEN_TUTORIAL: string = 'seenTutorial';
    public static CURRENT_LEVEL: string = 'currentLevel';
    public static HIGHEST_LEVEL: string = 'highestLevel';
    public static SCORE: string = 'score';
    public static DECISIONS: string = 'decisions';

    savedDataKey: string = '__bunnyTraps';
    savedDataString: string;
    savedData: Object;

    /**
     *
     * @param localStorage
     */
    constructor() {
        this.savedDataString = localStorage.getItem(this.savedDataKey);
        if (this.savedDataString === null) {
            this.savedDataString = this.setDefaultSave();
        }
        this.savedData = JSON.parse(this.savedDataString);
    }

    private setDefaultSave(): string {
        localStorage.setItem(this.savedDataKey, JSON.stringify({
            'roomGamesPlayed': 0,
            'player1': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player2': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player3': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player4': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'currentLevel': 1,
            'currentState': 0,
            'seenTutorial': false
        }))
        return localStorage.getItem(this.savedDataKey);
    }

    load() {
        return this.savedData;
    }

    get(key: string) {
        return this.savedData[key];
    }

    getChild(key: string, child: string) {
        return this.savedData[key][child];
    }

    set(key: string, val: string) {
        this.savedData[key] = val;
        localStorage.setItem(
            this.savedDataKey,
            JSON.stringify(this.savedData)
        );
    }

    setChild(key, child, val) {
        this.savedData[key][child] = val;
        localStorage.setItem(
            this.savedDataKey,
            JSON.stringify(this.savedData)
        );
    }

    /**
     *
     * @param player 'player1'
     * @param options {}
     */
    storeDecisions(playerId: number, decisions) {
        this.setChild(`player${playerId}`, 'decisions', decisions);
    }

    getDecisions(playerId: number) {
        return this.getChild(`player${playerId}`, 'decisions');
    }

    getBunnyCount(playerId) {
        return this.getChild(`player${playerId}`, 'bunniesLeft');
    }

    reduceBunnyCount(playerId: number) {
        let bunnyCount: number = this.getChild(`player${playerId}`, 'bunniesLeft') - 1;
        if (bunnyCount < 0) {
            bunnyCount = 0;
        }
        this.setChild(`player${playerId}`, 'bunniesLeft', bunnyCount);
    }

    roomGameFinished() {
        let gamesPlayed = parseInt(this.get('roomGamesPlayed'), 10);
        if (gamesPlayed > 5) {
            gamesPlayed = 5;
        } else {
            gamesPlayed++;
        }
        this.set('roomGamesPlayed', (gamesPlayed).toString());
    }

    getRoomGamesPlayed() {
        return this.get('roomGamesPlayed');
    }

    static getUser() {
        return localStorage.getItem('__bunnyTrapsClientName');
    }
}