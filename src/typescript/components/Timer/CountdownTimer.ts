/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import Timer = Phaser.Timer;
import Assets from "../../Assets";
import BitmapText = Phaser.BitmapText;
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
import textFieldFactory from './../../helpers/textfield-factory';
import TimerEvent = Phaser.TimerEvent;

export default class CountdownTimer {
    private game: Game;
    private bar: Sprite;
    private textField: BitmapText;
    private group: Group;
    private currentCount: number;
    private timerEvent: TimerEvent;
    private timer: Timer;
    private minutesToCountFrom: number;
    private secondsToCountFrom: number;
    private finalSecondsTriggered: Phaser.Signal;
    private finalSecondIterationTriggered: Phaser.Signal;
    private triggeredFinalSecondsMusic: boolean = false;
    private countdownMusic: Phaser.Sound;

    constructor(game: Game, minutesToCountFrom: number = 1, secondsToCountFrom: number = 30) {
        this.game = game;
        this.minutesToCountFrom = minutesToCountFrom;
        this.secondsToCountFrom = secondsToCountFrom;
        this.currentCount = 0;
    }

    create(xPos: number, yPos: number, autoRun:boolean = false) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.bar = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_TIMER_INDICATOR, this.group);
        this.bar.anchor.set(0.5, 0.5);

        const options = getDefaultTextFieldOptions('00:00');
        options.fontSize = 44;
        options.parent = this.group;
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = this.bar.x;
        this.textField.y = this.bar.y - 10;

        this.timer = this.game.time.create();

        // Create a delayed event 1m and 30s from now
        this.timerEvent = this.timer.add(Phaser.Timer.MINUTE * this.minutesToCountFrom + Phaser.Timer.SECOND * this.secondsToCountFrom, () => {
            this.timer.stop();
            this.textField.text = '00:00';
        }, this);

        if (autoRun) {
            this.start();
        }

        this.finalSecondsTriggered = new Phaser.Signal();
        this.finalSecondIterationTriggered = new Phaser.Signal();
    }

    addFinalSecondsCallback(callback: Function) {
        this.finalSecondsTriggered.add(callback);
    }

    addFinalSecondIteration(callback: Function) {
        this.finalSecondIterationTriggered.add(callback);
    }

    start() {
        if (this.timer.running) {
            return;
        }
        // Start the timer
        this.timer.start();
    }

    stop(clearEvents: boolean = false) {
        this.timer.stop(clearEvents);
    }

    private formatTime(s) {
        const minutes: any = '0' + Math.floor(s / 60);
        const seconds = '0' + (s - minutes * 60);
        return minutes.substr(-2) + ':' + seconds.substr(-2);
    }

    render() {
        if (this.timer.running) {
            const timeMs = this.timerEvent.delay - this.timer.ms;
            const time =  Math.round(timeMs / 1000);

            if (this.triggeredFinalSecondsMusic === false && time === 10) {
                this.triggeredFinalSecondsMusic = true;
                this.onFinalSeconds();
            }

            const diff = timeMs % 1000;

            if (time === 0) {
                this.onFinalSecondIteration();
            }

            const formattedTime = this.formatTime(time);
            this.textField.text = formattedTime;
        }
    }

    onFinalSeconds() {
        this.countdownMusic = this.game.add.audio(Assets.AUDIO_KEY_AUDIO_CLOCK_TICK, 1, false);
        this.countdownMusic.play();
        this.finalSecondsTriggered.dispatch();
    }

    stopMusic() {
        if (this.countdownMusic) {
            this.countdownMusic.stop();
        }
    }

    onFinalSecondIteration() {
        this.finalSecondIterationTriggered.dispatch();
    }

    destroy() {
        this.timer.stop();
        this.timer.destroy();
        this.bar.destroy(true);
        this.finalSecondsTriggered.dispose();
        this.finalSecondIterationTriggered.dispose();
    }
}