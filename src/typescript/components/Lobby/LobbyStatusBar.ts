import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import Assets from "../../Assets";
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
import textFieldFactory from '../../helpers/textfield-factory';

export default class LobbyStatusBar {
    private game: Game;
    private sprite: Sprite;
    private textField: BitmapText;
    private group: Group;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number, yPos: number) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.sprite = this.game.add.sprite(0, 0, Assets.LOBBY_ASSETS, Assets.GRAPHIC_LOBBY_LOBBY_STATUS_BAR, this.group);
        this.sprite.anchor.set(0.5, 0.5);

        const options = getDefaultTextFieldOptions('Waiting for 2 Players');
        options.fontSize = 44;
        options.parent = this.group;
        options.font = Assets.FONT_INVERSE
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = this.sprite.x;
        this.textField.y = this.sprite.y + 6;
        this.textField.anchor.set(0.5, 0.5);
    }

    setText(text: string) {
        this.textField.text = text;
    }

    update() {
        // this.textField.text = '';
    }

    destroy() {
        this.group.destroy(true);
    }
}