import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
import textFieldFactory from '../../helpers/textfield-factory';
import usernameReducer from '../../helpers/username-shortener';

export default class LobbyPlayerBar {

    private game: Game;
    private textField: BitmapText;
    private group: Group;
    private username: string;

    constructor(game: Game, username: string) {
        this.game = game;
        this.username = usernameReducer(username);
    }

    create(xPos: number, yPos: number) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        const options = getDefaultTextFieldOptions(this.username);
        options.fontSize = 60;
        options.parent = this.group;
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = 0;
        this.textField.y = -(20);
        this.textField.anchor.set(0.5, 0.5);
    }

    setText(text: string) {
        this.textField.text = text;
    }

    update() {
        // this.textField.text = '';
    }

    destroy() {
        this.group.destroy(true);
    }
}