import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import Eyes from "./BodyParts/Eyes";
import Body from "./BodyParts/Body";
import Ear from "./BodyParts/Ear";
import Cheek from "./BodyParts/Cheek";
import Face from "./BodyParts/Face";
import Foot from "./BodyParts/Foot";
import Mouth from "./BodyParts/Mouth";
import Paw from "./BodyParts/Paw";
import ArrayUtils = Phaser.ArrayUtils;
import Tween = Phaser.Tween;
import Assets from "../../../Assets";

export default class Bunny {
    private game: Game;
    private group: Group;
    private bodyParts = {
        'body': null,
        'eyes': null,
        'earLeft': null,
        'earRight': null,
        'cheek': null,
        'face': null,
        'mouth': null,
        'pawLeft': null,
        'pawRight': null,
        'footLeft': null,
        'footRight': null,
    };
    private bodyPartOriginalCoordinates = {
        'body': {x: 0, y: 0},
        'eyes': {x: 0, y: -120},
        'earLeft': {x: -157, y: -207},
        'earRight': {x: 157, y: -207},
        'cheek': {x: 0, y: 0},
        'face': {x: 0, y: -58},
        'mouth': {x: 0, y: 90},
        'pawLeft': {x: -250, y: 44},
        'pawRight': {x: 250, y: 44},
        'footLeft': {x: -200, y: 120},
        'footRight': {x: 200, y: 120}
    }
    private bunnyId: number;

    private body: Body;
    private eyes: Eyes; // four frames
    private earLeft: Ear; // create two sprites
    private earRight: Ear; // create two sprites
    private cheek: Cheek;
    private face: Face;
    private mouth: Mouth; // four frames
    private leftPaw: Paw; // create two sprites
    private rightPaw: Paw; // create two sprites
    private leftFoot: Foot;
    private rightFoot: Foot;
    private angleTween: Tween;

    constructor(game: Game, bunnyId: number = 1) {
        this.game = game;
        this.bunnyId = bunnyId;
    }

    create(xPos: number, yPos: number, scale: number = 0.35) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.body = new Body(this.game, this.group);
        this.eyes = new Eyes(this.game, this.group);
        this.earLeft = new Ear(this.game, this.group);
        this.earRight = new Ear(this.game, this.group);
        this.cheek = new Cheek(this.game, this.group);
        this.face = new Face(this.game, this.group);
        this.mouth = new Mouth(this.game, this.group);
        this.leftPaw = new Paw(this.game, this.group);
        this.rightPaw = new Paw(this.game, this.group);
        this.leftFoot = new Foot(this.game, this.group);
        this.rightFoot = new Foot(this.game, this.group);

        // body created first
        this.bodyParts.body = this.body.create(0, 0, this.bunnyId);
        this.bodyParts.eyes = this.eyes.create(0, -120, this.bunnyId);

        const randomDelay = ArrayUtils.getRandomItem([3000, 3500, 4000, 4500, 5000]);

        this.bodyParts.earLeft = this.earLeft.create(-157, -207, this.bunnyId, randomDelay);
        this.bodyParts.earRight = this.earRight.create(157, -207, this.bunnyId, this.earLeft.TYPE_RIGHT, randomDelay);
        this.bodyParts.face = this.face.create(0, -58, this.bunnyId);
        this.bodyParts.mouth = this.mouth.create(0, 90, this.bunnyId);
        this.bodyParts.pawLeft = this.leftPaw.create(-250, 44, this.bunnyId);
        this.bodyParts.pawRight = this.rightPaw.create(250, 44, this.bunnyId, this.leftPaw.TYPE_RIGHT);
        this.bodyParts.footLeft = this.leftFoot.create(-200, 160, this.bunnyId);
        this.bodyParts.footRight = this.rightFoot.create(200, 160, this.bunnyId, this.leftFoot.TYPE_RIGHT);

        this.group.sendToBack(this.bodyParts.earLeft);
        this.group.sendToBack(this.bodyParts.earRight);

        this.group.sendToBack(this.bodyParts.footLeft);
        this.group.sendToBack(this.bodyParts.footRight);

        this.group.bringToTop(this.bodyParts.eyes);

        this.group.scale.set(scale, scale);
    }

    resetBodyParts() {

        this.bodyParts.body.x = this.bodyPartOriginalCoordinates.body.x;
        this.bodyParts.body.y = this.bodyPartOriginalCoordinates.body.y;

        this.bodyParts.earLeft.x = this.bodyPartOriginalCoordinates.earLeft.x;
        this.bodyParts.earLeft.y = this.bodyPartOriginalCoordinates.earLeft.y;

        this.bodyParts.earRight.x = this.bodyPartOriginalCoordinates.earRight.x;
        this.bodyParts.earRight.y = this.bodyPartOriginalCoordinates.earRight.y;

        this.bodyParts.face.x = this.bodyPartOriginalCoordinates.face.x;
        this.bodyParts.face.y = this.bodyPartOriginalCoordinates.face.y;

        this.bodyParts.mouth.x = this.bodyPartOriginalCoordinates.mouth.x;
        this.bodyParts.mouth.y = this.bodyPartOriginalCoordinates.mouth.y;

        this.bodyParts.pawLeft.x = this.bodyPartOriginalCoordinates.pawLeft.x;
        this.bodyParts.pawLeft.y = this.bodyPartOriginalCoordinates.pawLeft.y;

        this.bodyParts.pawRight.x = this.bodyPartOriginalCoordinates.pawRight.x;
        this.bodyParts.pawRight.y = this.bodyPartOriginalCoordinates.pawRight.y;

        this.bodyParts.footLeft.x = this.bodyPartOriginalCoordinates.footLeft.x;
        this.bodyParts.footLeft.y = this.bodyPartOriginalCoordinates.footLeft.y;

        this.bodyParts.footRight.x = this.bodyPartOriginalCoordinates.footRight.x;
        this.bodyParts.footRight.y = this.bodyPartOriginalCoordinates.footRight.y;

        this.bodyParts.eyes.x = this.bodyPartOriginalCoordinates.eyes.x;
        this.bodyParts.eyes.y = this.bodyPartOriginalCoordinates.eyes.y;
    }

    getSprite() {
        return this.group;
    }

    destroy() {
        this.group.destroy(true);
    }

    standAnimation(makeSound: boolean = false) {

        if (makeSound) {
            const randomSound = this.game.rnd.integerInRange(1, 5);
            this.game.add.sound(Assets[`AUDIO_KEY_AUDIO_BUNNY_SOUND_0${randomSound}`], 1, false)
                .play();
        }

        this.blinkAnimation();
        this.mouthAnimation();
        this.wiggleEars();
    }

    dropDown(onComplete?: Function, dropDistance: number = 100) {

        if (!onComplete) {
            onComplete = this.standAnimation.bind(this, true);
        }

        const timer = this.game.time.create();

        timer.add(Phaser.Timer.SECOND / 4, () => {
            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_TILE_LAND, 1, false)
                .play();
        }, this);

        timer.start();

        const originalY = this.group.y;
        const startingY = originalY - dropDistance;
        this.group.y = startingY;
        const dropDownTween = this.game.add.tween(this.group)
            .to({y: originalY}, 1000, Phaser.Easing.Bounce.Out, false);
        dropDownTween.onComplete.add(onComplete);

        this.eyes.shutEyes();
        this.mouth.close();
        this.body.fallDown();
        this.leftPaw.airToSide();
        this.rightPaw.airToSide();
        this.leftFoot.land();
        this.rightFoot.land();

        dropDownTween.start();
    }

    jumpFade(onComplete: Function) {

        const originalY = this.group.y;
        const finishY = originalY - 50;
        this.group.y = originalY;
        const hopUpTween = this.game.add.tween(this.group)
            .to({y: finishY, alpha: 0}, 500, Phaser.Easing.Circular.InOut, false);

        this.eyes.shutEyes();
        this.mouth.close();

        hopUpTween.onComplete.add(onComplete);

        hopUpTween.start();
    }

    scurry() {
        this.leftFoot.move();
        this.rightFoot.move();
    }

    private blinkAnimation() {
        this.eyes.blink();
    }

    private mouthAnimation() {
        this.mouth.move();
    }

    private wiggleEars() {
        this.earLeft.wiggle();
        this.earRight.wiggle();
    }

    shootToScreen(onHit: Function, onComplete: Function) {

        this.eyes.spinEyes();
        this.mouth.worry();
        this.body.stretch();
        this.leftPaw.flap();
        this.rightPaw.flap();
        this.leftFoot.flap();
        this.rightFoot.flap();

        this.game.add.sound(Assets.AUDIO_KEY_AUDIO_TILE_SPRING, 1, false)
            .play();

        const randomSound = this.game.rnd.integerInRange(1, 5);

        this.game.add.sound(Assets[`AUDIO_KEY_AUDIO_BUNNY_FLYING_0${randomSound}`], 1, false)
            .play();

        const shootAway = this.game.add.tween(this.group.scale)
            .to({x: 2, y: 2}, 2000, Phaser.Easing.Circular.In, false);

        this.angleTween = null;

        const spinCount = ArrayUtils.getRandomItem([0, 1, 2]);

        if (spinCount > 0) {

            const durationPerSpin = (Math.floor(2000 / spinCount));

            this.angleTween = this.game.add.tween(this.group)
                .to({angle: 360}, durationPerSpin,
                    Phaser.Easing.Linear.None, true, 0, spinCount);
        }

        const showingGlass = Phaser.Utils.chanceRoll(50);
        let glassCrack = null;
        if (showingGlass) {
            const randomGlass = this.game.rnd.integerInRange(1, 3);
            glassCrack = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets[`GRAPHIC_GAME_GLASS_${randomGlass}`]);
            glassCrack.anchor.set(0.5, 0.5);
            glassCrack.alpha = 0;
            glassCrack.angle = this.game.rnd.integerInRange(0, 359);
        }

        shootAway.onComplete.add(() => {

            if (this.angleTween) {
                this.game.tweens.remove(this.angleTween);
            }

            this.game.camera['shake'](0.05, 200);

            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUNNY_HITS_SCREEN, 1, false)
                .play();

            if (glassCrack !== null) {
                glassCrack.position.set(this.group.position.x, this.group.position.y);
                glassCrack.alpha = 1;
                this.game.add.tween(glassCrack)
                    .to({alpha: 0}, 2000, null, true, 2000)
                    .onComplete.add(() => {
                        glassCrack.destroy();
                    });
            }

            this.eyes.hitEyes();
            this.mouth.hit();
            this.leftPaw.stopFlapping();
            this.rightPaw.stopFlapping();
            this.leftFoot.stopFlapping();
            this.rightFoot.stopFlapping();

            this.slideDownScreen(onComplete);
        });
        shootAway.onComplete.add(onHit);
        shootAway.start();
    }

    slideDownScreen(onComplete) {

        const timer = this.game.time.create();

        timer.add(Phaser.Timer.SECOND / 2, () => {
            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUNNY_SLIDE, 1, false)
                .play();
        }, this);

        timer.start();

        const tween = this.game.add.tween(this.group)
            .to({y:(this.game.world.height + this.group.height)}, 2000,
                Phaser.Easing.Exponential.InOut, false, 200);
        tween.onComplete.add(onComplete);
        tween.start();
    }
}