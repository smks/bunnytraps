/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import Assets from "../../../../Assets";

export default class Body {
    private game: Game;
    private sprite: Sprite;
    private group: Group;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1) {
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets[`GRAPHIC_BUNNIES_BUNNY_${bunnyId}_BODY`], this.group);
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    }

    fallDown() {
        this.sprite.scale.x = 0.8;
        this.sprite.scale.y = 1.2;
        const tween = this.game.add.tween(this.sprite.scale)
            .to({x: 1, y: 1}, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    }

    stretch() {
        const tween = this.game.add.tween(this.sprite.scale)
            .to({x: 1.1, y: 1}, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    }

    getCenterPosition() {
        return this.sprite.position;
    }

    destroy() {
        this.sprite.destroy(true);
    }
}