/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;

export default class Foot {
    private game: Game;
    private sprite: Sprite;

    TYPE_LEFT: number = 1;
    TYPE_RIGHT: number = 2;

    private group: Group;
    private type: number;
    private originalX: number;
    private originalY: number;
    private currentTween: Phaser.Tween;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1, type: number = this.TYPE_LEFT) {
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets[`GRAPHIC_BUNNIES_BUNNY_${bunnyId}_FOOT`], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        this.originalX = this.sprite.x;
        this.originalY = this.sprite.y;
        return this.sprite;
    }

    destroy() {
        this.sprite.destroy(true);
    }

    land() {
        const originalX = this.sprite.x;
        const originalY = this.sprite.y;
        const originalAngle = 0;
        const startingY = originalY + 150;
        const startingX = originalX + ((this.type == this.TYPE_RIGHT) ? -80 : 80);
        const startingAngle = (this.type == this.TYPE_RIGHT) ? -20 : 20;
        this.sprite.x = startingX;
        this.sprite.y = startingY;
        this.sprite.angle = startingAngle;
        const tween = this.game.add.tween(this.sprite)
            .to({x: originalX, y: originalY, angle: originalAngle}, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    }


    move() {
        const topY = this.originalY - 30;
        const bottomY = this.originalY + 30;
        let fromYPos: number;
        let toYPos: number;

        if (this.type == this.TYPE_LEFT) {
            this.sprite.y = topY;
            fromYPos = topY;
            toYPos = bottomY;
        } else {
            this.sprite.y = bottomY;
            fromYPos = bottomY;
            toYPos = topY;
        }

        const tween = this.game.add.tween(this.sprite)
            .to({y: toYPos}, 200, Phaser.Easing.Quadratic.In)
            .to({y: fromYPos}, 200, Phaser.Easing.Quadratic.Out)
            .to({y: this.originalY}, 200, Phaser.Easing.Quadratic.Out);

        tween.start();
    }

    flap() {
        const DISTANCE = 35;

        let originalY = this.sprite.y;
        let startingY;
        let endY;

        if (this.type == this.TYPE_LEFT) {
            startingY = originalY - DISTANCE;
            endY = originalY + DISTANCE;
        } else {
            startingY = originalY + DISTANCE;
            endY = originalY - DISTANCE;
        }

        this.sprite.y = startingY;

        this.currentTween = this.game.add.tween(this.sprite)
            .to({y: endY}, 100, Phaser.Easing.Linear.None, true, 0, 20, true);
    }

    stopFlapping() {
        this.game.tweens.remove(this.currentTween);
    }
}