/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;
import ArrayUtils = Phaser.ArrayUtils;
import RandomDataGenerator = Phaser.RandomDataGenerator;

export default class Ear {
    private game: Game;
    private sprite: Sprite;
    private group: Group;
    private type: number;
    private tween: Phaser.Tween;
    private yPos: number;

    TYPE_LEFT: number = 1;
    TYPE_RIGHT: number = 2;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1, type: number = this.TYPE_LEFT, wiggleDelay: number = 4000) {
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets[`GRAPHIC_BUNNIES_BUNNY_${bunnyId}_EAR`], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 1);
        this.addAnimations(wiggleDelay);
        return this.sprite;
    }

    wiggle() {
        this.tween.start();
    }

    stopWiggle() {
        this.tween.stop();
    }

    destroy() {
        this.sprite.destroy(true);
    }

    private addAnimations(wiggleDelay: number) {
        this.addWiggle(wiggleDelay);
    }

    private addWiggle(wiggleDelay) {
        const angle = (this.type == this.TYPE_RIGHT) ? -8 : 8;

        this.tween = this.game.add
            .tween(this.sprite)
            .to({angle: angle}, 100, Phaser.Easing.Elastic.InOut, false, wiggleDelay);

        this.tween.yoyo(true);

        this.tween.onComplete.add(() => {
            this.tween.start(0);
        });
    }
}