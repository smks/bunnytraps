/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;
import ArrayUtils = Phaser.ArrayUtils;

export default class Mouth {
    private game: Game;
    private sprite: Sprite;

    private group: Group;
    private bunnyId: number;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1) {
        this.bunnyId = bunnyId;
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_OPEN, this.group);
        this.sprite.anchor.set(0.5, 0.5);
        this.addAnimations();
        return this.sprite;
    }

    move() {
        this.sprite.animations.play('move');
    }

    panic() {
        this.sprite.animations.play('panic');
    }

    worry() {
        this.sprite.animations.play('worry');
    }

    hit() {
        this.sprite.animations.play('hit');
    }

    private addAnimations() {
        this.addHappyMouth();
        this.addWorryMouth();
        this.addPanicMouth();
        this.addHitMouth();
    }

    private addHappyMouth() {
        let frames = Array.apply(null, Array(36));
        frames.forEach((item, i) => {
            if (i >= (frames.length / 2)) {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_OPEN;
            } else {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_CLOSED;
            }
        })

        ArrayUtils.shuffle(frames);
        this.sprite.animations.add('move', frames, 1, false);
    }

    private addWorryMouth() {
        let frames = Array.apply(null, Array(12));
        frames.forEach((item, i) => {
            if (i <= 2) {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_1;
            } else {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_2;
            }
        })

        this.sprite.animations.add('worry', frames, 12, false);
    }

    private addPanicMouth() {
        let frames = Array.apply(null, Array(12));
        frames.forEach((item, i) => {
            if (i == 0) {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC;
            } else {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC_CLOSED;
            }
        })

        this.sprite.animations.add('panic', frames, 12, true);
    }

    private addHitMouth() {
        let frames = Array.apply(null, Array(10));
        frames.forEach((item, i) => {
            if (i == 0) {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_1;
            } else {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_2;
            }
        })

        this.sprite.animations.add('hit', frames, 12, false);
    }

    destroy() {
        this.sprite.destroy(true);
    }

    close() {
        this.sprite.frameName = Assets.GRAPHIC_BUNNIES_BUNNY_MOUTH_CLOSED;
    }
}