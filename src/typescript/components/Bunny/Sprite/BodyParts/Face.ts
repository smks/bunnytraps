/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;

export default class Paw {
    private game: Game;
    private sprite: Sprite;

    private TYPE_LEFT: number = 1;
    private TYPE_RIGHT: number = 1;

    private group: Group;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1, type: number = this.TYPE_LEFT) {
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets[`GRAPHIC_BUNNIES_BUNNY_${bunnyId}_FACE`], this.group);
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    }

    destroy() {
        this.sprite.destroy(true);
    }
}