/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;
import ArrayUtils = Phaser.ArrayUtils;

export default class Eyes {
    private game: Game;
    private sprite: Sprite;
    private leftRollEye: Sprite;
    private rightRollEye: Sprite;
    private group: Group;
    private eyeGroup: Group;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1) {

        this.eyeGroup = this.game.add.group(this.group);

        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets.GRAPHIC_BUNNIES_BUNNY_EYES_OPEN, this.eyeGroup);
        this.leftRollEye = this.game.add.sprite(xPos - 110, yPos + 20, Assets.BUNNIES_ASSETS, Assets.GRAPHIC_BUNNIES_BUNNY_EYES_ROLL, this.eyeGroup);
        this.rightRollEye = this.game.add.sprite(xPos + 110, yPos + 20, Assets.BUNNIES_ASSETS, Assets.GRAPHIC_BUNNIES_BUNNY_EYES_ROLL, this.eyeGroup);
        this.rightRollEye.scale.set(-1, 1);

        this.leftRollEye.anchor.set(0.5, 0.5);
        this.rightRollEye.anchor.set(0.5, 0.5);
        this.sprite.anchor.set(0.5, 0.5);

        this.addAnimations();

        this.leftRollEye.visible = false;
        this.rightRollEye.visible = false;

        return this.eyeGroup;
    }

    destroy() {
        this.sprite.destroy(true);
    }

    blink() {
       this.sprite.animations.play('blink');
    }

    shutEyes() {
        this.sprite.animations.stop('blink');
        this.sprite.frameName = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_CLOSED;
    }

    spinEyes() {
        this.sprite.animations.stop('blink');

        this.sprite.visible = false;
        this.leftRollEye.visible = true;
        this.rightRollEye.visible = true;

        this.game.add.tween(this.leftRollEye)
            .to({angle: -360}, 400, Phaser.Easing.Linear.None, true, 0, 4);

        this.game.add.tween(this.rightRollEye)
            .to({angle: 360}, 400, Phaser.Easing.Linear.None, true, 0, 4);
    }

    sadEyes() {
        this.sprite.frameName = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_SAD;
    }

    worryEyes() {
        this.sprite.frameName = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_WORRY;
    }

    hitEyes() {
        this.leftRollEye.visible = false;
        this.rightRollEye.visible = false;
        this.sprite.visible = true;
        this.sprite.frameName = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_HIT;
    }

    private addAnimations() {
        this.addBlinkAnimation();
        this.addHitAnimation();
    }

    private addBlinkAnimation() {
        let frames = Array.apply(null, Array(36));
        frames.forEach((item, i) => {
            if (i >= (frames.length - 2)) {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_CLOSED;
            } else {
                frames[i] = Assets.GRAPHIC_BUNNIES_BUNNY_EYES_OPEN;
            }
        })

        ArrayUtils.shuffle(frames);
        this.sprite.animations.add('blink', frames, 12, true);
    }

    private addHitAnimation() {
        this.sprite.animations.add('hit', [Assets.GRAPHIC_BUNNIES_BUNNY_EYES_HIT], 1, true);
    }
}