/**
 * Created by shaun on 07/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../../../Assets";
import Group = Phaser.Group;
import Tween = Phaser.Tween;

export default class Paw {
    private game: Game;
    private sprite: Sprite;
    private currentTween: Tween;

    TYPE_LEFT: number = 1;
    TYPE_RIGHT: number = 2;

    private group: Group;
    private type: number;

    constructor(game: Game, group: Group) {
        this.game = game;
        this.group = group;
    }

    create(xPos: number, yPos: number, bunnyId: number = 1, type: number = this.TYPE_LEFT) {
        this.sprite = this.game.add.sprite(xPos, yPos, Assets.BUNNIES_ASSETS, Assets[`GRAPHIC_BUNNIES_BUNNY_${bunnyId}_PAW`], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    }

    destroy() {
        this.sprite.destroy(true);
    }

    flap() {
        const originalY = this.sprite.y;
        const startingY = originalY - 100;
        const endY = originalY + 100;

        this.sprite.y = startingY;
        const startingAngle = (this.type == this.TYPE_RIGHT) ? -90 : 90;

        this.currentTween = this.game.add.tween(this.sprite)
            .to({y: endY, angle: startingAngle}, 100, Phaser.Easing.Linear.None, true, 0, 20, true);
    }

    airToSide() {
        const originalY = this.sprite.y;
        const originalAngle = 0;
        const startingY = originalY - 400;
        const startingAngle = (this.type == this.TYPE_RIGHT) ? -90 : 90;
        this.sprite.y = startingY;
        this.sprite.angle = startingAngle;
        const tween = this.game.add.tween(this.sprite)
            .to({y: originalY, angle: originalAngle}, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    }

    stopFlapping() {
        this.game.tweens.remove(this.currentTween);
    }
}