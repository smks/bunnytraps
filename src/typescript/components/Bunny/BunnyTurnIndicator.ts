/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import Assets from "../../Assets";
import BitmapText = Phaser.BitmapText;
import textFieldFactory from './../../helpers/textfield-factory';
import {getDefaultTextFieldOptions} from './../../helpers/textfield-factory';
import Bunny from "./Sprite/Bunny";

export default class BunnyTurnIndicator {

    private DISTANCE: number = 30;
    private DEFAULT_BUNNIES: number = 25;
    private SPEED: number = 400;

    private game: Game;
    private group: Group;
    private bunny: Group;
    private card: Sprite;
    private cardText: BitmapText;
    private bunnyId: number;
    private isForward: boolean = false;
    private originalYPosition: number;
    private bunnyController: Bunny;
    private bunnyCount: number|null;

    constructor(game: Game, bunnyId: number = 1) {
        this.game = game;
        this.bunnyId = bunnyId;
    }

    create(xPos: number, yPos: number, bunnyCount: number = 25) {

        this.group = this.game.add.group();

        this.bunnyController = new Bunny(this.game, this.bunnyId);
        this.bunnyController.create(xPos, yPos, 0.2);
        this.bunnyController.standAnimation();
        this.bunny = this.bunnyController.getSprite();
        this.originalYPosition = yPos;

        this.card = this.game.add.sprite(xPos, yPos, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_BUNNY_CARD);
        this.card.anchor.set(0.5, 0.5);
        this.card.y = this.bunny.height + 190;

        this.group.add(this.bunny);
        this.group.add(this.card);

        const options = getDefaultTextFieldOptions(bunnyCount.toString());
        options.fontSize = 40;
        options.parent = this.group;
        this.cardText = textFieldFactory(this.game, options);
        this.cardText.x = this.card.x;
        this.cardText.y = this.card.y - 10;

        this.card.scale.y = 0;
        this.cardText.scale.y = 0;
    }

    reduceCount() {
        let newCount = parseInt(this.cardText.text, 10) - 1;
        if (newCount < 0) {
            newCount = 0;
        }
        this.cardText.text = newCount.toString();
    }

    // It is this bunnies turn to show card with number of bunnies
    moveForward(numberOfBunnies: number = 25) {

        if (this.isForward) {
            return;
        }

        const revealCard = this.game.add.sound(Assets.AUDIO_KEY_AUDIO_REVEAL_CARD, 1, false)
            .play();

        revealCard.onStop.addOnce(() => {
            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_CHANGE_PLAYER, 1, false)
                .play();
        })

        this.isForward = true;

        if (this.bunnyCount == null) {
            this.bunnyCount = numberOfBunnies;
            this.cardText.text = numberOfBunnies.toString();
        }

        this.bunnyController.scurry();

        this.game.add.tween(this.bunny)
            .to({y: (this.bunny.y + this.DISTANCE)}, this.SPEED, Phaser.Easing.Circular.InOut, true);

        this.game.add.tween(this.card.scale)
            .to({y: 1}, this.SPEED, Phaser.Easing.Circular.InOut, true);

        this.game.add.tween(this.cardText.scale)
            .to({y: 1}, this.SPEED, Phaser.Easing.Circular.InOut, true);
    }

    moveBackward() {

        if (!this.isForward) {
            return;
        }

        this.isForward = false;

        this.bunnyController.scurry();

        this.game.add.tween(this.bunny)
            .to({y: this.originalYPosition}, this.SPEED, Phaser.Easing.Circular.InOut, true);

        this.game.add.tween(this.card.scale)
            .to({y: 0}, this.SPEED, Phaser.Easing.Circular.InOut, true);

        this.game.add.tween(this.cardText.scale)
            .to({y: 0}, this.SPEED, Phaser.Easing.Circular.InOut, true);
    }

    destroy() {
        this.bunny.destroy(true);
    }
}