import Game = Phaser.Game;
import textFieldFactory from '../../helpers/textfield-factory';
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
/**
 * Created by shaun on 31/12/2016.
 */
export default class PercentageLoader {
    private game: Phaser.Game;
    private percentageTextField: Phaser.BitmapText;
    private percentage: number;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number, yPos: number) {
        const options = this.getTextFieldOptions();
        options.xPos = xPos;
        options.yPos = yPos;
        options.fontSize = 70;
        options.parent = this.game.world;
        this.percentageTextField = textFieldFactory(this.game, options);
    }

    private getTextFieldOptions() {
        let options = getDefaultTextFieldOptions('0%');
        return options;
    }

    /**
     * @loopEvent Called every {timeToWait} seconds
     */
    setPercentage(percentage: number) {
        this.percentage = percentage;
        this.percentageTextField.text = percentage.toString() + '%';
        console.log('setting percentage: ' + percentage);
    }

    destroy() {
        this.percentageTextField.destroy(true);
    }
}
