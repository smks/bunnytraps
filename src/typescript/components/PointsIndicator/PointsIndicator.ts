import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import Assets from "../../Assets";
import BitmapText = Phaser.BitmapText;
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
import textFieldFactory from './../../helpers/textfield-factory';

export default class PointsIndicator {

    private game: Game;
    private sprite: Sprite;
    private textField: BitmapText;
    private group: Group;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number, yPos: number, message: string = '') {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.sprite = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_POINTS_INDICATOR, this.group);
        this.sprite.anchor.set(0.5, 0.5);

        const options = getDefaultTextFieldOptions(message);
        options.fontSize = 44;
        options.parent = this.group;
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = this.sprite.x;
        this.textField.y = this.sprite.y - 10;
        this.textField.anchor.set(0.5, 0.5);
    }

    setMessage(message: string) {
        this.textField.text = message;
    }

    destroy() {
        this.group.destroy(true);
    }
}