/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Group = Phaser.Group;
import Sprite = Phaser.Sprite;
import Signal = Phaser.Signal;
import Assets from "../../Assets";

export default class Grid {

    private COLUMNS = 6;
    private ROWS = 5;
    private SIZE = 186;
    private ANIMATE_IN: boolean = true;
    private DELAY_INCREMENT: number = 150;

    private game: Game;
    private tileMap: Array<any>;
    private pressedTile: Signal;
    private placementComplete:Phaser.Signal;
    private group: Phaser.Group;
    private delayOfPlacement: number = 0;
    private tileTween: Phaser.Tween;

    constructor(game: Game, pressedTile?: Signal, placementComplete?: Signal, animateIn: boolean = true) {
        this.game = game;
        this.pressedTile = pressedTile;
        this.placementComplete = placementComplete;
        this.ANIMATE_IN = animateIn;
        this.tileMap = new Array();
    }

    create(xPos: number, yPos: number) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.createColumns();
    }

    addInteraction() {

        if (this.tileMap.length == 0) {
            throw new Error('Create the grid first before adding interaction!');
        }

        for (let columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (let rowIndex: number = 0; rowIndex < this.ROWS; rowIndex++) {
                const option = this.tileMap[columnIndex][rowIndex];
                const tile = option.tile;
                this.addTileEvents(tile, {columnIndex, rowIndex});
            }
        }
    }

    removeInteraction() {
        for (let columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (let tileIndex: number = 0; tileIndex < this.ROWS; tileIndex++) {
                const option = this.tileMap[columnIndex][tileIndex];
                const tile = option.tile;
                this.removeTileEvents(tile);
            }
        }
    }

    shootUpTile(columnIndex: number, rowIndex: number) {
        const option = this.tileMap[columnIndex][rowIndex];
        const tile = option.tile;
        this.group.bringToTop(tile);
        const tween = this.game.add.tween(tile.scale)
            .to({x: 1.2, y: 1.2}, 100, Phaser.Easing.Exponential.Out, false, 1)
            .chain(this.game.add.tween(tile.scale)
                .to({x: 1, y: 1}, 100, Phaser.Easing.Exponential.InOut, false, 1));

        tween.start();
    }

    private addTileEvents(tile: Sprite, params) {
        tile.inputEnabled = true;
        tile.events.onInputOver.add(this.onInputOver, this);
        tile.events.onInputOut.add(this.onInputOut, this);
        tile.events.onInputDown.add(this.onInputDown, this, 0, params);
    }

    private removeTileEvents(tile: Sprite) {
        tile.inputEnabled = false;
        tile.events.onInputOver.remove(this.onInputOver, this);
        tile.events.onInputOut.remove(this.onInputOut, this);
        tile.events.onInputDown.remove(this.onInputDown, this);
    }

   private  createColumns() {
        for (let columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            this.tileMap[columnIndex] = [];
            this.delayOfPlacement += this.DELAY_INCREMENT;
            this.createRow(columnIndex);
        }
    }

    private createRow(columnIndex) {
        for (let rowIndex: number = 0; rowIndex < this.ROWS; rowIndex++) {
            this.delayOfPlacement += this.DELAY_INCREMENT;
            let tileSprite = this.createTile();
            let xPos = 0;
            let yPos = 0;
            if (columnIndex > 0) {
                xPos = (columnIndex * this.SIZE);
            }
            if (rowIndex > 0) {
                yPos = (rowIndex * this.SIZE);
            }
            tileSprite.x = xPos;
            tileSprite.y = yPos;

            if (this.ANIMATE_IN) {
                this.animateIn(tileSprite);
            }

            this.tileMap[columnIndex][rowIndex] = {
                'tile': tileSprite,
                'isActive': false,
                'isHovered': false
            };
        }
        this.delayOfPlacement -= (this.DELAY_INCREMENT * this.ROWS);
    }

    private animateIn(tileSprite: Phaser.Sprite) {
        tileSprite.alpha = 0;
        tileSprite.scale.set(1.5, 1.5);
        this.game.add.tween(tileSprite.scale)
            .to({x: 1, y: 1}, 500, Phaser.Easing.Circular.InOut, true, this.delayOfPlacement);
        this.game.add.tween(tileSprite)
            .to({alpha: 1}, 250, Phaser.Easing.Circular.InOut, true, this.delayOfPlacement);
    }

    private createTile() {
        let tile = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_GRID_TILE, this.group);
        tile.anchor.set(0.5, 0.5);
        return tile;
    }

    // Event handlers

    private onInputOut(tile) {
        tile.scale.set(1, 1);
    }

    private onInputOver(tile) {
        this.group.bringToTop(tile);
        this.game.tweens.remove(this.tileTween);
        this.tileTween = this.game.add.tween(tile.scale).to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Exponential.Out, true);
    }

    private onInputDown(e) {
        const tile:Phaser.Sprite = e;
        const args = arguments[2];
        this.game.tweens.remove(this.tileTween);
        tile.scale.set(1, 1);
        this.pressedTile.dispatch({
            columnIndex: args.columnIndex,
            rowIndex: args.rowIndex,
            tileXPos: this.group.x + tile.x,
            tileYPos: this.group.y + tile.y
        });
    }

    destroy() {
        for (let columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (let tileIndex: number = 0; tileIndex < this.ROWS; tileIndex++) {
                const option = this.tileMap[columnIndex][tileIndex];
                const tile = option.tile;
                this.removeTileEvents(tile);
                tile.destroy(true);
            }
        }
    }

    getCoordinatesByIndex(columnIndex: number, rowIndex: number) {
        const option = this.tileMap[columnIndex][rowIndex];
        const tile = option.tile;
        return {
            tileXPos: this.group.x + tile.x,
            tileYPos: this.group.y + tile.y
        };
    }
}