import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import Assets from "../../Assets";
import {getDefaultTextFieldOptions} from "../../helpers/textfield-factory";
import textFieldFactory from '../../helpers/textfield-factory';

export default class BushSectionFactory {
    private game: Game;

    private portion1: Sprite;
    private portion2: Sprite;
    private portion3: Sprite;
    private portion4: Sprite;
    private portion5: Sprite;
    private portion6: Sprite;
    private portion7: Sprite;
    private portion8: Sprite;
    private portion9: Sprite;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number, yPos: number, mirror: boolean = false) {

        const group = this.game.add.group();
        group.x = xPos;
        group.y = yPos;

        this.portion1 = this.game.add.sprite(-309, -131, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion2 = this.game.add.sprite(-218, 244, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion3 = this.game.add.sprite(-126, 666, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion4 = this.game.add.sprite(-35, -131, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion5 = this.game.add.sprite(57, 244, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion6 = this.game.add.sprite(148, 666, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion7 = this.game.add.sprite(239, -131, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion8 = this.game.add.sprite(331, 244, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);
        this.portion9 = this.game.add.sprite(422, 666, Assets.BUSH_ASSETS, Assets.GRAPHIC_BUSH, group);

        if (mirror) {
            group.scale.set(-1, -1);
        }

        return group;
    }
}