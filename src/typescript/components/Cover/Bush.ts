import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import BushSection from "./BushSectionFactory";
import Assets from "../../Assets";
import ArrayUtils = Phaser.ArrayUtils;

export default class Bush {
    private game: Game;
    private textField: BitmapText;
    private group: Group;
    private leftBush: Group;
    private rightBush: Group;
    private originalLeftX: number;
    private originalLeftY: number;
    private originalRightX: number;
    private originalRightY: number;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number = 0, yPos: number = 0) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        const bushSectionFactory = new BushSection(this.game);

        this.leftBush = bushSectionFactory.create(0, 0);

        let mirror = true;
        this.rightBush = bushSectionFactory.create(0, 0, mirror);
        this.rightBush.x = this.game.world.width;
        this.rightBush.y = this.game.world.height;

        this.group.add(this.leftBush);
        this.group.add(this.rightBush);

        this.originalLeftX = this.leftBush.x;
        this.originalLeftY = this.leftBush.y;
        this.originalRightX = this.rightBush.x;
        this.originalRightY = this.rightBush.y;
    }

    open(onComplete: Function) {

        this.leftBush.x = this.originalLeftX;
        this.rightBush.x = this.originalRightX;

        const leftXPos = (this.originalLeftX - this.leftBush.width);
        const rightXPos = (this.originalRightX + -(this.rightBush.width));

        const onOpened = () => {
            onComplete();
        }

        this.tween(leftXPos, rightXPos, onOpened);
    }

    close(onComplete: Function) {

        this.leftBush.x = (this.originalLeftX - this.leftBush.width);
        this.rightBush.x = (this.originalRightX + -(this.rightBush.width));

        const leftXPos = this.originalLeftX;
        const rightXPos = this.originalRightX;

        const onClose = () => {

            this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUSH_COLLIDE, 1, false)
                .play();

            onComplete();
        }

        this.tween(leftXPos, rightXPos, onClose);
    }

    tween(leftX: number, rightX: number, onComplete: Function) {
        const leftTween = this.game.add
            .tween(this.leftBush)
            .to({x: leftX}, 600, Phaser.Easing.Quartic.InOut, false);

        const rightTween = this.game.add
            .tween(this.rightBush)
            .to({x: rightX}, 600, Phaser.Easing.Quartic.InOut, false);

        leftTween.onComplete.add(onComplete);

        leftTween.start();
        rightTween.start();
    }

    getSprite() {
        return this.group;
    }

    destroy() {
        this.group.destroy(true);
    }
}