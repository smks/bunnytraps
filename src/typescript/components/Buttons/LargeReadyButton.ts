/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import Assets from "../../Assets";
import textFieldFactory from './../../helpers/textfield-factory';
import {getDefaultTextFieldOptions} from './../../helpers/textfield-factory';

export default class LargeReadyButton {
    private game: Game;
    private button: Sprite;
    private inputDown: Function;
    private textField: BitmapText;
    private group: Group;

    constructor(game: Game, inputDown) {
        this.game = game;
        this.inputDown = inputDown;
    }

    setOnClick(inputDown) {
        this.inputDown = inputDown;
    }

    create(xPos: number, yPos: number) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.button = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_BUTTONS_LARGE, this.group);
        this.button.anchor.set(0.5, 1);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);

        const options = getDefaultTextFieldOptions('READY');
        options.fontSize = 60;
        options.font = Assets.FONT_BUTTONS;
        options.parent = this.group;
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = this.button.x
        this.textField.y = (this.button.y - (this.button.height / 2)) - 10;
        this.textField.anchor.set(0.5, 0.5);

        this.group.add(this.textField);
    }

    private onInputOut(button) {
        button.scale.set(1, 1);
        this.textField.scale.set(1, 1);
    }

    private onInputOver(button) {
        button.scale.set(1, 1.05);
        this.textField.scale.set(1, 1.05);
    }

    private onInputDown(e) {
        this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        this.inputDown();
    }

    destroy() {
        this.button.destroy(true);
    }

    setEnabled(isEnabled: boolean) {
        this.button.inputEnabled = isEnabled;
        if (!isEnabled) {
            this.button.alpha = 0.5;
            this.textField.alpha = 0.5;
        } else {
            this.button.alpha = 1;
            this.textField.alpha = 1;
        }
    }
}