/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import Assets from "../../Assets";

export default class LargeReadOnlyButton {
    private game: Game;
    private button: Sprite;
    private inputDown: Function;
    private textField: BitmapText;
    private group: Group;

    constructor(game: Game) {
        this.game = game;
    }

    create(xPos: number, yPos: number) {

        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;

        this.button = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_BUTTONS_RESULTS, this.group);
        this.button.anchor.set(0.5, 1);
        this.button.inputEnabled = false;
    }

    destroy() {
        this.button.destroy(true);
    }
}