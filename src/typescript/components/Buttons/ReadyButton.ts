/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../Assets";

export default class ReadyButton {
    private game: Game;
    private button: Sprite;
    private inputDown: Function;
    private playerNumber: number;

    constructor(game: Game, playerNumber: number = 1, inputDown?) {
        this.game = game;
        this.playerNumber = playerNumber;
        this.inputDown = inputDown;
    }

    create(xPos: number = 0, yPos: number = 0, inputDown?: Function) {
        this.button = this.game.add.sprite(xPos, yPos, Assets.LOBBY_ASSETS, Assets.GRAPHIC_LOBBY_READY_BUTTON.replace(/\{id\}/, this.playerNumber.toString()));
        this.button.anchor.set(0.5, 0.5);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
    }

    addOnPress(inputDown) {
        this.inputDown = inputDown;
    }

    private onInputOut(e) {
        e.scale.set(1, 1);
    }

    private onInputOver(e) {
        e.scale.set(1.05, 1.05);
    }

    private onInputDown(e) {
        const button = e;
        this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        if (this.inputDown) {
            this.inputDown();
        }
    }

    movePositionByPlayerNumber(playerNumber: number) {
        switch (playerNumber) {
            case 1:
                this.button.x = 570.5;
                break;
            case 2:
                this.button.x = 830.5;
                break;
            case 3:
                this.button.x = 1090.5;
                break;
            case 4:
                this.button.x = 1350.5;
                break;
        }

        this.button.y = 410;
    }

    destroy() {
        this.button.destroy(true);
    }

    setEnabled(isEnabled: boolean) {
        this.button.inputEnabled = isEnabled;
    }

    setDisabled() {
        this.button.alpha = 0.2;
    }

    setPressedState() {
        this.button.frameName = Assets.GRAPHIC_LOBBY_READY_BUTTON_ACTIVE.replace(/\{id\}/, this.playerNumber.toString());
    }

    setInactive() {
        this.button.frameName = Assets.GRAPHIC_LOBBY_READY_BUTTON.replace(/\{id\}/, this.playerNumber.toString());
    }
}