/**
 * Created by shaun on 02/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../Assets";

export default class ExitButton {
    private game: Game;
    private button: Sprite;
    private inputDown: Function;

    constructor(game: Game, inputDown) {
        this.game = game;
        this.inputDown = inputDown;
    }

    create(xPos: number, yPos: number) {
        this.button = this.game.add.sprite(xPos, yPos, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_BUTTONS_SMALL_EXIT);
        this.button.anchor.set(0.5, 0.5);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
    }

    private onInputOut(e) {
        e.scale.set(1, 1);
    }

    private onInputOver(e) {
        e.scale.set(1.05, 1.05);
    }

    private onInputDown(e) {
        const button = e;
        this.game.add.sound(Assets.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        this.inputDown();
    }

    destroy() {
        this.button.destroy(true);
    }
}