import Model from "./Model";
import View from "./View";
import Game = Phaser.Game;
/**
 * Created by shaun on 31/12/2016.
 */
export default class Hints {
    private model: Model;
    private view: View;

    constructor(game: Game, options, timeToWait: number) {
        this.model = new Model(options);
        this.view = new View(game, this.model);
        this.view.create(timeToWait);
    }

    destroy() {
        this.model = null;
        this.view.destroy();
    }
}