import Game = Phaser.Game;
import Group = Phaser.Group;
import textFieldFactory from '../../helpers/textfield-factory';
import {getDefaultTextFieldOptions} from '../../helpers/textfield-factory';
import Model from "./Model";

export default class View {
    private game: Phaser.Game;
    private model: Model;
    private hintTextField: Phaser.BitmapText;
    private group: Phaser.Group;
    private loopTimer: Phaser.TimerEvent;

    constructor(game: Game, model: Model) {
        this.game = game;
        this.model = model;
    }

    create(timeToWait:number) {
        this.group = new Group(this.game);
        const options = this.getTextFieldOptions();
        options.parent = this.group;
        this.hintTextField = textFieldFactory(this.game, options);
        this.group.add(this.hintTextField);

        this.loopTimer = this.game.time.events.loop(timeToWait, this.setRandomHint, this);
    }

    private getTextFieldOptions() {
        let options = getDefaultTextFieldOptions(this.model.getRandomHint());
        options.xPos = this.model.getX();
        options.yPos = this.model.getY();
        options.font = this.model.getFont();
        options.fontSize = this.model.getFontSize();
        options.align = this.model.getAlign();
        options.anchorX = this.model.getAnchorX();
        options.anchorY = this.model.getAnchorY();
        return options;
    }

    /**
     * @loopEvent Called every {timeToWait} seconds
     */
    setRandomHint() {
        const nextHint = this.model.getRandomHint();
        if (nextHint === this.hintTextField.text) {
            this.game.time.events.remove(this.loopTimer);
        }
        this.hintTextField.text = this.model.getRandomHint();
    }

    destroy() {
        this.hintTextField.destroy(true);
        this.game.time.events.remove(this.loopTimer);
        this.group = null;
    }
}