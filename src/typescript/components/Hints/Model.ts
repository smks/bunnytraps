import ArrayUtils = Phaser.ArrayUtils;

export default class Model {
    private x: number;
    private y: number;
    private fontSize: number;
    private font: string;
    private align: string;
    private anchorX: number;
    private anchorY: number;
    private hints: (string)[];
    private currentHint: string;

    constructor(options: {
        xPos: number,
        yPos: number,
        fontSize: number,
        font: string,
        align: string,
        anchorX: number,
        anchorY: number
    }) {
        this.x = options.xPos;
        this.y = options.yPos;
        this.font = options.font;
        this.fontSize = options.fontSize;
        this.align = options.align;
        this.anchorX = options.anchorX;
        this.anchorY = options.anchorY;
        this.hints = this.createHints();
        this.currentHint = '';
    }

    private createHints(): (string)[] {
        return [
            'This is hint 1',
            'This is hint 2',
            'This is hint 3',
            'This is hint 4',
            'This is hint 5',
            'This is hint 6',
            'This is hint 7',
            'This is hint 8',
            'This is hint 9',
            'This is hint 10',
            'This is hint 11',
            'This is hint 12',
            'This is hint 13',
            'This is hint 14',
            'This is hint 15',
            'This is hint 16',
            'This is hint 17',
            'This is hint 18',
            'This is hint 19',
            'This is hint 20'
        ];
    }

    getRandomHint() {
        if (this.hints.length == 0) {
            return this.currentHint;
        }
        const hint:string = ArrayUtils.getRandomItem(this.hints, 0, this.hints.length);
        this.hints.splice(this.hints.indexOf(hint), 1);
        return this.currentHint = hint;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    getFont() {
        return this.font;
    }

    getFontSize():number {
        return this.fontSize;
    }

    getAlign() {
        return this.align;
    }

    getAnchorX() {
        return this.anchorX;
    }

    getAnchorY() {
        return this.anchorY;
    }

}