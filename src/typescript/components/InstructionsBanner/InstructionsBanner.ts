/**
 * Created by shaun on 01/01/2017.
 */
import Game = Phaser.Game;
import Sprite = Phaser.Sprite;
import Assets from "../../Assets";
import Group = Phaser.Group;
import BitmapText = Phaser.BitmapText;
import textFieldFactory from './../../helpers/textfield-factory';
import {getDefaultTextFieldOptions} from './../../helpers/textfield-factory';

export default class InstructionsBanner {

    private SPEED: number = 400;

    private game: Game;
    private xPos: number;
    private yPos: number;
    private group: Group;
    private sprite: Sprite;
    private textField: BitmapText;
    private messages: Array<string> = new Array();

    constructor(game: Game, xPos: number, yPos: number) {
        this.game = game;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    create(instructionMessage: string) {
        this.group = this.game.add.group();
        this.group.x = this.xPos;
        this.group.y = this.yPos;

        this.sprite = this.game.add.sprite(0, 0, Assets.GAME_ASSETS, Assets.GRAPHIC_GAME_INSTRUCTIONS_BANNER);
        this.sprite.alpha = 0;

        this.group.add(this.sprite);

        const options = getDefaultTextFieldOptions(instructionMessage);
        options.fontSize = 40;
        options.parent = this.group;
        this.textField = textFieldFactory(this.game, options);
        this.textField.x = this.sprite.x + this.sprite.width / 2;
        this.textField.y = (this.sprite.y + this.sprite.height / 2) - 12;
        this.textField.anchor.set(0.5, 0.5);

        this.group.add(this.textField);
    }

    changeMessage(newInstructionsMessage: string) {
        const changeTween = this.game.add.tween(this.textField.scale)
            .to({y: 0}, this.SPEED, Phaser.Easing.Circular.InOut, false);

        changeTween.onComplete.add(() => {
            this.textField.text = newInstructionsMessage;
            this.game.add.tween(this.textField.scale)
                .to({y: 1}, this.SPEED, Phaser.Easing.Circular.InOut, true);
        })

        changeTween.start();
    }

    destroy() {
        this.group.destroy(true);
        this.sprite.destroy(true);
        this.textField.destroy(true);
    }

    addMessages(messages: Array<string>) {
        this.messages = messages;
    }

    // testing only
    changeRandomMessage() {
        const randomMessage = Phaser.ArrayUtils.getRandomItem(this.messages);
        this.changeMessage(randomMessage);
    }
}