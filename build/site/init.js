"use strict";
var Game_1 = require("../Game");
var initGame = function () {
    document.addEventListener('DOMContentLoaded', function () {
        var clientName = '__bunnyTrapsClientName';
        var clientPin = '__bunnyTrapsClientPin';
        if (localStorage.getItem(clientName)) {
            startGame();
        }
        else {
            document.getElementById('submit-button').addEventListener('click', function (e) {
                setPlayerName();
            });
        }
        function startGame() {
            document.getElementById('name-container')
                .remove();
            document.getElementById('logo-container')
                .remove();
            document.getElementById('container').style.backgroundImage = 'none';
            window['bunnyTrap'] = new Game_1.default();
        }
        function setPlayerName() {
            var username = document.getElementById('username').value;
            var pin = document.getElementById('pin').value;
            window.localStorage.setItem(clientName, username);
            window.localStorage.setItem(clientPin, pin);
            startGame();
        }
        var stateSelect = document.getElementById('state');
        stateSelect.addEventListener('change', function (e) {
            if (window['bunnyTrap']) {
                window['showPreloader'] = true;
                var sel = e.currentTarget;
                var stateString = sel.options[sel.selectedIndex].value;
                if (stateString != '') {
                    window['bunnyTrap'].game.state.start(stateString, true, false);
                }
            }
        });
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = initGame;
