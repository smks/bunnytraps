"use strict";
var Bunny_1 = require("../components/Bunny/Sprite/Bunny");
var Assets_1 = require("../Assets");
var BunnyPlacer = (function () {
    function BunnyPlacer(game, pressedTile, bunnyId, bunnyPlacementComplete) {
        this.bunnies = [];
        this.placements = [];
        this.currentIndex = 0;
        this.game = game;
        this.pressedTile = pressedTile;
        this.bunnyId = bunnyId;
        this.bunnyPlacementComplete = bunnyPlacementComplete;
    }
    BunnyPlacer.prototype.listen = function () {
        this.pressedTile.add(this.addBunny, this);
    };
    BunnyPlacer.prototype.hasPlantedAll = function () {
        return this.placements.length == 5;
    };
    BunnyPlacer.prototype.getPlantedCoordinates = function () {
        return this.placements;
    };
    BunnyPlacer.prototype.addBunny = function (options) {
        var columnIndex = options.columnIndex, rowIndex = options.rowIndex, tileXPos = options.tileXPos, tileYPos = options.tileYPos;
        if (this.hasAlreadyPlacedHere(columnIndex, rowIndex)) {
            console.log('Placed here already!');
            return;
        }
        if (this.bunnies.length > 4) {
            this.moveExisting(tileXPos, tileYPos, columnIndex, rowIndex);
            return;
        }
        this.createNewBunny(tileXPos, tileYPos, columnIndex, rowIndex);
        if (this.bunnies.length > 4 && this.bunnyPlacementComplete) {
            this.bunnyPlacementComplete.dispatch(this.placements);
        }
    };
    BunnyPlacer.prototype.createNewBunny = function (tileXPos, tileYPos, columnIndex, rowIndex) {
        var bunny = new Bunny_1.default(this.game, this.bunnyId);
        bunny.create(tileXPos, (tileYPos - 10), 0.2);
        bunny.dropDown(function () {
            bunny.standAnimation(true);
        });
        this.bunnies.push(bunny);
        this.placements.push({ columnIndex: columnIndex, rowIndex: rowIndex });
    };
    BunnyPlacer.prototype.moveExisting = function (tileXPos, tileYPos, columnIndex, rowIndex) {
        var bunny = this.bunnies[this.currentIndex];
        var bunnySprite = bunny.getSprite();
        bunnySprite.x = tileXPos;
        bunnySprite.y = (tileYPos - 10);
        this.game.world.bringToTop(bunnySprite);
        bunny.dropDown(function () {
            bunny.standAnimation();
        });
        this.placements[this.currentIndex].columnIndex = columnIndex;
        this.placements[this.currentIndex].rowIndex = rowIndex;
        this.currentIndex++;
        if (this.currentIndex > 4) {
            this.currentIndex = 0;
        }
    };
    BunnyPlacer.prototype.ignore = function () {
        this.pressedTile.remove(this.addBunny, this);
    };
    BunnyPlacer.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    BunnyPlacer.prototype.hasAlreadyPlacedHere = function (columnIndex, rowIndex) {
        for (var i = 0; i < this.placements.length; i++) {
            var placement = this.placements[i];
            var placementColumn = placement.columnIndex;
            var placementRow = placement.rowIndex;
            if (columnIndex === placementColumn && placementRow === rowIndex) {
                return true;
            }
        }
        return false;
    };
    BunnyPlacer.prototype.shootBunnyAway = function (onHit, onComplete) {
        if (this.bunnies.length > 0) {
            var bunny = this.bunnies.pop();
            bunny.shootToScreen(onHit, onComplete);
        }
    };
    BunnyPlacer.prototype.survived = function () {
        if (this.bunnies.length > 0) {
            var bunny_1 = this.bunnies.pop();
            this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_CROWD_CHEERING, 1, false)
                .play();
            bunny_1.jumpFade(function () {
                bunny_1.destroy();
            });
        }
    };
    BunnyPlacer.prototype.clear = function () {
        this.bunnies.map(function (bunny) {
            bunny.jumpFade(function () {
                bunny.destroy();
            });
        });
    };
    return BunnyPlacer;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BunnyPlacer;
