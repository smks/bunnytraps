"use strict";
var Assets_1 = require("../Assets");
var TrapPlacer = (function () {
    function TrapPlacer(game, pressedTile, trapPlacementComplete) {
        this.traps = [];
        this.placements = [];
        this.currentIndex = 0;
        this.game = game;
        this.pressedTile = pressedTile;
        this.trapPlacementComplete = trapPlacementComplete;
    }
    TrapPlacer.prototype.listen = function () {
        this.pressedTile.add(this.addTrap, this);
    };
    TrapPlacer.prototype.hasPlantedAll = function () {
        return this.placements.length == 5;
    };
    TrapPlacer.prototype.getPlantedCoordinates = function () {
        return this.placements;
    };
    TrapPlacer.prototype.addTrap = function (options) {
        var columnIndex = options.columnIndex, rowIndex = options.rowIndex, tileXPos = options.tileXPos, tileYPos = options.tileYPos;
        if (this.hasAlreadyPlacedHere(columnIndex, rowIndex)) {
            console.warn('Placed here already!');
            return;
        }
        if (this.traps.length > 4) {
            this.moveExisting(tileXPos, tileYPos, columnIndex, rowIndex);
            return;
        }
        this.createNewTrap(tileXPos, tileYPos, columnIndex, rowIndex);
        if (this.traps.length > 4 && this.trapPlacementComplete) {
            this.trapPlacementComplete.dispatch(this.placements);
        }
    };
    TrapPlacer.prototype.createNewTrap = function (tileXPos, tileYPos, columnIndex, rowIndex) {
        var trap = this.game.add.sprite(tileXPos, tileYPos, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUNNIES_GRID_CROSS);
        trap.anchor.set(0.5, 0.5);
        this.animateIn(trap);
        this.traps.push(trap);
        this.placements.push({ columnIndex: columnIndex, rowIndex: rowIndex });
        this.playSound();
    };
    TrapPlacer.prototype.moveExisting = function (tileXPos, tileYPos, columnIndex, rowIndex) {
        var trap = this.traps[this.currentIndex];
        trap.x = tileXPos;
        trap.y = (tileYPos - 0);
        this.animateIn(trap);
        this.playSound();
        this.placements[this.currentIndex].columnIndex = columnIndex;
        this.placements[this.currentIndex].rowIndex = rowIndex;
        this.currentIndex++;
        if (this.currentIndex > 4) {
            this.currentIndex = 0;
        }
    };
    TrapPlacer.prototype.ignore = function () {
        this.pressedTile.remove(this.addTrap, this);
    };
    TrapPlacer.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    TrapPlacer.prototype.hasAlreadyPlacedHere = function (columnIndex, rowIndex) {
        for (var i = 0; i < this.placements.length; i++) {
            var placement = this.placements[i];
            var placementColumn = placement.columnIndex;
            var placementRow = placement.rowIndex;
            if (columnIndex === placementColumn && placementRow === rowIndex) {
                return true;
            }
        }
        return false;
    };
    TrapPlacer.prototype.animateIn = function (trap) {
        trap.scale.set(3, 3);
        this.game.add.tween(trap.scale)
            .to({ x: 1, y: 1 }, 1000, Phaser.Easing.Elastic.Out, true);
        trap.alpha = 0;
        this.game.add.tween(trap)
            .to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true);
    };
    TrapPlacer.prototype.clear = function () {
        var _this = this;
        this.traps.map(function (trap) {
            trap.scale.set(1, 1);
            var tween = _this.game.add.tween(trap.scale)
                .to({ x: 1.5, y: 1.5 }, 250, Phaser.Easing.Linear.None, false);
            tween.onComplete.add(function () {
                trap.destroy(true);
            });
            trap.alpha = 1;
            _this.game.add.tween(trap)
                .to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
            tween.start();
        });
    };
    TrapPlacer.prototype.playSound = function () {
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_SET_TRAP, 1, false)
            .play();
    };
    return TrapPlacer;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TrapPlacer;
