"use strict";
var SocketEvents = {
    USERNAME_CREATE: 'username.create',
    LOBBY_JOIN: 'lobby.join',
    ROOM_JOIN: 'room.join',
    GAME_START: 'game.start',
    PLAYER_READY: 'player.ready',
    PLACEMENT_BUNNY: 'placement.bunny',
    PLACEMENT_TRAP: 'placement.trap',
    PLACEMENT_COMPLETE: 'placement.complete',
    ROOM_FINISHED: 'room.finished',
    ROUND_COMPLETE: 'round.complete',
    GAME_COMPLETE: 'game.complete',
    DISCONNECT: 'disconnect'
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SocketEvents;
