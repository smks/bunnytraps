"use strict";
var Socket_1 = require('../socket/Socket');
var SocketEvents_1 = require("./SocketEvents");
var LobbyEvents = (function () {
    function LobbyEvents(game) {
        this.game = game;
    }
    LobbyEvents.prototype.onLobbyJoin = function (data) {
        console.log('on lobby join event called');
        console.log(data);
        var username = data.username;
        var room = data.room;
        Socket_1.default.emit(SocketEvents_1.default.ROOM_JOIN, {
            username: username,
            room: room
        });
    };
    LobbyEvents.prototype.playerReady = function (username) {
        Socket_1.default.emit(SocketEvents_1.default.PLAYER_READY, { username: username });
    };
    return LobbyEvents;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LobbyEvents;
