"use strict";
var socket = require('socket.io-client');
var SocketEvents_1 = require('./SocketEvents');
var Socket = (function () {
    function Socket() {
        this.onLobbyJoin = new Phaser.Signal();
        this.onRoomJoined = new Phaser.Signal();
        this.onGameStarted = new Phaser.Signal();
        this.onPlayerReady = new Phaser.Signal();
        this.onPlacedBunny = new Phaser.Signal();
        this.onPlacedTrap = new Phaser.Signal();
        this.onPlacementComplete = new Phaser.Signal();
        this.onRoomFinished = new Phaser.Signal();
        this.onRoundComplete = new Phaser.Signal();
        this.onGameSessionComplete = new Phaser.Signal();
        this.onDisconnect = new Phaser.Signal();
        this.onEnteredLobby = new Phaser.Signal();
        this.socket = this.initSocket();
    }
    Socket.prototype.init = function () {
        var _this = this;
        this.socket.on(SocketEvents_1.default.LOBBY_JOIN, function (data) {
            console.log('socket event player joined ' + data.username);
            _this.onLobbyJoin.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.ROOM_JOIN, function (data) {
            console.log('socket event player joined ' + data.username);
            _this.onRoomJoined.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.GAME_START, function (data) {
            console.log('game started');
            _this.onGameStarted.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.PLAYER_READY, function (data) {
            console.log('player ready');
            _this.onPlayerReady.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.PLACEMENT_BUNNY, function (data) {
            _this.onPlacedBunny.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.PLACEMENT_TRAP, function (data) {
            _this.onPlacedTrap.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.PLACEMENT_COMPLETE, function (data) {
            _this.onPlacementComplete.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.ROOM_FINISHED, function (data) {
            _this.onRoomFinished.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.ROUND_COMPLETE, function (data) {
            _this.onRoundComplete.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.GAME_COMPLETE, function (data) {
            _this.onGameSessionComplete.dispatch(data);
        });
        this.socket.on(SocketEvents_1.default.DISCONNECT, function (data) {
            _this.onDisconnect.dispatch(data);
        });
    };
    Socket.prototype.initSocket = function () {
        return socket('localhost:5000');
    };
    Socket.prototype.emit = function (event, data) {
        console.log('sending event: ' + event);
        this.socket.emit(event, data);
    };
    return Socket;
}());
;
var gameSocket = new Socket();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = gameSocket;
