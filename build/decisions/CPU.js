"use strict";
var positions_available_1 = require('../helpers/positions-available');
var CPU = (function () {
    function CPU() {
        this.availablePositions = positions_available_1.default();
        return this;
    }
    CPU.prototype.generateDecisions = function (playerId, bunnyId) {
        var decisions = {
            playerId: playerId,
            bunnyId: bunnyId,
            bunnyPlacements: [],
            trapPlacements: [],
        };
        var i = 0;
        while (i < 5) {
            decisions.bunnyPlacements.push(this.getPosition());
            decisions.trapPlacements.push(this.getPosition());
            i++;
        }
        return decisions;
    };
    CPU.prototype.getPosition = function () {
        return Phaser.ArrayUtils.removeRandomItem(this.availablePositions);
    };
    return CPU;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = CPU;
