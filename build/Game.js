"use strict";
var Game = Phaser.Game;
var Boot_1 = require("./states/Boot");
var Preload_1 = require("./states/Preload");
var Strategy_1 = require("./states/Strategy");
var ResultsDemo_1 = require("./states/ResultsDemo");
var LobbyDemo_1 = require("./states/LobbyDemo");
var init_1 = require("./site/init");
var GameSession = (function () {
    function GameSession() {
        this.game = new Game(1920, 1080, Phaser.AUTO, 'game', {
            create: this.create
        });
    }
    GameSession.prototype.create = function () {
        this.game.stage.setBackgroundColor('#cccccc');
        this.game.state.add('Boot', Boot_1.Boot, false);
        this.game.state.add('Preload', Preload_1.Preload, false);
        this.game.state.add('Lobby', LobbyDemo_1.default, false);
        this.game.state.add('Strategy', Strategy_1.Strategy, false);
        this.game.state.add('Results', ResultsDemo_1.ResultsDemo, false);
        this.game.state.start('Boot');
    };
    return GameSession;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = GameSession;
init_1.default();
