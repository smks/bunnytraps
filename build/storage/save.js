"use strict";
var Save = (function () {
    function Save() {
        this.savedDataKey = '__bunnyTraps';
        this.savedDataString = localStorage.getItem(this.savedDataKey);
        if (this.savedDataString === null) {
            this.savedDataString = this.setDefaultSave();
        }
        this.savedData = JSON.parse(this.savedDataString);
    }
    Save.prototype.setDefaultSave = function () {
        localStorage.setItem(this.savedDataKey, JSON.stringify({
            'roomGamesPlayed': 0,
            'player1': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player2': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player3': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'player4': {
                bunniesLeft: 25,
                timesWon: 0,
                decisions: {}
            },
            'currentLevel': 1,
            'currentState': 0,
            'seenTutorial': false
        }));
        return localStorage.getItem(this.savedDataKey);
    };
    Save.prototype.load = function () {
        return this.savedData;
    };
    Save.prototype.get = function (key) {
        return this.savedData[key];
    };
    Save.prototype.getChild = function (key, child) {
        return this.savedData[key][child];
    };
    Save.prototype.set = function (key, val) {
        this.savedData[key] = val;
        localStorage.setItem(this.savedDataKey, JSON.stringify(this.savedData));
    };
    Save.prototype.setChild = function (key, child, val) {
        this.savedData[key][child] = val;
        localStorage.setItem(this.savedDataKey, JSON.stringify(this.savedData));
    };
    Save.prototype.storeDecisions = function (playerId, decisions) {
        this.setChild("player" + playerId, 'decisions', decisions);
    };
    Save.prototype.getDecisions = function (playerId) {
        return this.getChild("player" + playerId, 'decisions');
    };
    Save.prototype.getBunnyCount = function (playerId) {
        return this.getChild("player" + playerId, 'bunniesLeft');
    };
    Save.prototype.reduceBunnyCount = function (playerId) {
        var bunnyCount = this.getChild("player" + playerId, 'bunniesLeft') - 1;
        if (bunnyCount < 0) {
            bunnyCount = 0;
        }
        this.setChild("player" + playerId, 'bunniesLeft', bunnyCount);
    };
    Save.prototype.roomGameFinished = function () {
        var gamesPlayed = parseInt(this.get('roomGamesPlayed'), 10);
        if (gamesPlayed > 5) {
            gamesPlayed = 5;
        }
        else {
            gamesPlayed++;
        }
        this.set('roomGamesPlayed', (gamesPlayed).toString());
    };
    Save.prototype.getRoomGamesPlayed = function () {
        return this.get('roomGamesPlayed');
    };
    Save.getUser = function () {
        return localStorage.getItem('__bunnyTrapsClientName');
    };
    Save.SEEN_TUTORIAL = 'seenTutorial';
    Save.CURRENT_LEVEL = 'currentLevel';
    Save.HIGHEST_LEVEL = 'highestLevel';
    Save.SCORE = 'score';
    Save.DECISIONS = 'decisions';
    return Save;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Save;
