"use strict";
var COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = COLORS;
