"use strict";
function positionsAvailable() {
    var COLUMNS = 6;
    var ROWS = 5;
    var positionsAvailable = [];
    for (var columnIndex = 0; columnIndex < COLUMNS; columnIndex++) {
        for (var rowIndex = 0; rowIndex < ROWS; rowIndex++) {
            positionsAvailable.push({ columnIndex: columnIndex, rowIndex: rowIndex });
        }
    }
    return positionsAvailable;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = positionsAvailable;
