'use strict';
var BunnyTurnIndicator_1 = require("../../components/Bunny/BunnyTurnIndicator");
function default_1(game, xPos, yPos, bunnyId, bunnyCount) {
    if (bunnyId === void 0) { bunnyId = 1; }
    var bunnyIndicator = new BunnyTurnIndicator_1.default(game, bunnyId);
    bunnyIndicator.create(xPos, yPos, bunnyCount);
    return bunnyIndicator;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
