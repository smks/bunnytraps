'use strict';
var LargeReadyButton_1 = require("../../components/Buttons/LargeReadyButton");
function default_1(game, isEnabled) {
    if (isEnabled === void 0) { isEnabled = false; }
    var readyButton = new LargeReadyButton_1.default(game, function () { });
    readyButton.create(355, 540.5);
    readyButton.setEnabled(isEnabled);
    return readyButton;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
