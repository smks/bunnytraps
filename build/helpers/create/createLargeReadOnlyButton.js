'use strict';
var LargeReadOnlyButton_1 = require("../../components/Buttons/LargeReadOnlyButton");
function default_1(game) {
    var readyButton = new LargeReadOnlyButton_1.default(game);
    readyButton.create(355, 540.5);
    return readyButton;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
