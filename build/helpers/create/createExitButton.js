'use strict';
var ExitButton_1 = require("../../components/Buttons/ExitButton");
function default_1(game) {
    var exitButton = new ExitButton_1.default(game, function () {
        alert('Pressed exit button');
    });
    exitButton.create(1787, 55);
    return exitButton;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
