'use strict';
var Bush_1 = require("../../components/Cover/Bush");
function default_1(game, open, onComplete) {
    if (open === void 0) { open = true; }
    var bush = new Bush_1.default(game);
    bush.create();
    if (open) {
        bush.open(function () {
            if (onComplete) {
                onComplete();
            }
        });
    }
    return bush;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
