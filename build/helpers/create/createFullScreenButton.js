'use strict';
var FullScreenButton_1 = require("../../components/Buttons/FullScreenButton");
function default_1(game) {
    var fullScreenButton = new FullScreenButton_1.default(game, function () {
        if (game.scale.isFullScreen) {
            game.scale.stopFullScreen();
        }
        else {
            game.scale.startFullScreen(true);
        }
    });
    fullScreenButton.create(1415, 55);
    return fullScreenButton;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
