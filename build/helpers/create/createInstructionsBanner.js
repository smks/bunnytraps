'use strict';
var InstructionsBanner_1 = require("../../components/InstructionsBanner/InstructionsBanner");
function default_1(game, defaultMessage) {
    if (defaultMessage === void 0) { defaultMessage = ''; }
    var instructionsBanner = new InstructionsBanner_1.default(game, 80, 40);
    instructionsBanner.create(defaultMessage);
    return instructionsBanner;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
