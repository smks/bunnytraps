'use strict';
var SoundButton_1 = require("../../components/Buttons/SoundButton");
function default_1(game) {
    var soundButton = new SoundButton_1.default(game);
    soundButton.addOnPress(function () {
        if (!game.sound.mute) {
            game.sound.mute = true;
            soundButton.setOff();
        }
        else {
            game.sound.mute = false;
            soundButton.setOn();
        }
    });
    soundButton.create(1600, 55);
    return soundButton;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
