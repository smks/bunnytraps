'use strict';
function default_1(game) {
    var gameBg = game.add.sprite(game.world.centerX, game.world.centerY, 'game-bg');
    gameBg.anchor.set(0.5, 0.5);
    return gameBg;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
