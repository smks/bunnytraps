"use strict";
function default_1(playerID) {
    var mapper = {
        1: 1,
        2: 2,
        3: 3,
        4: 4
    };
    return mapper[playerID];
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
