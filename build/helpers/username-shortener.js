"use strict";
var MAX_LENGTH = 5;
function default_1(username) {
    var newUsername;
    if (username.length > MAX_LENGTH) {
        newUsername = username.substr(0, MAX_LENGTH) + '...';
    }
    else {
        newUsername = username;
    }
    return newUsername;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
