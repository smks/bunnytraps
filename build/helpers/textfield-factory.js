"use strict";
var Assets_1 = require("../Assets");
function default_1(game, options) {
    var text = game.add.bitmapText(options.xPos, options.yPos, options.font, options.content, options.fontSize, options.parent);
    text.anchor.set(options.anchorX, options.anchorY);
    return text;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
function getDefaultTextFieldOptions(content) {
    return {
        content: content,
        fontSize: 30,
        font: Assets_1.default.FONT,
        xPos: 0,
        yPos: 0,
        align: 'center',
        anchorX: 0.5,
        anchorY: 0.5,
        parent: undefined
    };
}
exports.getDefaultTextFieldOptions = getDefaultTextFieldOptions;
