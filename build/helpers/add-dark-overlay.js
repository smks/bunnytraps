"use strict";
function default_1(game) {
    var mapDarkOverlay = game.add.graphics(0, 0);
    mapDarkOverlay.beginFill(0x111111);
    mapDarkOverlay.drawRect(0, 0, game.world.width, game.world.height);
    mapDarkOverlay.endFill();
    mapDarkOverlay.alpha = 0.65;
    return mapDarkOverlay;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
;
