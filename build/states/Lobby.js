"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require("../Assets");
var Socket_1 = require("../socket/Socket");
var SocketEvents_1 = require("../socket/SocketEvents");
var save_1 = require("../storage/save");
var COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
};
var Lobby = (function (_super) {
    __extends(Lobby, _super);
    function Lobby() {
        _super.call(this);
        this.players = [];
        this.setCountdown = this.setCountdown.bind(this);
    }
    Lobby.prototype.preload = function () {
        Socket_1.default.onLobbyJoin.add(this.onLobbyJoin, this);
        Socket_1.default.onRoomJoined.add(this.onRoomJoin, this);
        Socket_1.default.onGameStarted.add(this.onGameStart, this);
        Socket_1.default.onDisconnect.add(this.onPlayerDisconnected, this);
        this.myUsername = save_1.default.getUser();
    };
    Lobby.prototype.create = function () {
        var _this = this;
        this.bg = this.game.add.sprite(0, 0, Assets_1.default.LOBBY_BG);
        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets_1.default.LOBBY_ASSETS, Assets_1.default.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);
        this.lobbyStatusText.anchor.set(0.5, 0.5);
        var tween = this.game.add.tween(this.logo.scale)
            .to({ x: 1, y: 1 }, 1000, Phaser.Easing.Circular.InOut, false);
        tween.onComplete.add(function () {
            Socket_1.default.emit(SocketEvents_1.default.LOBBY_JOIN, {
                username: _this.myUsername
            });
            _this.lobbyStatusText.alpha = 1;
        });
        tween.start();
    };
    Lobby.prototype.onLobbyJoin = function (data) {
        console.log('on lobby join event called');
        console.log(data);
        var username = data.username;
        var room = data.room;
        Socket_1.default.emit(SocketEvents_1.default.ROOM_JOIN, {
            username: username,
            room: room
        });
    };
    Lobby.prototype.onGameStart = function () {
        this.game.state.start('Strategy', true);
    };
    Lobby.prototype.onRoomJoin = function (data) {
        var _this = this;
        console.log('on room join called');
        var allPlayersData = data.players;
        var secondsToWait = data.secondsToWait;
        allPlayersData.map(function (player) {
            var playerNumber = player.playerNumber;
            var username = player.username;
            for (var i = 0; i < _this.players.length; i++) {
                var clientPlayer = _this.players[i];
                console.log('Checking player: ' + clientPlayer.username);
                if (clientPlayer.username == username) {
                    console.log('player exists already, no need to add to lobby');
                    return;
                }
            }
            var isPlayerMe = player === _this.myUsername;
            var newPlayer = {
                id: playerNumber,
                username: player.username,
                playerHead: null
            };
            _this.players.push(newPlayer);
            _this.lobbyStatusText.text = _this.setLobbyStatus(_this.players.length);
        });
        if (secondsToWait && !this.isAboutToPlayGame) {
            this.isAboutToPlayGame = true;
            this.secondsToWait = secondsToWait;
            this.countDownHandle = this.game.time.create(false);
            this.countDownHandle.loop(Phaser.Timer.SECOND, this.setCountdown, this);
        }
    };
    Lobby.prototype.setCountdown = function () {
        if (this.secondsToWait > 1) {
            this.secondsToWait--;
            this.lobbyStatusText.text = "Playing in " + this.secondsToWait + " seconds";
        }
        else {
            this.lobbyStatusText.destroy(true);
            this.countDownHandle.destroy();
        }
    };
    Lobby.prototype.onPlayerDisconnected = function (data) {
        var username = data.username;
        console.log(username + ' just left');
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player.username == username) {
                player.playerHead.destroy(true);
                this.players.splice(i, 1);
                this.lobbyStatusText.text = this.setLobbyStatus(this.players.length);
                break;
            }
        }
    };
    Lobby.prototype.setLobbyStatus = function (totalPlayers) {
        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }
        return 'WAITING FOR {n} PLAYERS'.replace('{n}', (4 - this.players.length).toString());
    };
    Lobby.prototype.showPlayButton = function () {
        var _this = this;
        var tween = this.game.add.tween(this.playButton)
            .to({ alpha: 1 }, 1000, "Linear", false);
        tween.onComplete.add(function () {
            _this.playButton.events.onInputDown.add(function () {
                _this.game.state.start('Strategy', true);
                _this.fadeOut();
            });
        });
        tween.start();
    };
    Lobby.prototype.fadeOut = function () {
    };
    Lobby.prototype.shutdown = function () {
        this.bg = null;
        Socket_1.default.onLobbyJoin.remove(this.onLobbyJoin, this);
        Socket_1.default.onRoomJoined.remove(this.onRoomJoin, this);
        Socket_1.default.onGameStarted.remove(this.onGameStart, this);
        Socket_1.default.onDisconnect.remove(this.onPlayerDisconnected, this);
        if (this.lobbyStatusText) {
            this.lobbyStatusText.destroy(true);
        }
        if (this.countDownHandle) {
            this.countDownHandle.destroy();
        }
    };
    return Lobby;
}(Phaser.State));
exports.Lobby = Lobby;
