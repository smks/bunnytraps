"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require("../Assets");
var ReadyButton_1 = require("../components/Buttons/ReadyButton");
var LobbyStatusBar_1 = require("../components/Lobby/LobbyStatusBar");
var LobbyPlayerBar_1 = require("../components/Lobby/LobbyPlayerBar");
var Bunny_1 = require("../components/Bunny/Sprite/Bunny");
var FullScreenButton_1 = require("../components/Buttons/FullScreenButton");
var ExitButton_1 = require("../components/Buttons/ExitButton");
var Bush_1 = require("../components/Cover/Bush");
var createSoundButton_1 = require('../helpers/create/createSoundButton');
var COLORS = {
    PINK: 'pink',
    BLUE: 'blue',
    GREEN: 'green',
    ORANGE: 'orange'
};
var LobbyNew = (function (_super) {
    __extends(LobbyNew, _super);
    function LobbyNew() {
        _super.call(this);
    }
    LobbyNew.prototype.preload = function () {
    };
    LobbyNew.prototype.create = function () {
        var _this = this;
        this.lobbyMusic = this.game.add.audio(Assets_1.default.AUDIO_KEY_AUDIO_MAIN_THEME_01, 1, true);
        this.lobbyMusic.play();
        this.addBackground();
        this.addLogo();
        this.addReadyButtons();
        this.addLobbyStatusBar();
        this.addLobbyPlayerBar();
        this.addBunnies();
        this.addCustomButtons();
        this.bush = new Bush_1.default(this.game);
        this.bush.create(0, 0);
        this.bush.open(function () {
            _this.tweenInLogo();
        });
    };
    LobbyNew.prototype.tweenInLogo = function () {
        var tween = this.game.add.tween(this.logo.scale)
            .to({ x: 1, y: 1 }, 1000, Phaser.Easing.Circular.InOut, false);
        tween.onComplete.add(function () {
        });
        tween.start();
    };
    LobbyNew.prototype.addBackground = function () {
        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'lobby-bg');
        this.bg.anchor.set(0.5, 0.5);
    };
    LobbyNew.prototype.addLogo = function () {
        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets_1.default.LOBBY_ASSETS, Assets_1.default.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);
    };
    LobbyNew.prototype.addReadyButtons = function () {
        var _this = this;
        this.readyButtonOne = new ReadyButton_1.default(this.game, 1, function () {
            _this.readyButtonOne.setPressedState();
            _this.readyButtonOne.setEnabled(false);
            _this.onGameStart();
        });
        this.readyButtonOne.create(570.5, 410);
        this.readyButtonTwo = new ReadyButton_1.default(this.game, 2);
        this.readyButtonTwo.create(830.5, 410);
        this.readyButtonTwo.setEnabled(false);
        this.readyButtonTwo.setDisabled();
        this.readyButtonThree = new ReadyButton_1.default(this.game, 3);
        this.readyButtonThree.create(1090.5, 410);
        this.readyButtonThree.setDisabled();
        this.readyButtonFour = new ReadyButton_1.default(this.game, 4);
        this.readyButtonFour.create(1350.5, 410);
        this.readyButtonFour.setEnabled(false);
        this.readyButtonFour.setDisabled();
    };
    LobbyNew.prototype.onGameStart = function () {
        var _this = this;
        this.lobbyMusic.fadeOut(500);
        this.game.world.bringToTop(this.bush.getSprite());
        this.bush.close(function () {
            _this.game.state.start('Strategy', true);
        });
    };
    LobbyNew.prototype.addBunnies = function () {
        this.bunnyOne = new Bunny_1.default(this.game, 1);
        this.bunnyOne.create(580, 700);
        this.bunnyOne.standAnimation();
        this.bunnyTwo = new Bunny_1.default(this.game, 2);
        this.bunnyTwo.create(840, 700);
        this.bunnyTwo.standAnimation();
        this.bunnyThree = new Bunny_1.default(this.game, 3);
        this.bunnyThree.create(1100, 700);
        this.bunnyThree.standAnimation();
        this.bunnyFour = new Bunny_1.default(this.game, 4);
        this.bunnyFour.create(1360, 700);
        this.bunnyFour.standAnimation();
    };
    LobbyNew.prototype.addLobbyStatusBar = function () {
        this.lobbyStatusBar = new LobbyStatusBar_1.default(this.game);
        this.lobbyStatusBar.create(this.game.world.centerX, 950);
        this.lobbyStatusBar.setText('Waiting for 3 players');
    };
    LobbyNew.prototype.addLobbyPlayerBar = function () {
        this.playerBarOne = new LobbyPlayerBar_1.default(this.game, 'aaaaaaaaaaaaaaaaa');
        this.playerBarOne.create(570.5, 850);
        this.playerBarTwo = new LobbyPlayerBar_1.default(this.game, 'bbbbbbbbbbbbbbbbb');
        this.playerBarTwo.create(830.5, 850);
        this.playerBarThree = new LobbyPlayerBar_1.default(this.game, 'ccccccccccccccccc');
        this.playerBarThree.create(1090.5, 850);
        this.playerBarFour = new LobbyPlayerBar_1.default(this.game, 'ddddddddddddddddd');
        this.playerBarFour.create(1350.5, 850);
    };
    LobbyNew.prototype.setLobbyStatus = function (totalPlayers) {
        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }
    };
    LobbyNew.prototype.addPlayer = function (username, playerNumber) {
    };
    LobbyNew.prototype.fadeOut = function () {
    };
    LobbyNew.prototype.shutdown = function () {
    };
    LobbyNew.prototype.addCustomButtons = function () {
        var _this = this;
        this.fullScreenButton = new FullScreenButton_1.default(this.game, function () {
            if (_this.game.scale.isFullScreen) {
                _this.game.scale.stopFullScreen();
            }
            else {
                _this.game.scale.startFullScreen(true);
            }
        });
        this.fullScreenButton.create(1415, 55);
        this.soundButton = createSoundButton_1.default(this.game);
        this.soundButton.create(1600, 55);
        this.exitButton = new ExitButton_1.default(this.game, function () {
            alert('Pressed exit button');
        });
        this.exitButton.create(1787, 55);
    };
    return LobbyNew;
}(Phaser.State));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LobbyNew;
