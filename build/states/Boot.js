"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require("../Assets");
var Boot = (function (_super) {
    __extends(Boot, _super);
    function Boot() {
        _super.apply(this, arguments);
    }
    Boot.prototype.preload = function () {
        this.game.load.image('loading-bg', Assets_1.default.LOADING_BG);
        this.game.load.bitmapFont(Assets_1.default.FONT, '/assets/fonts/FreckleFace.png', '/assets/fonts/FreckleFace.fnt');
    };
    Boot.prototype.create = function () {
        this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = true;
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.windowConstraints.bottom = "visual";
        this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        if (this.game.device.desktop) {
        }
        else {
        }
        this.game.state.start('Preload', true, false);
    };
    return Boot;
}(Phaser.State));
exports.Boot = Boot;
