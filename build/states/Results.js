"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Platforms_1 = require("../components/Platforms");
var save_1 = require("../storage/save");
var Assets_1 = require("../Assets");
var Results = (function (_super) {
    __extends(Results, _super);
    function Results() {
        _super.apply(this, arguments);
        this.stepMade = new Phaser.Signal();
        this.playersTurn = 1;
        this.playerOneDecisions = {};
        this.playerTwoDecisions = {};
    }
    Results.prototype.create = function () {
        var _this = this;
        this.ambience = this.game.add.audio(Assets_1.default.AUDIO_KEY_AUDIO_AMBIENCE_LOOP, 2, true);
        this.ambience.play();
        this.bg = this.game.add.sprite(0, 0, 'sprites', 'play-background.jpg');
        this.save = new save_1.default();
        this.playerOneDecisions = this.save.getChild('player1', 'decisions');
        this.playerTwoDecisions = this.save.getChild('player2', 'decisions');
        this.platforms = new Platforms_1.default(this.game, this.save);
        this.platforms.x = 548;
        this.platforms.initPlay();
        this.stepMade.add(function () {
            if (_this.playersTurn == 1) {
                _this.playersTurn = 2;
            }
            else {
                _this.playersTurn = 1;
            }
            _this.platforms.showNextResult(_this.playersTurn);
        });
        this.instructionText = this.game.add.text(275, 177, "LET'S BEGIN", {
            font: "50px Bombardier",
            fill: "#ffffff",
            align: "center"
        });
        this.instructionText.anchor.set(0.5, 0.5);
        this.instructionText.border = true;
        this.playerOneText = this.game.add.text(104, 257, "dan_88", {
            font: "40px Bombardier",
            fill: "#fff",
            align: "center"
        });
        this.playerTwoText = this.game.add.text(324, 257, "smks", {
            font: "40px Bombardier",
            fill: "#fff",
            align: "center"
        });
        this.actionsText = this.game.add.text(70, 518, "09:43:59 - dan_88 is ready\n09:43:59 - Enemy just placed a trap\n09:43:59 - Enemy just placed a bunny\n09:43:59 - You just placed a bunny\n09:43:59 - Enemy just placed a bunny\n09:43:59 - You just placed a bunny\n09:43:59 - Enemy just placed a bunny\n09:43:59 - You just placed a bunny\n09:43:59 - Enemy just placed a bunny\n09:43:59 - You just placed a bunny\n09:43:59 - Enemy just placed a bunny".trim(), {
            font: "24px Bombardier",
            fill: "#fff",
            align: "left"
        });
        this.actionsText.lineHeight = -20;
    };
    Results.prototype.destroy = function () {
    };
    return Results;
}(Phaser.State));
exports.Results = Results;
