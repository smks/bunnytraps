"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require("../Assets");
var ReadyButton_1 = require("../components/Buttons/ReadyButton");
var LobbyStatusBar_1 = require("../components/Lobby/LobbyStatusBar");
var LobbyPlayerBar_1 = require("../components/Lobby/LobbyPlayerBar");
var Bunny_1 = require("../components/Bunny/Sprite/Bunny");
var FullScreenButton_1 = require("../components/Buttons/FullScreenButton");
var ExitButton_1 = require("../components/Buttons/ExitButton");
var Bush_1 = require("../components/Cover/Bush");
var save_1 = require("../storage/save");
var Socket_1 = require("../socket/Socket");
var LobbyEvents_1 = require("../socket/LobbyEvents");
var State = Phaser.State;
var SocketEvents_1 = require("../socket/SocketEvents");
var createSoundButton_1 = require('../helpers/create/createSoundButton');
var LobbyNew = (function (_super) {
    __extends(LobbyNew, _super);
    function LobbyNew() {
        _super.call(this);
        this.isAboutToPlayGame = false;
        this.readyButtons = new Array();
    }
    LobbyNew.prototype.preload = function () {
        this.events = new LobbyEvents_1.default(this.game);
        this.players = new Array();
        Socket_1.default.onLobbyJoin.add(this.events.onLobbyJoin, this);
        Socket_1.default.onRoomJoined.add(this.onRoomJoin, this);
        Socket_1.default.onPlayerReady.add(this.onPlayerReady, this);
        Socket_1.default.onGameStarted.add(this.onGameStart, this);
        Socket_1.default.onDisconnect.add(this.onPlayerDisconnected, this);
        this.myUsername = save_1.default.getUser();
    };
    LobbyNew.prototype.onPlayerReady = function (data) {
        var usernameReady = data.player;
        var userId;
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player.username == usernameReady) {
                userId = player.id;
            }
        }
        var readyButton = this.readyButtons[userId - 1];
        if (readyButton) {
            readyButton.setPressedState();
        }
    };
    LobbyNew.prototype.onPlayerDisconnected = function (data) {
        var username = data.username;
        console.log(username + ' just left');
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (player.username == username) {
                player.bunnySprite.destroy();
                this.players.splice(i, 1);
                this.lobbyStatusBar.setText(this.setLobbyStatus(this.players.length));
                break;
            }
        }
    };
    LobbyNew.prototype.onRoomJoin = function (data) {
        var _this = this;
        console.log('on room join called');
        var allPlayersData = data.players;
        var secondsToWait = data.secondsToWait;
        allPlayersData.map(function (player) {
            var playerNumber = player.playerNumber;
            var bunnyId = player.bunnyId;
            var username = player.username;
            for (var i = 0; i < _this.players.length; i++) {
                var clientPlayer = _this.players[i];
                console.log('Checking player: ' + clientPlayer.username);
                if (clientPlayer.username == username) {
                    console.log('player exists already, no need to add to lobby');
                    return;
                }
            }
            var isPlayerMe = player === _this.myUsername;
            var newPlayer = {
                id: playerNumber,
                username: player.username,
                bunnySprite: _this.addBunny(playerNumber)
            };
            _this.addLobbyPlayerBar(playerNumber, player.username);
            _this.addReadyButton(playerNumber, player.username);
            _this.players.push(newPlayer);
            if (!_this.isAboutToPlayGame) {
                _this.lobbyStatusBar.setText(_this.setLobbyStatus(_this.players.length));
            }
        });
        if (secondsToWait && !this.isAboutToPlayGame) {
            this.isAboutToPlayGame = true;
            this.secondsToWait = secondsToWait;
            this.countDownHandle = this.game.time.create(false);
            this.countDownHandle.loop(Phaser.Timer.SECOND, this.setCountdown, this);
            this.countDownHandle.start();
        }
    };
    LobbyNew.prototype.setCountdown = function () {
        console.log('set countdown called');
        if (this.secondsToWait > 1) {
            this.secondsToWait--;
            this.lobbyStatusBar.setText("Playing in " + this.secondsToWait + " seconds");
        }
        else {
            this.lobbyStatusBar.destroy();
            this.countDownHandle.destroy();
        }
    };
    LobbyNew.prototype.create = function () {
        var _this = this;
        this.lobbyMusic = this.game.add.audio('main-music', 1, true);
        this.lobbyMusic.play();
        this.addBackground();
        this.addLogo();
        this.addLobbyStatusBar();
        this.addCustomButtons();
        this.bush = new Bush_1.default(this.game);
        this.bush.create(0, 0);
        this.bush.open(function () {
            _this.tweenInLogo();
        });
    };
    LobbyNew.prototype.tweenInLogo = function () {
        var _this = this;
        var tween = this.game.add.tween(this.logo.scale)
            .to({ x: 1, y: 1 }, 1000, Phaser.Easing.Circular.InOut, false);
        tween.onComplete.add(function () {
            Socket_1.default.emit(SocketEvents_1.default.LOBBY_JOIN, {
                username: _this.myUsername
            });
        });
        tween.start();
    };
    LobbyNew.prototype.addBackground = function () {
        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'lobby-bg');
        this.bg.anchor.set(0.5, 0.5);
    };
    LobbyNew.prototype.addLogo = function () {
        this.logo = this.game.add.sprite(this.game.world.centerX, 190, Assets_1.default.LOBBY_ASSETS, Assets_1.default.GRAPHIC_LOBBY_LOGO);
        this.logo.anchor.set(0.5, 0.5);
        this.logo.scale.set(0, 0);
    };
    LobbyNew.prototype.addReadyButton = function (playerNumber, username) {
        var isMe = username === this.myUsername;
        var readyButton = new ReadyButton_1.default(this.game, playerNumber);
        readyButton.create();
        if (isMe) {
            readyButton.addOnPress(function () {
                readyButton.setPressedState();
                readyButton.setEnabled(false);
            });
        }
        else {
            readyButton.setEnabled(false);
            readyButton.setDisabled();
        }
        readyButton.movePositionByPlayerNumber(playerNumber);
        this.readyButtons[playerNumber] = readyButton;
    };
    LobbyNew.prototype.addLobbyStatusBar = function () {
        this.lobbyStatusBar = new LobbyStatusBar_1.default(this.game);
        this.lobbyStatusBar.create(this.game.world.centerX, 950);
        this.lobbyStatusBar.setText(this.setLobbyStatus());
    };
    LobbyNew.prototype.addLobbyPlayerBar = function (playerNumber, username) {
        switch (playerNumber) {
            case 1:
                this.playerBar1 = new LobbyPlayerBar_1.default(this.game, username);
                this.playerBar1.create(570.5, 850);
                break;
            case 2:
                this.playerBar2 = new LobbyPlayerBar_1.default(this.game, username);
                this.playerBar2.create(830.5, 850);
                break;
            case 3:
                this.playerBar3 = new LobbyPlayerBar_1.default(this.game, username);
                this.playerBar3.create(1090.5, 850);
                break;
            case 4:
                this.playerBar4 = new LobbyPlayerBar_1.default(this.game, username);
                this.playerBar4.create(1350.5, 850);
                break;
        }
    };
    LobbyNew.prototype.setLobbyStatus = function (totalPlayers) {
        if (totalPlayers === void 0) { totalPlayers = 1; }
        if (totalPlayers > 3) {
            return 'PLAYERS READY';
        }
        return 'WAITING FOR {n} PLAYERS'.replace('{n}', (4 - this.players.length).toString());
    };
    LobbyNew.prototype.addBunny = function (bunnyId) {
        var _this = this;
        var animate = function (bunny) {
            bunny.standAnimation();
        };
        switch (bunnyId) {
            case 1:
                this.bunnyOne = new Bunny_1.default(this.game, bunnyId);
                this.bunnyOne.create(580, 700);
                this.bunnyOne.dropDown(function () {
                    animate(_this.bunnyOne);
                }, 200);
                return this.bunnyOne;
            case 2:
                this.bunnyTwo = new Bunny_1.default(this.game, bunnyId);
                this.bunnyTwo.create(840, 700);
                this.bunnyTwo.dropDown(function () {
                    animate(_this.bunnyTwo);
                }, 200);
                return this.bunnyTwo;
            case 3:
                this.bunnyThree = new Bunny_1.default(this.game, bunnyId);
                this.bunnyThree.create(1100, 700);
                this.bunnyThree.dropDown(function () {
                    animate(_this.bunnyThree);
                }, 200);
                return this.bunnyThree;
            case 4:
                this.bunnyFour = new Bunny_1.default(this.game, bunnyId);
                this.bunnyFour.create(1360, 700);
                this.bunnyFour.dropDown(function () {
                    animate(_this.bunnyFour);
                }, 200);
                return this.bunnyFour;
        }
    };
    LobbyNew.prototype.fadeOut = function () {
    };
    LobbyNew.prototype.addCustomButtons = function () {
        var _this = this;
        this.fullScreenButton = new FullScreenButton_1.default(this.game, function () {
            if (_this.game.scale.isFullScreen) {
                _this.game.scale.stopFullScreen();
            }
            else {
                _this.game.scale.startFullScreen(true);
            }
        });
        this.fullScreenButton.create(1415, 55);
        this.soundButton = createSoundButton_1.default(this.game);
        this.exitButton = new ExitButton_1.default(this.game, function () {
            alert('Pressed exit button');
        });
        this.exitButton.create(1787, 55);
    };
    LobbyNew.prototype.onGameStart = function () {
        var _this = this;
        this.lobbyMusic.fadeOut(500);
        this.game.world.bringToTop(this.bush.getSprite());
        this.bush.close(function () {
            _this.game.state.start('Strategy', true);
        });
    };
    LobbyNew.prototype.shutdown = function () {
        this.bg.destroy(true);
        Socket_1.default.onLobbyJoin.remove(this.events.onLobbyJoin, this);
        Socket_1.default.onRoomJoined.remove(this.onRoomJoin, this);
        Socket_1.default.onGameStarted.remove(this.onGameStart, this);
        Socket_1.default.onDisconnect.remove(this.onPlayerDisconnected, this);
        if (this.lobbyStatusBar) {
            this.lobbyStatusBar.destroy();
        }
        if (this.countDownHandle) {
            this.countDownHandle.destroy();
        }
    };
    return LobbyNew;
}(State));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LobbyNew;
