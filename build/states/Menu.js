"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Socket_1 = require("../socket/Socket");
var Menu = (function (_super) {
    __extends(Menu, _super);
    function Menu() {
        _super.call(this);
    }
    Menu.prototype.create = function () {
        var _this = this;
        Socket_1.default.init();
        this.bg = this.game.add.sprite(0, 0, 'background');
        this.logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'sprites', 'logo.png');
        this.logo.alpha = 0;
        this.logo.anchor.set(0.5, 0.5);
        this.playButton = this.game.add.sprite(701, 648, 'sprites', 'play-button.png');
        this.playButton.inputEnabled = true;
        this.playButton.alpha = 0;
        var tween = this.game.add.tween(this.logo)
            .to({ alpha: 1 }, 1000, "Linear", false);
        tween.onComplete.add(function () {
            _this.showPlayButton();
        });
        tween.start();
    };
    Menu.prototype.showPlayButton = function () {
        var _this = this;
        var tween = this.game.add.tween(this.playButton)
            .to({ alpha: 1 }, 1000, "Linear", false);
        tween.onComplete.add(function () {
            _this.playButton.events.onInputDown.add(function () {
                _this.game.state.start('Lobby', true);
                _this.fadeOut();
            });
        });
        tween.start();
    };
    Menu.prototype.fadeOut = function () {
    };
    Menu.prototype.shutdown = function () {
        this.bg = null;
    };
    return Menu;
}(Phaser.State));
exports.Menu = Menu;
