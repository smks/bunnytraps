"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Logs_1 = require("../components/Logs/Logs");
var Grid_1 = require("../components/Grid/Grid");
var CountdownTimer_1 = require("../components/Timer/CountdownTimer");
var PointsIndicator_1 = require("../components/PointsIndicator/PointsIndicator");
var BunnyPlacer_1 = require("../placers/BunnyPlacer");
var TrapPlacer_1 = require("../placers/TrapPlacer");
var Signal = Phaser.Signal;
var State = Phaser.State;
var save_1 = require("../storage/save");
var createGameBackground_1 = require('../helpers/create/createGameBackground');
var createInstructionsBanner_1 = require('../helpers/create/createInstructionsBanner');
var createBunnyIndicator_1 = require('../helpers/create/createBunnyIndicator');
var createLargeReadyButton_1 = require('../helpers/create/createLargeReadyButton');
var createFullScreenButton_1 = require('../helpers/create/createFullScreenButton');
var createSoundButton_1 = require('../helpers/create/createSoundButton');
var createExitButton_1 = require('../helpers/create/createExitButton');
var createBush_1 = require('../helpers/create/createBush');
var Assets_1 = require("../Assets");
var Strategy = (function (_super) {
    __extends(Strategy, _super);
    function Strategy() {
        _super.call(this);
        this.placementTotal = 5;
        this.currentPlayerIndex = 1;
        this.playerScore = 0;
        this.hasPlacedAllBunnies = false;
    }
    Strategy.prototype.init = function () {
        this.currentPlayerIndex = 1;
        this.playerScore = 0;
    };
    Strategy.prototype.create = function () {
        var _this = this;
        this.save = new save_1.default();
        this.playMusic();
        this.addBackground();
        this.addInstructionsBanner();
        this.addBunnyTurnIndicators();
        this.addBigActionButton();
        this.addActionButton();
        this.addCountdownTimer();
        this.addPointsIndicator();
        this.addLogs();
        this.bush = createBush_1.default(this.game, true, function () {
            _this.instructionsBanner.changeMessage("Place " + _this.placementTotal + " bunnies on the grid");
        });
        this.addListeners();
        this.addGrid();
        this.addBunnyPlacement();
        this.addTrapPlacement();
        this.listenForBunnyPlacement();
    };
    Strategy.prototype.addListeners = function () {
        this.addGridListeners();
        this.addBunnyPlacementListeners();
        this.addTrapPlacementListeners();
    };
    Strategy.prototype.addGridListeners = function () {
        var _this = this;
        this.placeTileSignal = new Phaser.Signal();
        this.placeTileSignal.add(function (params) {
            var numbersLeft;
            var subject = '';
            var placements;
            if (_this.hasPlacedAllBunnies === true) {
                placements = _this.trapPlacement.getPlantedCoordinates().length;
                subject = (placements === (_this.placementTotal - 2)) ? 'trap' : 'traps';
            }
            else {
                placements = _this.bunnyPlacement.getPlantedCoordinates().length;
                subject = (placements === (_this.placementTotal - 2)) ? 'bunny' : 'bunnies';
            }
            numbersLeft = _this.placementTotal - (placements + 1);
            _this.instructionsBanner.changeMessage(numbersLeft + " " + subject + " left");
            console.log('Pressed tile');
            console.log(params);
        });
    };
    Strategy.prototype.addBunnyPlacementListeners = function () {
        var _this = this;
        this.bunnyPlacementComplete = new Signal();
        this.bunnyPlacementComplete.add(this.enableReadyButton, this);
        this.bunnyPlacementComplete.add(function () {
            _this.instructionsBanner.changeMessage('Hit ready below');
            _this.readyButton.setOnClick(_this.enableTrapPlacement.bind(_this));
        }, this);
    };
    Strategy.prototype.enableReadyButton = function (isEnabled) {
        if (isEnabled === void 0) { isEnabled = true; }
        this.readyButton.setEnabled(isEnabled);
    };
    Strategy.prototype.enableTrapPlacement = function () {
        this.hasPlacedAllBunnies = true;
        this.instructionsBanner.changeMessage('Add 5 traps to the grid');
        this.enableReadyButton(false);
        this.bunnyPlacement.ignore();
        this.bunnyPlacement.clear();
        this.listenForTrapPlacement();
    };
    Strategy.prototype.addTrapPlacementListeners = function () {
        var _this = this;
        this.trapPlacementComplete = new Signal();
        this.trapPlacementComplete.add(this.enableReadyButton.bind(this));
        this.trapPlacementComplete.add(function () {
            _this.instructionsBanner.changeMessage('Hit ready below');
            _this.readyButton.setOnClick(function () {
                _this.enableReadyButton(false);
                _this.disableTrapPlacement();
                _this.removeAllTweens();
                _this.switchStateWithBushAnimation();
            });
        });
    };
    Strategy.prototype.removeAllTweens = function () {
        this.game.tweens.removeAll();
    };
    Strategy.prototype.disableTrapPlacement = function () {
        this.trapPlacement.clear();
        this.trapPlacement.ignore();
    };
    Strategy.prototype.switchStateWithBushAnimation = function () {
        var _this = this;
        this.game.world.bringToTop(this.bush.getSprite());
        this.strategyMusic.fadeOut(500);
        this.storeResults();
        this.bush.close(function () {
            _this.game.state.start('Results', true);
        });
    };
    Strategy.prototype.storeResults = function () {
        var decisions = {
            playerId: 1,
            bunnyId: 1,
            bunnyPlacements: this.bunnyPlacement.getPlantedCoordinates(),
            trapPlacements: this.trapPlacement.getPlantedCoordinates(),
        };
        this.save.storeDecisions(1, decisions);
    };
    Strategy.prototype.render = function () {
        this.timer.render();
    };
    Strategy.prototype.playMusic = function () {
        this.strategyMusic = this.game.add.audio(Assets_1.default.AUDIO_KEY_AUDIO_IN_GAME_01, 1, true);
        this.strategyMusic.play();
    };
    Strategy.prototype.addBackground = function () {
        this.gameBg = createGameBackground_1.default(this.game);
    };
    Strategy.prototype.addInstructionsBanner = function () {
        this.instructionsBanner = createInstructionsBanner_1.default(this.game, 'Hello');
    };
    Strategy.prototype.addBunnyTurnIndicators = function () {
        var _this = this;
        this.bunnyOne = createBunnyIndicator_1.default(this.game, 160.5, 295, 1, this.save.getBunnyCount(1));
        this.bunnyTwo = createBunnyIndicator_1.default(this.game, 290.5, 295, 2, this.save.getBunnyCount(2));
        this.bunnyThree = createBunnyIndicator_1.default(this.game, 420.5, 295, 3, this.save.getBunnyCount(3));
        this.bunnyFour = createBunnyIndicator_1.default(this.game, 550.5, 295, 4, this.save.getBunnyCount(4));
        var bunnies = [];
        bunnies.push(this.bunnyOne);
        bunnies.push(this.bunnyTwo);
        bunnies.push(this.bunnyThree);
        bunnies.push(this.bunnyFour);
        bunnies.map(function (bunny, index) {
            bunny.moveForward(_this.save.getBunnyCount(index + 1));
        });
    };
    Strategy.prototype.addBigActionButton = function () {
        this.readyButton = createLargeReadyButton_1.default(this.game);
    };
    Strategy.prototype.addActionButton = function () {
        this.fullScreenButton = createFullScreenButton_1.default(this.game);
        this.soundButton = createSoundButton_1.default(this.game);
        this.exitButton = createExitButton_1.default(this.game);
    };
    Strategy.prototype.addGrid = function () {
        this.grid = new Grid_1.default(this.game, this.placeTileSignal, this.placementComplete);
        this.grid.create(856.5, 202.5);
        this.grid.addInteraction();
    };
    Strategy.prototype.addLogs = function () {
        this.logs = new Logs_1.default(this.game, 355, 620);
    };
    Strategy.prototype.addLogMessage = function (message) {
        this.logs.addLogMessage(message);
    };
    Strategy.prototype.addCountdownTimer = function () {
        this.timer = new CountdownTimer_1.default(this.game, 0, 20);
        this.timer.create(857, 55);
        this.timer.start();
        this.timer.addFinalSecondsCallback(this.onFinalSeconds.bind(this));
        this.timer.addFinalSecondIteration(this.onFinalSecondIteration.bind(this));
    };
    Strategy.prototype.onFinalSeconds = function () {
        this.strategyMusic.fadeOut(200);
    };
    Strategy.prototype.onFinalSecondIteration = function () {
        this.game.camera.flash(0xf73d2f, 200);
    };
    Strategy.prototype.addPointsIndicator = function () {
        this.pointsIndicator = new PointsIndicator_1.default(this.game);
        var played = parseInt(this.save.getRoomGamesPlayed(), 10) + 1;
        this.pointsIndicator.create(1136, 55, "Round " + played + " of 5");
    };
    Strategy.prototype.addBunnyPlacement = function () {
        var playerID = 1;
        this.bunnyPlacement = new BunnyPlacer_1.default(this.game, this.placeTileSignal, playerID, this.bunnyPlacementComplete);
        this.bunnyPlacement.ignore();
    };
    Strategy.prototype.addTrapPlacement = function () {
        this.trapPlacement = new TrapPlacer_1.default(this.game, this.placeTileSignal, this.trapPlacementComplete);
        this.trapPlacement.ignore();
    };
    Strategy.prototype.listenForBunnyPlacement = function () {
        this.bunnyPlacement.listen();
    };
    Strategy.prototype.listenForTrapPlacement = function () {
        this.trapPlacement.listen();
    };
    Strategy.prototype.shutdown = function () {
        this.grid.destroy();
        this.instructionsBanner.destroy();
        this.logs.destroy();
        this.strategyMusic.destroy(true);
        this.timer.stopMusic();
        this.timer.destroy();
    };
    return Strategy;
}(State));
exports.Strategy = Strategy;
