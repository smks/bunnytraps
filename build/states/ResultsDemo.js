"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Logs_1 = require("../components/Logs/Logs");
var Grid_1 = require("../components/Grid/Grid");
var CountdownTimer_1 = require("../components/Timer/CountdownTimer");
var PointsIndicator_1 = require("../components/PointsIndicator/PointsIndicator");
var BunnyPlacer_1 = require("../placers/BunnyPlacer");
var bunny_id_by_player_1 = require('../helpers/bunny-id-by-player');
var createGameBackground_1 = require('../helpers/create/createGameBackground');
var createInstructionsBanner_1 = require('../helpers/create/createInstructionsBanner');
var createBunnyIndicator_1 = require('../helpers/create/createBunnyIndicator');
var createLargeReadOnlyButton_1 = require('../helpers/create/createLargeReadOnlyButton');
var createFullScreenButton_1 = require('../helpers/create/createFullScreenButton');
var createSoundButton_1 = require('../helpers/create/createSoundButton');
var createExitButton_1 = require('../helpers/create/createExitButton');
var createBush_1 = require('../helpers/create/createBush');
var ResultsBuilder_1 = require('../builders/ResultsBuilder');
var CPU_1 = require('../decisions/CPU');
var save_1 = require('../storage/save');
var ResultsDemo = (function (_super) {
    __extends(ResultsDemo, _super);
    function ResultsDemo() {
        _super.call(this);
        this.bunnies = new Array();
        this.latestPlayerIdTurn = 1;
        this.totalMoveCount = 0;
    }
    ResultsDemo.prototype.create = function () {
        this.save = new save_1.default();
        this.addBackground();
        this.addInstructionsBanner();
        this.addBunnyTurnIndicators();
        this.addBigActionButton();
        this.addActionButton();
        this.addCountdownTimer();
        this.addMessageBoard();
        this.addLogs();
        this.addGrid();
        this.bush = createBush_1.default(this.game, true);
        this.addBunnyPlacers();
        var results = this.getResults();
        this.resultsBuilder = new ResultsBuilder_1.default(results);
        this.resultsBuilder.createMoves();
        this.moves = this.resultsBuilder.getMoves();
        this.totalMoveCount = this.moves.length;
        this.game.time.events.add(Phaser.Timer.SECOND, this.processMove, this);
    };
    ResultsDemo.prototype.getResults = function () {
        var player1 = this.save.getDecisions(1);
        var cpu2 = new CPU_1.default().generateDecisions(2, 2);
        var cpu3 = new CPU_1.default().generateDecisions(3, 3);
        var cpu4 = new CPU_1.default().generateDecisions(4, 4);
        var results = {
            player1Results: player1,
            player2Results: cpu2,
            player3Results: cpu3,
            player4Results: cpu4,
        };
        console.log(JSON.stringify(results));
        return results;
    };
    ResultsDemo.prototype.concludeResults = function () {
        var _this = this;
        this[("bunny" + this.latestPlayerIdTurn)].moveBackward();
        this.save.roomGameFinished();
        this.bush.close(function () {
            _this.game.state.start('Strategy', true);
        });
    };
    ResultsDemo.prototype.processMove = function () {
        if (this.moves.length == 0) {
            this.concludeResults();
            return;
        }
        this.pointsIndicator.setMessage("Bunny " + ((this.totalMoveCount - this.moves.length) + 1) + " / " + this.totalMoveCount);
        var move = this.moves.shift();
        var playerId = move.playerId;
        this.latestPlayerIdTurn = playerId;
        var columnIndex = move.columnIndex;
        var rowIndex = move.rowIndex;
        var playersWhoHitBunny = move.gotHitByPlayers;
        var isBeingLaunched = playersWhoHitBunny.length > 0;
        this.instructionsBanner.changeMessage("Player " + playerId + "'s turn");
        var _a = this.grid.getCoordinatesByIndex(columnIndex, rowIndex), tileXPos = _a.tileXPos, tileYPos = _a.tileYPos;
        this[("bunnyPlacement" + playerId)].addBunny({
            columnIndex: columnIndex,
            rowIndex: rowIndex,
            tileXPos: tileXPos,
            tileYPos: tileYPos
        });
        this.bunnies.map(function (bunny) {
            bunny.moveBackward();
        });
        this[("bunny" + playerId)].moveForward(this.save.getBunnyCount(playerId));
        this.game.time.events.add(Phaser.Timer.SECOND * 4, this.determineBunnyFate.bind(this, isBeingLaunched, columnIndex, rowIndex, playerId), this);
    };
    ResultsDemo.prototype.addLogMessage = function (message) {
        this.logs.addLogMessage(message);
    };
    ResultsDemo.prototype.determineBunnyFate = function (isBeingLaunched, columnIndex, rowIndex, playerId) {
        var _this = this;
        if (isBeingLaunched) {
            this.grid.shootUpTile(columnIndex, rowIndex);
            this[("bunny" + playerId)].reduceCount();
            this.save.reduceBunnyCount(playerId);
            this[("bunnyPlacement" + playerId)].shootBunnyAway(function () {
                _this.game.camera.flash(0xf73d2f, 200);
                _this.game.time.events.add(Phaser.Timer.SECOND, _this.addLogMessage.bind(_this, "Player " + playerId + " just lost a bunny"), _this);
                console.log('hit screen');
            }, function () {
                _this.processMove();
            });
        }
        else {
            console.log('avoided trap!');
            this.addLogMessage("Player " + playerId + "'s bunny just survived");
            this[("bunnyPlacement" + playerId)].survived();
            this.processMove();
        }
    };
    ResultsDemo.prototype.update = function () {
    };
    ResultsDemo.prototype.render = function () {
        this.timer.render();
    };
    ResultsDemo.prototype.addBackground = function () {
        this.gameBg = createGameBackground_1.default(this.game);
    };
    ResultsDemo.prototype.addInstructionsBanner = function () {
        this.instructionsBanner = createInstructionsBanner_1.default(this.game, 'Results');
    };
    ResultsDemo.prototype.addBunnyTurnIndicators = function () {
        this.bunny1 = createBunnyIndicator_1.default(this.game, 160.5, 295, bunny_id_by_player_1.default(1), this.save.getBunnyCount(1));
        this.bunnies.push(this.bunny1);
        this.bunny2 = createBunnyIndicator_1.default(this.game, 290.5, 295, bunny_id_by_player_1.default(2), this.save.getBunnyCount(2));
        this.bunnies.push(this.bunny2);
        this.bunny3 = createBunnyIndicator_1.default(this.game, 420.5, 295, bunny_id_by_player_1.default(3), this.save.getBunnyCount(3));
        this.bunnies.push(this.bunny3);
        this.bunny4 = createBunnyIndicator_1.default(this.game, 550.5, 295, bunny_id_by_player_1.default(4), this.save.getBunnyCount(4));
        this.bunnies.push(this.bunny4);
    };
    ResultsDemo.prototype.addBigActionButton = function () {
        this.largeReadOnlyButton = createLargeReadOnlyButton_1.default(this.game);
    };
    ResultsDemo.prototype.addActionButton = function () {
        this.fullScreenButton = createFullScreenButton_1.default(this.game);
        this.soundButton = createSoundButton_1.default(this.game);
        this.exitButton = createExitButton_1.default(this.game);
    };
    ResultsDemo.prototype.addGrid = function () {
        this.placeTileSignal = new Phaser.Signal();
        this.placeTileSignal.add(function (params) {
            console.log('Pressed tile');
            console.log(params);
        });
        var placementComplete = new Phaser.Signal();
        placementComplete.add(function () {
            console.log('Tiles finished placement');
        });
        this.grid = new Grid_1.default(this.game, null, null, false);
        this.grid.create(856.5, 202.5);
    };
    ResultsDemo.prototype.addLogs = function () {
        this.logs = new Logs_1.default(this.game, 355, 620);
    };
    ResultsDemo.prototype.addCountdownTimer = function () {
        this.timer = new CountdownTimer_1.default(this.game, 0, 0);
        this.timer.create(857, 55);
    };
    ResultsDemo.prototype.addMessageBoard = function () {
        this.pointsIndicator = new PointsIndicator_1.default(this.game);
        this.pointsIndicator.create(1136, 55, "Bunny 1 / " + this.totalMoveCount);
    };
    ResultsDemo.prototype.addBunnyPlacers = function () {
        this.bunnyPlacement1 = new BunnyPlacer_1.default(this.game, null, bunny_id_by_player_1.default(1));
        this.bunnyPlacement2 = new BunnyPlacer_1.default(this.game, null, bunny_id_by_player_1.default(2));
        this.bunnyPlacement3 = new BunnyPlacer_1.default(this.game, null, bunny_id_by_player_1.default(3));
        this.bunnyPlacement4 = new BunnyPlacer_1.default(this.game, null, bunny_id_by_player_1.default(4));
    };
    ResultsDemo.prototype.shutdown = function () {
        this.grid.destroy();
        this.instructionsBanner.destroy();
        this.logs.destroy();
    };
    return ResultsDemo;
}(Phaser.State));
exports.ResultsDemo = ResultsDemo;
