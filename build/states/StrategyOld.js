"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Platforms_1 = require("../components/Platforms");
var save_1 = require("../storage/save");
var Strategy = (function (_super) {
    __extends(Strategy, _super);
    function Strategy() {
        _super.call(this);
        this.readyToPlay = new Phaser.Signal();
        this.actionsList = new Array();
    }
    Strategy.prototype.create = function () {
        var _this = this;
        this.bg = this.game.add.sprite(0, 0, 'sprites', 'play-background.jpg');
        this.save = save_1.default;
        this.readyToPlay.add(function (decisions) {
            var made = decisions.placementsMade;
            if (made == 5) {
                _this.instructionText.text = 'SET YOUR TRAPS';
            }
        });
        this.readyToPlay.add(function (decisions) {
            var made = decisions.placementsMade;
            if (made == 10) {
                _this.setupPlatforms.disableInputs();
                _this.instructionText.text = 'WAITING FOR PLAYER';
                _this.createEnemySelections();
                _this.game.state.start('Results');
            }
        });
        this.setupPlatforms = new Platforms_1.default(this.game, this.save);
        this.setupPlatforms.x = 548;
        this.setupPlatforms.y = 70;
        this.setupPlatforms.initPrepare(this.readyToPlay);
        this.instructionText = this.game.add.text(275, 177, "PLACE BUNNIES", {
            font: "50px Bombardier",
            fill: "#d6485e",
            align: "center"
        });
        this.instructionText.anchor.set(0.5, 0.5);
        this.instructionText.border = true;
        this.playerOneText = this.game.add.text(104, 257, "dan_88", {
            font: "40px Bombardier",
            fill: "#fff",
            align: "center"
        });
        this.playerTwoText = this.game.add.text(324, 257, "smks", {
            font: "40px Bombardier",
            fill: "#fff",
            align: "center"
        });
        this.actionsText = this.game.add.text(70, 518, this.getActionMessages(), {
            font: "24px Bombardier",
            fill: "#fff",
            align: "left"
        });
        this.actionsText.lineHeight = -20;
    };
    Strategy.prototype.getActionMessages = function () {
        var messages = '';
        this.actionsList.map(function (message) {
            messages = +message + '\n';
        });
        return messages;
    };
    Strategy.prototype.addActionMessage = function (message) {
        if (this.actionsList.length > 10) {
            this.actionsList.pop();
        }
        this.actionsList.unshift(message);
        this.actionsText.text = this.getActionMessages();
    };
    Strategy.prototype.destroy = function () {
    };
    Strategy.prototype.createEnemySelections = function () {
        var enemySelections = {
            bunnyPlacements: [],
            trapPlacements: []
        };
        enemySelections.bunnyPlacements = [
            { columnIndex: 0, rowIndex: 1 },
            { columnIndex: 1, rowIndex: 1 },
            { columnIndex: 2, rowIndex: 1 },
            { columnIndex: 3, rowIndex: 1 },
            { columnIndex: 4, rowIndex: 1 }
        ];
        enemySelections.trapPlacements = [
            { columnIndex: 0, rowIndex: 3 },
            { columnIndex: 1, rowIndex: 3 },
            { columnIndex: 2, rowIndex: 3 },
            { columnIndex: 3, rowIndex: 3 },
            { columnIndex: 4, rowIndex: 3 }
        ];
        this.save.storeEnemyDecisions(enemySelections);
    };
    return Strategy;
}(Phaser.State));
exports.Strategy = Strategy;
