"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require('../Assets');
var Hints_1 = require('../components/Hints/Hints');
var Timer = Phaser.Timer;
var PercentageLoader_1 = require("../components/Loader/PercentageLoader");
var Bush_1 = require('../components/Cover/Bush');
var Preload = (function (_super) {
    __extends(Preload, _super);
    function Preload() {
        _super.apply(this, arguments);
    }
    Preload.prototype.preload = function () {
        this.addBackground();
        this.addPercentageLoader();
        this.addHint();
        this.game.load.atlas(Assets_1.default.LOBBY_ASSETS, Assets_1.default.ATLAS_LOBBY, Assets_1.default.ATLAS_LOBBY_JSON, Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
        this.game.load.atlas(Assets_1.default.GAME_ASSETS, Assets_1.default.ATLAS_GAME, Assets_1.default.ATLAS_GAME_JSON, Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
        this.game.load.atlas(Assets_1.default.BUNNIES_ASSETS, Assets_1.default.ATLAS_BUNNIES, Assets_1.default.ATLAS_BUNNIES_JSON, Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
        this.game.load.atlas(Assets_1.default.BUSH_ASSETS, Assets_1.default.ATLAS_BUSH, Assets_1.default.ATLAS_BUSH_JSON, Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
        this.game.load.image('lobby-bg', Assets_1.default.LOBBY_BG);
        this.game.load.image('game-bg', Assets_1.default.GAME_BG);
        this.game.load.bitmapFont(Assets_1.default.FONT_INVERSE, '/assets/fonts/FreckleFaceWhite.png', '/assets/fonts/FreckleFaceWhite.fnt');
        this.game.load.bitmapFont(Assets_1.default.FONT_BUTTONS, '/assets/fonts/DentOne.png', '/assets/fonts/DentOne.fnt');
        this.game.load.bitmapFont(Assets_1.default.FONT_LOGS, '/assets/fonts/Roboto.png', '/assets/fonts/Roboto.fnt');
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_MAIN_THEME_01, Assets_1.default.AUDIO_MAIN_THEME_01);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_IN_GAME_01, Assets_1.default.AUDIO_IN_GAME_01);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_THEME_02, Assets_1.default.AUDIO_THEME_02);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_AMBIENCE_LOOP, Assets_1.default.AUDIO_AMBIENCE_LOOP);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_DROP, Assets_1.default.AUDIO_BUNNY_DROP);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_SET_TRAP, Assets_1.default.AUDIO_SET_TRAP);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_HITS_SCREEN, Assets_1.default.AUDIO_BUNNY_HITS_SCREEN);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SLIDE, Assets_1.default.AUDIO_BUNNY_SLIDE);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SOUND_01, Assets_1.default.AUDIO_BUNNY_SOUND_01);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SOUND_02, Assets_1.default.AUDIO_BUNNY_SOUND_02);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SOUND_03, Assets_1.default.AUDIO_BUNNY_SOUND_03);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SOUND_04, Assets_1.default.AUDIO_BUNNY_SOUND_04);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SOUND_05, Assets_1.default.AUDIO_BUNNY_SOUND_05);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_FLYING_01, Assets_1.default.AUDIO_BUNNY_FLYING_01);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_FLYING_02, Assets_1.default.AUDIO_BUNNY_FLYING_02);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_FLYING_03, Assets_1.default.AUDIO_BUNNY_FLYING_03);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_FLYING_04, Assets_1.default.AUDIO_BUNNY_FLYING_04);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_FLYING_05, Assets_1.default.AUDIO_BUNNY_FLYING_05);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUSH_COLLIDE, Assets_1.default.AUDIO_BUSH_COLLIDE);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUSH_SEPARATE, Assets_1.default.AUDIO_BUSH_SEPARATE);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_BUTTON_PRESS, Assets_1.default.AUDIO_BUTTON_PRESS);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_CHANGE_PLAYER, Assets_1.default.AUDIO_CHANGE_PLAYER);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_CLOCK_TICK, Assets_1.default.AUDIO_CLOCK_TICK);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_CROWD_CHEERING, Assets_1.default.AUDIO_CROWD_CHEERING);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_LOG_APPEAR, Assets_1.default.AUDIO_LOG_APPEAR);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_NEW_MESSAGE, Assets_1.default.AUDIO_NEW_MESSAGE);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_REVEAL_CARD, Assets_1.default.AUDIO_REVEAL_CARD);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_TILE_LAND, Assets_1.default.AUDIO_TILE_LAND);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_TILE_SPRING, Assets_1.default.AUDIO_TILE_SPRING);
        this.game.load.audio(Assets_1.default.AUDIO_KEY_AUDIO_TIME_UP, Assets_1.default.AUDIO_TIME_UP);
    };
    Preload.prototype.loadUpdate = function () {
        this.loader.setPercentage(this.game.load.progress);
    };
    Preload.prototype.addBackground = function () {
        this.bg = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'loading-bg');
        this.bg.anchor.set(0.5, 0.5);
    };
    Preload.prototype.addHint = function () {
        var options = {
            xPos: 950,
            yPos: 935,
            fontSize: 60,
            font: Assets_1.default.FONT,
            align: 'center',
            anchorX: 0.5,
            anchorY: 0.5
        };
        var timeToWaitNextHint = Timer.SECOND * 4;
        this.hints = new Hints_1.default(this.game, options, timeToWaitNextHint);
    };
    Preload.prototype.addPercentageLoader = function () {
        this.loader = new PercentageLoader_1.default(this.game);
        this.loader.create(this.game.world.centerX, 280);
        this.loader.setPercentage(this.game.load.progress);
    };
    Preload.prototype.update = function () {
    };
    Preload.prototype.create = function () {
        var _this = this;
        var bush = new Bush_1.default(this.game);
        bush.create(0, 0);
        bush.close(function () {
            if (!window['showPreloader']) {
                _this.game.state.start('Lobby', true, false);
            }
        });
    };
    Preload.prototype.shutdown = function () {
        this.bg.destroy(true);
        this.hints.destroy();
    };
    return Preload;
}(Phaser.State));
exports.Preload = Preload;
