"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Assets_1 = require("../Assets");
var Logs_1 = require("../components/Logs/Logs");
var Grid_1 = require("../components/Grid/Grid");
var RandomDataGenerator = Phaser.RandomDataGenerator;
var CountdownTimer_1 = require("../components/Timer/CountdownTimer");
var PointsIndicator_1 = require("../components/PointsIndicator/PointsIndicator");
var BunnyPlacer_1 = require("../placers/BunnyPlacer");
var TrapPlacer_1 = require("../placers/TrapPlacer");
var Signal = Phaser.Signal;
var save_1 = require("../storage/save");
var State = Phaser.State;
var createGameBackground_1 = require('../helpers/create/createGameBackground');
var createInstructionsBanner_1 = require('../helpers/create/createInstructionsBanner');
var createBunnyIndicator_1 = require('../helpers/create/createBunnyIndicator');
var createLargeReadyButton_1 = require('../helpers/create/createLargeReadyButton');
var createFullScreenButton_1 = require('../helpers/create/createFullScreenButton');
var createSoundButton_1 = require('../helpers/create/createSoundButton');
var createExitButton_1 = require('../helpers/create/createExitButton');
var createBush_1 = require('../helpers/create/createBush');
var Strategy = (function (_super) {
    __extends(Strategy, _super);
    function Strategy() {
        _super.call(this);
        this.tempInterval = 0;
        this.tempInterval2 = 0;
    }
    Strategy.prototype.create = function () {
        this.save = new save_1.default();
        this.addBackground();
        this.addInstructionsBanner();
        this.addBunnyTurnIndicators();
        this.addBigActionButton();
        this.addActionButton();
        this.addCountdownTimer();
        this.addPointsIndicator();
        this.addLogs();
        this.addGrid();
        this.bush = createBush_1.default(this.game, true);
        this.addBunnyPlacement();
        this.addTrapPlacement();
        this.listenForBunnyPlacement();
    };
    Strategy.prototype.update = function () {
        this.pointsIndicator.increment(100);
    };
    Strategy.prototype.render = function () {
        this.timer.render();
    };
    Strategy.prototype.playMusic = function () {
        this.strategyMusic = this.game.add.audio(Assets_1.default.AUDIO_KEY_AUDIO_IN_GAME_01, 1, true);
        this.strategyMusic.play();
    };
    Strategy.prototype.addBackground = function () {
        this.gameBg = createGameBackground_1.default(this.game);
    };
    Strategy.prototype.addInstructionsBanner = function () {
        var _this = this;
        this.instructionsBanner = createInstructionsBanner_1.default(this.game);
        this.tempInterval3 = setInterval(function () {
            _this.instructionsBanner.changeRandomMessage();
        }, 2000);
    };
    Strategy.prototype.addBunnyTurnIndicators = function () {
        this.bunnyOne = createBunnyIndicator_1.default(this.game, 160.5, 295, 1, this.save.getBunnyCount(1));
        this.bunnyTwo = createBunnyIndicator_1.default(this.game, 290.5, 295, 2, this.save.getBunnyCount(2));
        this.bunnyThree = createBunnyIndicator_1.default(this.game, 420.5, 295, 3, this.save.getBunnyCount(3));
        this.bunnyFour = createBunnyIndicator_1.default(this.game, 550.5, 295, 5, this.save.getBunnyCount(4));
        var bunnies = [];
        var bIndex = 0;
        bunnies.push(this.bunnyOne);
        bunnies.push(this.bunnyTwo);
        bunnies.push(this.bunnyThree);
        bunnies.push(this.bunnyFour);
        var gen = new RandomDataGenerator([]);
        this.tempInterval2 = setInterval(function () {
            for (var i = 0; i < bunnies.length; i++) {
                if (i == bIndex) {
                    bunnies[i].moveForward(gen.integerInRange(0, 25));
                }
                else {
                    bunnies[i].moveBackward();
                }
            }
            bIndex++;
            if (bIndex > (bunnies.length - 1)) {
                bIndex = 0;
            }
        }, 2000);
    };
    Strategy.prototype.addBigActionButton = function () {
        this.readyButton = createLargeReadyButton_1.default(this.game);
    };
    Strategy.prototype.addActionButton = function () {
        this.fullScreenButton = createFullScreenButton_1.default(this.game);
        this.soundButton = createSoundButton_1.default(this.game);
        this.exitButton = createExitButton_1.default(this.game);
    };
    Strategy.prototype.addGrid = function () {
        this.placeTileSignal = new Phaser.Signal();
        this.placeTileSignal.add(function (params) {
            console.log('Pressed tile');
            console.log(params);
        });
        var placementComplete = new Phaser.Signal();
        placementComplete.add(function () {
            console.log('Tiles finished placement');
        });
        this.grid = new Grid_1.default(this.game, this.placeTileSignal, placementComplete);
        this.grid.create(856.5, 202.5);
        this.grid.addInteraction();
    };
    Strategy.prototype.addLogs = function () {
        var _this = this;
        this.logs = new Logs_1.default(this.game, 355, 620);
        var messages = [
            "Player {no} just placed a bunny",
            "Player {no} just placed a trap",
            'Player {no} just used a power up',
            'Player {no} just lost a bunny',
            'Player {no} won the round',
        ];
        var gen = new RandomDataGenerator([]);
        var addMessage = function () {
            var playerNo = gen.integerInRange(1, 4);
            var message = (Phaser.ArrayUtils.getRandomItem(messages) + '').replace(/\{no\}/, playerNo.toString());
            _this.logs.addLogMessage(message);
        };
        addMessage();
        this.tempInterval = setInterval(function () {
            addMessage();
        }, 2000);
    };
    Strategy.prototype.addCountdownTimer = function () {
        this.timer = new CountdownTimer_1.default(this.game, 2, 30);
        this.timer.create(857, 55);
        this.timer.start();
    };
    Strategy.prototype.addPointsIndicator = function () {
        this.pointsIndicator = new PointsIndicator_1.default(this.game);
        this.pointsIndicator.create(1136, 55, 0);
    };
    Strategy.prototype.addBunnyPlacement = function () {
        var _this = this;
        this.bunnyPlacementComplete = new Signal();
        this.bunnyPlacementComplete.add(function () {
            _this.readyButton.setEnabled(true);
            _this.readyButton.setOnClick(function () {
                _this.readyButton.setEnabled(false);
                _this.bunnyPlacement.ignore();
                _this.bunnyPlacement.clear();
                _this.listenForTrapPlacement();
            });
        });
        this.bunnyPlacement = new BunnyPlacer_1.default(this.game, this.placeTileSignal, 6, this.bunnyPlacementComplete);
        this.bunnyPlacement.ignore();
    };
    Strategy.prototype.addTrapPlacement = function () {
        var _this = this;
        this.trapPlacementComplete = new Signal();
        this.trapPlacementComplete.add(function () {
            _this.readyButton.setEnabled(true);
            _this.readyButton.setOnClick(function () {
                _this.readyButton.setEnabled(false);
                _this.trapPlacement.clear();
                _this.trapPlacement.ignore();
                _this.removeIntervals();
                _this.game.tweens.removeAll();
                _this.storeResults();
                _this.game.world.bringToTop(_this.bush.getSprite());
                _this.bush.close(function () {
                    _this.game.state.start('Results', true);
                });
            });
        });
        this.trapPlacement = new TrapPlacer_1.default(this.game, this.placeTileSignal, this.trapPlacementComplete);
        this.trapPlacement.ignore();
    };
    Strategy.prototype.storeResults = function () {
        var decisions;
        decisions.playerId = 1;
        decisions.bunnyId = 1;
        decisions.bunnyPlacements = this.bunnyPlacement.getPlantedCoordinates();
        decisions.trapPlacements = this.trapPlacement.getPlantedCoordinates();
        this.save.storeDecisions(1, decisions);
    };
    Strategy.prototype.listenForBunnyPlacement = function () {
        this.bunnyPlacement.listen();
    };
    Strategy.prototype.listenForTrapPlacement = function () {
        this.trapPlacement.listen();
    };
    Strategy.prototype.removeIntervals = function () {
        clearInterval(this.tempInterval);
        clearInterval(this.tempInterval2);
        clearInterval(this.tempInterval3);
    };
    Strategy.prototype.shutdown = function () {
        this.removeIntervals();
        this.grid.destroy();
        this.instructionsBanner.destroy();
        this.logs.destroy();
    };
    return Strategy;
}(State));
exports.Strategy = Strategy;
