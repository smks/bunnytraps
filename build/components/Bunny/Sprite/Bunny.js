"use strict";
var Eyes_1 = require("./BodyParts/Eyes");
var Body_1 = require("./BodyParts/Body");
var Ear_1 = require("./BodyParts/Ear");
var Cheek_1 = require("./BodyParts/Cheek");
var Face_1 = require("./BodyParts/Face");
var Foot_1 = require("./BodyParts/Foot");
var Mouth_1 = require("./BodyParts/Mouth");
var Paw_1 = require("./BodyParts/Paw");
var ArrayUtils = Phaser.ArrayUtils;
var Assets_1 = require("../../../Assets");
var Bunny = (function () {
    function Bunny(game, bunnyId) {
        if (bunnyId === void 0) { bunnyId = 1; }
        this.bodyParts = {
            'body': null,
            'eyes': null,
            'earLeft': null,
            'earRight': null,
            'cheek': null,
            'face': null,
            'mouth': null,
            'pawLeft': null,
            'pawRight': null,
            'footLeft': null,
            'footRight': null,
        };
        this.bodyPartOriginalCoordinates = {
            'body': { x: 0, y: 0 },
            'eyes': { x: 0, y: -120 },
            'earLeft': { x: -157, y: -207 },
            'earRight': { x: 157, y: -207 },
            'cheek': { x: 0, y: 0 },
            'face': { x: 0, y: -58 },
            'mouth': { x: 0, y: 90 },
            'pawLeft': { x: -250, y: 44 },
            'pawRight': { x: 250, y: 44 },
            'footLeft': { x: -200, y: 120 },
            'footRight': { x: 200, y: 120 }
        };
        this.game = game;
        this.bunnyId = bunnyId;
    }
    Bunny.prototype.create = function (xPos, yPos, scale) {
        if (scale === void 0) { scale = 0.35; }
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.body = new Body_1.default(this.game, this.group);
        this.eyes = new Eyes_1.default(this.game, this.group);
        this.earLeft = new Ear_1.default(this.game, this.group);
        this.earRight = new Ear_1.default(this.game, this.group);
        this.cheek = new Cheek_1.default(this.game, this.group);
        this.face = new Face_1.default(this.game, this.group);
        this.mouth = new Mouth_1.default(this.game, this.group);
        this.leftPaw = new Paw_1.default(this.game, this.group);
        this.rightPaw = new Paw_1.default(this.game, this.group);
        this.leftFoot = new Foot_1.default(this.game, this.group);
        this.rightFoot = new Foot_1.default(this.game, this.group);
        this.bodyParts.body = this.body.create(0, 0, this.bunnyId);
        this.bodyParts.eyes = this.eyes.create(0, -120, this.bunnyId);
        var randomDelay = ArrayUtils.getRandomItem([3000, 3500, 4000, 4500, 5000]);
        this.bodyParts.earLeft = this.earLeft.create(-157, -207, this.bunnyId, randomDelay);
        this.bodyParts.earRight = this.earRight.create(157, -207, this.bunnyId, this.earLeft.TYPE_RIGHT, randomDelay);
        this.bodyParts.face = this.face.create(0, -58, this.bunnyId);
        this.bodyParts.mouth = this.mouth.create(0, 90, this.bunnyId);
        this.bodyParts.pawLeft = this.leftPaw.create(-250, 44, this.bunnyId);
        this.bodyParts.pawRight = this.rightPaw.create(250, 44, this.bunnyId, this.leftPaw.TYPE_RIGHT);
        this.bodyParts.footLeft = this.leftFoot.create(-200, 160, this.bunnyId);
        this.bodyParts.footRight = this.rightFoot.create(200, 160, this.bunnyId, this.leftFoot.TYPE_RIGHT);
        this.group.sendToBack(this.bodyParts.earLeft);
        this.group.sendToBack(this.bodyParts.earRight);
        this.group.sendToBack(this.bodyParts.footLeft);
        this.group.sendToBack(this.bodyParts.footRight);
        this.group.bringToTop(this.bodyParts.eyes);
        this.group.scale.set(scale, scale);
    };
    Bunny.prototype.resetBodyParts = function () {
        this.bodyParts.body.x = this.bodyPartOriginalCoordinates.body.x;
        this.bodyParts.body.y = this.bodyPartOriginalCoordinates.body.y;
        this.bodyParts.earLeft.x = this.bodyPartOriginalCoordinates.earLeft.x;
        this.bodyParts.earLeft.y = this.bodyPartOriginalCoordinates.earLeft.y;
        this.bodyParts.earRight.x = this.bodyPartOriginalCoordinates.earRight.x;
        this.bodyParts.earRight.y = this.bodyPartOriginalCoordinates.earRight.y;
        this.bodyParts.face.x = this.bodyPartOriginalCoordinates.face.x;
        this.bodyParts.face.y = this.bodyPartOriginalCoordinates.face.y;
        this.bodyParts.mouth.x = this.bodyPartOriginalCoordinates.mouth.x;
        this.bodyParts.mouth.y = this.bodyPartOriginalCoordinates.mouth.y;
        this.bodyParts.pawLeft.x = this.bodyPartOriginalCoordinates.pawLeft.x;
        this.bodyParts.pawLeft.y = this.bodyPartOriginalCoordinates.pawLeft.y;
        this.bodyParts.pawRight.x = this.bodyPartOriginalCoordinates.pawRight.x;
        this.bodyParts.pawRight.y = this.bodyPartOriginalCoordinates.pawRight.y;
        this.bodyParts.footLeft.x = this.bodyPartOriginalCoordinates.footLeft.x;
        this.bodyParts.footLeft.y = this.bodyPartOriginalCoordinates.footLeft.y;
        this.bodyParts.footRight.x = this.bodyPartOriginalCoordinates.footRight.x;
        this.bodyParts.footRight.y = this.bodyPartOriginalCoordinates.footRight.y;
        this.bodyParts.eyes.x = this.bodyPartOriginalCoordinates.eyes.x;
        this.bodyParts.eyes.y = this.bodyPartOriginalCoordinates.eyes.y;
    };
    Bunny.prototype.getSprite = function () {
        return this.group;
    };
    Bunny.prototype.destroy = function () {
        this.group.destroy(true);
    };
    Bunny.prototype.standAnimation = function (makeSound) {
        if (makeSound === void 0) { makeSound = false; }
        if (makeSound) {
            var randomSound = this.game.rnd.integerInRange(1, 5);
            this.game.add.sound(Assets_1.default[("AUDIO_KEY_AUDIO_BUNNY_SOUND_0" + randomSound)], 1, false)
                .play();
        }
        this.blinkAnimation();
        this.mouthAnimation();
        this.wiggleEars();
    };
    Bunny.prototype.dropDown = function (onComplete, dropDistance) {
        var _this = this;
        if (dropDistance === void 0) { dropDistance = 100; }
        if (!onComplete) {
            onComplete = this.standAnimation.bind(this, true);
        }
        var timer = this.game.time.create();
        timer.add(Phaser.Timer.SECOND / 4, function () {
            _this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_TILE_LAND, 1, false)
                .play();
        }, this);
        timer.start();
        var originalY = this.group.y;
        var startingY = originalY - dropDistance;
        this.group.y = startingY;
        var dropDownTween = this.game.add.tween(this.group)
            .to({ y: originalY }, 1000, Phaser.Easing.Bounce.Out, false);
        dropDownTween.onComplete.add(onComplete);
        this.eyes.shutEyes();
        this.mouth.close();
        this.body.fallDown();
        this.leftPaw.airToSide();
        this.rightPaw.airToSide();
        this.leftFoot.land();
        this.rightFoot.land();
        dropDownTween.start();
    };
    Bunny.prototype.jumpFade = function (onComplete) {
        var originalY = this.group.y;
        var finishY = originalY - 50;
        this.group.y = originalY;
        var hopUpTween = this.game.add.tween(this.group)
            .to({ y: finishY, alpha: 0 }, 500, Phaser.Easing.Circular.InOut, false);
        this.eyes.shutEyes();
        this.mouth.close();
        hopUpTween.onComplete.add(onComplete);
        hopUpTween.start();
    };
    Bunny.prototype.scurry = function () {
        this.leftFoot.move();
        this.rightFoot.move();
    };
    Bunny.prototype.blinkAnimation = function () {
        this.eyes.blink();
    };
    Bunny.prototype.mouthAnimation = function () {
        this.mouth.move();
    };
    Bunny.prototype.wiggleEars = function () {
        this.earLeft.wiggle();
        this.earRight.wiggle();
    };
    Bunny.prototype.shootToScreen = function (onHit, onComplete) {
        var _this = this;
        this.eyes.spinEyes();
        this.mouth.worry();
        this.body.stretch();
        this.leftPaw.flap();
        this.rightPaw.flap();
        this.leftFoot.flap();
        this.rightFoot.flap();
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_TILE_SPRING, 1, false)
            .play();
        var randomSound = this.game.rnd.integerInRange(1, 5);
        this.game.add.sound(Assets_1.default[("AUDIO_KEY_AUDIO_BUNNY_FLYING_0" + randomSound)], 1, false)
            .play();
        var shootAway = this.game.add.tween(this.group.scale)
            .to({ x: 2, y: 2 }, 2000, Phaser.Easing.Circular.In, false);
        this.angleTween = null;
        var spinCount = ArrayUtils.getRandomItem([0, 1, 2]);
        if (spinCount > 0) {
            var durationPerSpin = (Math.floor(2000 / spinCount));
            this.angleTween = this.game.add.tween(this.group)
                .to({ angle: 360 }, durationPerSpin, Phaser.Easing.Linear.None, true, 0, spinCount);
        }
        var showingGlass = Phaser.Utils.chanceRoll(50);
        var glassCrack = null;
        if (showingGlass) {
            var randomGlass = this.game.rnd.integerInRange(1, 3);
            glassCrack = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default[("GRAPHIC_GAME_GLASS_" + randomGlass)]);
            glassCrack.anchor.set(0.5, 0.5);
            glassCrack.alpha = 0;
            glassCrack.angle = this.game.rnd.integerInRange(0, 359);
        }
        shootAway.onComplete.add(function () {
            if (_this.angleTween) {
                _this.game.tweens.remove(_this.angleTween);
            }
            _this.game.camera['shake'](0.05, 200);
            _this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_HITS_SCREEN, 1, false)
                .play();
            if (glassCrack !== null) {
                glassCrack.position.set(_this.group.position.x, _this.group.position.y);
                glassCrack.alpha = 1;
                _this.game.add.tween(glassCrack)
                    .to({ alpha: 0 }, 2000, null, true, 2000)
                    .onComplete.add(function () {
                    glassCrack.destroy();
                });
            }
            _this.eyes.hitEyes();
            _this.mouth.hit();
            _this.leftPaw.stopFlapping();
            _this.rightPaw.stopFlapping();
            _this.leftFoot.stopFlapping();
            _this.rightFoot.stopFlapping();
            _this.slideDownScreen(onComplete);
        });
        shootAway.onComplete.add(onHit);
        shootAway.start();
    };
    Bunny.prototype.slideDownScreen = function (onComplete) {
        var _this = this;
        var timer = this.game.time.create();
        timer.add(Phaser.Timer.SECOND / 2, function () {
            _this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUNNY_SLIDE, 1, false)
                .play();
        }, this);
        timer.start();
        var tween = this.game.add.tween(this.group)
            .to({ y: (this.game.world.height + this.group.height) }, 2000, Phaser.Easing.Exponential.InOut, false, 200);
        tween.onComplete.add(onComplete);
        tween.start();
    };
    return Bunny;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Bunny;
