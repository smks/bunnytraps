"use strict";
var Assets_1 = require("../../../../Assets");
var ArrayUtils = Phaser.ArrayUtils;
var Mouth = (function () {
    function Mouth(game, group) {
        this.game = game;
        this.group = group;
    }
    Mouth.prototype.create = function (xPos, yPos, bunnyId) {
        if (bunnyId === void 0) { bunnyId = 1; }
        this.bunnyId = bunnyId;
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_OPEN, this.group);
        this.sprite.anchor.set(0.5, 0.5);
        this.addAnimations();
        return this.sprite;
    };
    Mouth.prototype.move = function () {
        this.sprite.animations.play('move');
    };
    Mouth.prototype.panic = function () {
        this.sprite.animations.play('panic');
    };
    Mouth.prototype.worry = function () {
        this.sprite.animations.play('worry');
    };
    Mouth.prototype.hit = function () {
        this.sprite.animations.play('hit');
    };
    Mouth.prototype.addAnimations = function () {
        this.addHappyMouth();
        this.addWorryMouth();
        this.addPanicMouth();
        this.addHitMouth();
    };
    Mouth.prototype.addHappyMouth = function () {
        var frames = Array.apply(null, Array(36));
        frames.forEach(function (item, i) {
            if (i >= (frames.length / 2)) {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_OPEN;
            }
            else {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_CLOSED;
            }
        });
        ArrayUtils.shuffle(frames);
        this.sprite.animations.add('move', frames, 1, false);
    };
    Mouth.prototype.addWorryMouth = function () {
        var frames = Array.apply(null, Array(12));
        frames.forEach(function (item, i) {
            if (i <= 2) {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_1;
            }
            else {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_FEAR_2;
            }
        });
        this.sprite.animations.add('worry', frames, 12, false);
    };
    Mouth.prototype.addPanicMouth = function () {
        var frames = Array.apply(null, Array(12));
        frames.forEach(function (item, i) {
            if (i == 0) {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC;
            }
            else {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_PANIC_CLOSED;
            }
        });
        this.sprite.animations.add('panic', frames, 12, true);
    };
    Mouth.prototype.addHitMouth = function () {
        var frames = Array.apply(null, Array(10));
        frames.forEach(function (item, i) {
            if (i == 0) {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_1;
            }
            else {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_HIT_2;
            }
        });
        this.sprite.animations.add('hit', frames, 12, false);
    };
    Mouth.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    Mouth.prototype.close = function () {
        this.sprite.frameName = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_MOUTH_CLOSED;
    };
    return Mouth;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Mouth;
