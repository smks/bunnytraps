"use strict";
var Assets_1 = require("../../../../Assets");
var Body = (function () {
    function Body(game, group) {
        this.game = game;
        this.group = group;
    }
    Body.prototype.create = function (xPos, yPos, bunnyId) {
        if (bunnyId === void 0) { bunnyId = 1; }
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default[("GRAPHIC_BUNNIES_BUNNY_" + bunnyId + "_BODY")], this.group);
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    };
    Body.prototype.fallDown = function () {
        this.sprite.scale.x = 0.8;
        this.sprite.scale.y = 1.2;
        var tween = this.game.add.tween(this.sprite.scale)
            .to({ x: 1, y: 1 }, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    };
    Body.prototype.stretch = function () {
        var tween = this.game.add.tween(this.sprite.scale)
            .to({ x: 1.1, y: 1 }, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    };
    Body.prototype.getCenterPosition = function () {
        return this.sprite.position;
    };
    Body.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    return Body;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Body;
