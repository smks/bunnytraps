"use strict";
var Assets_1 = require("../../../../Assets");
var Paw = (function () {
    function Paw(game, group) {
        this.TYPE_LEFT = 1;
        this.TYPE_RIGHT = 2;
        this.game = game;
        this.group = group;
    }
    Paw.prototype.create = function (xPos, yPos, bunnyId, type) {
        if (bunnyId === void 0) { bunnyId = 1; }
        if (type === void 0) { type = this.TYPE_LEFT; }
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default[("GRAPHIC_BUNNIES_BUNNY_" + bunnyId + "_PAW")], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    };
    Paw.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    Paw.prototype.flap = function () {
        var originalY = this.sprite.y;
        var startingY = originalY - 100;
        var endY = originalY + 100;
        this.sprite.y = startingY;
        var startingAngle = (this.type == this.TYPE_RIGHT) ? -90 : 90;
        this.currentTween = this.game.add.tween(this.sprite)
            .to({ y: endY, angle: startingAngle }, 100, Phaser.Easing.Linear.None, true, 0, 20, true);
    };
    Paw.prototype.airToSide = function () {
        var originalY = this.sprite.y;
        var originalAngle = 0;
        var startingY = originalY - 400;
        var startingAngle = (this.type == this.TYPE_RIGHT) ? -90 : 90;
        this.sprite.y = startingY;
        this.sprite.angle = startingAngle;
        var tween = this.game.add.tween(this.sprite)
            .to({ y: originalY, angle: originalAngle }, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    };
    Paw.prototype.stopFlapping = function () {
        this.game.tweens.remove(this.currentTween);
    };
    return Paw;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Paw;
