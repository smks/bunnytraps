"use strict";
var Assets_1 = require("../../../../Assets");
var Ear = (function () {
    function Ear(game, group) {
        this.TYPE_LEFT = 1;
        this.TYPE_RIGHT = 2;
        this.game = game;
        this.group = group;
    }
    Ear.prototype.create = function (xPos, yPos, bunnyId, type, wiggleDelay) {
        if (bunnyId === void 0) { bunnyId = 1; }
        if (type === void 0) { type = this.TYPE_LEFT; }
        if (wiggleDelay === void 0) { wiggleDelay = 4000; }
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default[("GRAPHIC_BUNNIES_BUNNY_" + bunnyId + "_EAR")], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 1);
        this.addAnimations(wiggleDelay);
        return this.sprite;
    };
    Ear.prototype.wiggle = function () {
        this.tween.start();
    };
    Ear.prototype.stopWiggle = function () {
        this.tween.stop();
    };
    Ear.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    Ear.prototype.addAnimations = function (wiggleDelay) {
        this.addWiggle(wiggleDelay);
    };
    Ear.prototype.addWiggle = function (wiggleDelay) {
        var _this = this;
        var angle = (this.type == this.TYPE_RIGHT) ? -8 : 8;
        this.tween = this.game.add
            .tween(this.sprite)
            .to({ angle: angle }, 100, Phaser.Easing.Elastic.InOut, false, wiggleDelay);
        this.tween.yoyo(true);
        this.tween.onComplete.add(function () {
            _this.tween.start(0);
        });
    };
    return Ear;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Ear;
