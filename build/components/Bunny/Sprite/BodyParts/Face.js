"use strict";
var Assets_1 = require("../../../../Assets");
var Paw = (function () {
    function Paw(game, group) {
        this.TYPE_LEFT = 1;
        this.TYPE_RIGHT = 1;
        this.game = game;
        this.group = group;
    }
    Paw.prototype.create = function (xPos, yPos, bunnyId, type) {
        if (bunnyId === void 0) { bunnyId = 1; }
        if (type === void 0) { type = this.TYPE_LEFT; }
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default[("GRAPHIC_BUNNIES_BUNNY_" + bunnyId + "_FACE")], this.group);
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        return this.sprite;
    };
    Paw.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    return Paw;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Paw;
