"use strict";
var Assets_1 = require("../../../../Assets");
var ArrayUtils = Phaser.ArrayUtils;
var Eyes = (function () {
    function Eyes(game, group) {
        this.game = game;
        this.group = group;
    }
    Eyes.prototype.create = function (xPos, yPos, bunnyId) {
        if (bunnyId === void 0) { bunnyId = 1; }
        this.eyeGroup = this.game.add.group(this.group);
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_OPEN, this.eyeGroup);
        this.leftRollEye = this.game.add.sprite(xPos - 110, yPos + 20, Assets_1.default.BUNNIES_ASSETS, Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_ROLL, this.eyeGroup);
        this.rightRollEye = this.game.add.sprite(xPos + 110, yPos + 20, Assets_1.default.BUNNIES_ASSETS, Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_ROLL, this.eyeGroup);
        this.rightRollEye.scale.set(-1, 1);
        this.leftRollEye.anchor.set(0.5, 0.5);
        this.rightRollEye.anchor.set(0.5, 0.5);
        this.sprite.anchor.set(0.5, 0.5);
        this.addAnimations();
        this.leftRollEye.visible = false;
        this.rightRollEye.visible = false;
        return this.eyeGroup;
    };
    Eyes.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    Eyes.prototype.blink = function () {
        this.sprite.animations.play('blink');
    };
    Eyes.prototype.shutEyes = function () {
        this.sprite.animations.stop('blink');
        this.sprite.frameName = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_CLOSED;
    };
    Eyes.prototype.spinEyes = function () {
        this.sprite.animations.stop('blink');
        this.sprite.visible = false;
        this.leftRollEye.visible = true;
        this.rightRollEye.visible = true;
        this.game.add.tween(this.leftRollEye)
            .to({ angle: -360 }, 400, Phaser.Easing.Linear.None, true, 0, 4);
        this.game.add.tween(this.rightRollEye)
            .to({ angle: 360 }, 400, Phaser.Easing.Linear.None, true, 0, 4);
    };
    Eyes.prototype.sadEyes = function () {
        this.sprite.frameName = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_SAD;
    };
    Eyes.prototype.worryEyes = function () {
        this.sprite.frameName = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_WORRY;
    };
    Eyes.prototype.hitEyes = function () {
        this.leftRollEye.visible = false;
        this.rightRollEye.visible = false;
        this.sprite.visible = true;
        this.sprite.frameName = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_HIT;
    };
    Eyes.prototype.addAnimations = function () {
        this.addBlinkAnimation();
        this.addHitAnimation();
    };
    Eyes.prototype.addBlinkAnimation = function () {
        var frames = Array.apply(null, Array(36));
        frames.forEach(function (item, i) {
            if (i >= (frames.length - 2)) {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_CLOSED;
            }
            else {
                frames[i] = Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_OPEN;
            }
        });
        ArrayUtils.shuffle(frames);
        this.sprite.animations.add('blink', frames, 12, true);
    };
    Eyes.prototype.addHitAnimation = function () {
        this.sprite.animations.add('hit', [Assets_1.default.GRAPHIC_BUNNIES_BUNNY_EYES_HIT], 1, true);
    };
    return Eyes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Eyes;
