"use strict";
var Assets_1 = require("../../../../Assets");
var Foot = (function () {
    function Foot(game, group) {
        this.TYPE_LEFT = 1;
        this.TYPE_RIGHT = 2;
        this.game = game;
        this.group = group;
    }
    Foot.prototype.create = function (xPos, yPos, bunnyId, type) {
        if (bunnyId === void 0) { bunnyId = 1; }
        if (type === void 0) { type = this.TYPE_LEFT; }
        this.sprite = this.game.add.sprite(xPos, yPos, Assets_1.default.BUNNIES_ASSETS, Assets_1.default[("GRAPHIC_BUNNIES_BUNNY_" + bunnyId + "_FOOT")], this.group);
        this.type = type;
        if (type == this.TYPE_RIGHT) {
            this.sprite.scale.x = -1;
        }
        this.sprite.anchor.set(0.5, 0.5);
        this.originalX = this.sprite.x;
        this.originalY = this.sprite.y;
        return this.sprite;
    };
    Foot.prototype.destroy = function () {
        this.sprite.destroy(true);
    };
    Foot.prototype.land = function () {
        var originalX = this.sprite.x;
        var originalY = this.sprite.y;
        var originalAngle = 0;
        var startingY = originalY + 150;
        var startingX = originalX + ((this.type == this.TYPE_RIGHT) ? -80 : 80);
        var startingAngle = (this.type == this.TYPE_RIGHT) ? -20 : 20;
        this.sprite.x = startingX;
        this.sprite.y = startingY;
        this.sprite.angle = startingAngle;
        var tween = this.game.add.tween(this.sprite)
            .to({ x: originalX, y: originalY, angle: originalAngle }, 1000, Phaser.Easing.Bounce.Out, false);
        tween.start();
    };
    Foot.prototype.move = function () {
        var topY = this.originalY - 30;
        var bottomY = this.originalY + 30;
        var fromYPos;
        var toYPos;
        if (this.type == this.TYPE_LEFT) {
            this.sprite.y = topY;
            fromYPos = topY;
            toYPos = bottomY;
        }
        else {
            this.sprite.y = bottomY;
            fromYPos = bottomY;
            toYPos = topY;
        }
        var tween = this.game.add.tween(this.sprite)
            .to({ y: toYPos }, 200, Phaser.Easing.Quadratic.In)
            .to({ y: fromYPos }, 200, Phaser.Easing.Quadratic.Out)
            .to({ y: this.originalY }, 200, Phaser.Easing.Quadratic.Out);
        tween.start();
    };
    Foot.prototype.flap = function () {
        var DISTANCE = 35;
        var originalY = this.sprite.y;
        var startingY;
        var endY;
        if (this.type == this.TYPE_LEFT) {
            startingY = originalY - DISTANCE;
            endY = originalY + DISTANCE;
        }
        else {
            startingY = originalY + DISTANCE;
            endY = originalY - DISTANCE;
        }
        this.sprite.y = startingY;
        this.currentTween = this.game.add.tween(this.sprite)
            .to({ y: endY }, 100, Phaser.Easing.Linear.None, true, 0, 20, true);
    };
    Foot.prototype.stopFlapping = function () {
        this.game.tweens.remove(this.currentTween);
    };
    return Foot;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Foot;
