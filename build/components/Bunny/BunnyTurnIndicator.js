"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require('./../../helpers/textfield-factory');
var textfield_factory_2 = require('./../../helpers/textfield-factory');
var Bunny_1 = require("./Sprite/Bunny");
var BunnyTurnIndicator = (function () {
    function BunnyTurnIndicator(game, bunnyId) {
        if (bunnyId === void 0) { bunnyId = 1; }
        this.DISTANCE = 30;
        this.DEFAULT_BUNNIES = 25;
        this.SPEED = 400;
        this.isForward = false;
        this.game = game;
        this.bunnyId = bunnyId;
    }
    BunnyTurnIndicator.prototype.create = function (xPos, yPos, bunnyCount) {
        if (bunnyCount === void 0) { bunnyCount = 25; }
        this.group = this.game.add.group();
        this.bunnyController = new Bunny_1.default(this.game, this.bunnyId);
        this.bunnyController.create(xPos, yPos, 0.2);
        this.bunnyController.standAnimation();
        this.bunny = this.bunnyController.getSprite();
        this.originalYPosition = yPos;
        this.card = this.game.add.sprite(xPos, yPos, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUNNY_CARD);
        this.card.anchor.set(0.5, 0.5);
        this.card.y = this.bunny.height + 190;
        this.group.add(this.bunny);
        this.group.add(this.card);
        var options = textfield_factory_2.getDefaultTextFieldOptions(bunnyCount.toString());
        options.fontSize = 40;
        options.parent = this.group;
        this.cardText = textfield_factory_1.default(this.game, options);
        this.cardText.x = this.card.x;
        this.cardText.y = this.card.y - 10;
        this.card.scale.y = 0;
        this.cardText.scale.y = 0;
    };
    BunnyTurnIndicator.prototype.reduceCount = function () {
        var newCount = parseInt(this.cardText.text, 10) - 1;
        if (newCount < 0) {
            newCount = 0;
        }
        this.cardText.text = newCount.toString();
    };
    BunnyTurnIndicator.prototype.moveForward = function (numberOfBunnies) {
        var _this = this;
        if (numberOfBunnies === void 0) { numberOfBunnies = 25; }
        if (this.isForward) {
            return;
        }
        var revealCard = this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_REVEAL_CARD, 1, false)
            .play();
        revealCard.onStop.addOnce(function () {
            _this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_CHANGE_PLAYER, 1, false)
                .play();
        });
        this.isForward = true;
        if (this.bunnyCount == null) {
            this.bunnyCount = numberOfBunnies;
            this.cardText.text = numberOfBunnies.toString();
        }
        this.bunnyController.scurry();
        this.game.add.tween(this.bunny)
            .to({ y: (this.bunny.y + this.DISTANCE) }, this.SPEED, Phaser.Easing.Circular.InOut, true);
        this.game.add.tween(this.card.scale)
            .to({ y: 1 }, this.SPEED, Phaser.Easing.Circular.InOut, true);
        this.game.add.tween(this.cardText.scale)
            .to({ y: 1 }, this.SPEED, Phaser.Easing.Circular.InOut, true);
    };
    BunnyTurnIndicator.prototype.moveBackward = function () {
        if (!this.isForward) {
            return;
        }
        this.isForward = false;
        this.bunnyController.scurry();
        this.game.add.tween(this.bunny)
            .to({ y: this.originalYPosition }, this.SPEED, Phaser.Easing.Circular.InOut, true);
        this.game.add.tween(this.card.scale)
            .to({ y: 0 }, this.SPEED, Phaser.Easing.Circular.InOut, true);
        this.game.add.tween(this.cardText.scale)
            .to({ y: 0 }, this.SPEED, Phaser.Easing.Circular.InOut, true);
    };
    BunnyTurnIndicator.prototype.destroy = function () {
        this.bunny.destroy(true);
    };
    return BunnyTurnIndicator;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BunnyTurnIndicator;
