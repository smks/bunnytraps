"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require("../../helpers/textfield-factory");
var textfield_factory_2 = require('./../../helpers/textfield-factory');
var PointsIndicator = (function () {
    function PointsIndicator(game) {
        this.game = game;
    }
    PointsIndicator.prototype.create = function (xPos, yPos, message) {
        if (message === void 0) { message = ''; }
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.sprite = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_POINTS_INDICATOR, this.group);
        this.sprite.anchor.set(0.5, 0.5);
        var options = textfield_factory_1.getDefaultTextFieldOptions(message);
        options.fontSize = 44;
        options.parent = this.group;
        this.textField = textfield_factory_2.default(this.game, options);
        this.textField.x = this.sprite.x;
        this.textField.y = this.sprite.y - 10;
        this.textField.anchor.set(0.5, 0.5);
    };
    PointsIndicator.prototype.setMessage = function (message) {
        this.textField.text = message;
    };
    PointsIndicator.prototype.destroy = function () {
        this.group.destroy(true);
    };
    return PointsIndicator;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PointsIndicator;
