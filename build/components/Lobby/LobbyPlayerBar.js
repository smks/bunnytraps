"use strict";
var textfield_factory_1 = require("../../helpers/textfield-factory");
var textfield_factory_2 = require('../../helpers/textfield-factory');
var username_shortener_1 = require('../../helpers/username-shortener');
var LobbyPlayerBar = (function () {
    function LobbyPlayerBar(game, username) {
        this.game = game;
        this.username = username_shortener_1.default(username);
    }
    LobbyPlayerBar.prototype.create = function (xPos, yPos) {
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        var options = textfield_factory_1.getDefaultTextFieldOptions(this.username);
        options.fontSize = 60;
        options.parent = this.group;
        this.textField = textfield_factory_2.default(this.game, options);
        this.textField.x = 0;
        this.textField.y = -(20);
        this.textField.anchor.set(0.5, 0.5);
    };
    LobbyPlayerBar.prototype.setText = function (text) {
        this.textField.text = text;
    };
    LobbyPlayerBar.prototype.update = function () {
    };
    LobbyPlayerBar.prototype.destroy = function () {
        this.group.destroy(true);
    };
    return LobbyPlayerBar;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LobbyPlayerBar;
