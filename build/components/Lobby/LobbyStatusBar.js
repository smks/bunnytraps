"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require("../../helpers/textfield-factory");
var textfield_factory_2 = require('../../helpers/textfield-factory');
var LobbyStatusBar = (function () {
    function LobbyStatusBar(game) {
        this.game = game;
    }
    LobbyStatusBar.prototype.create = function (xPos, yPos) {
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.sprite = this.game.add.sprite(0, 0, Assets_1.default.LOBBY_ASSETS, Assets_1.default.GRAPHIC_LOBBY_LOBBY_STATUS_BAR, this.group);
        this.sprite.anchor.set(0.5, 0.5);
        var options = textfield_factory_1.getDefaultTextFieldOptions('Waiting for 2 Players');
        options.fontSize = 44;
        options.parent = this.group;
        options.font = Assets_1.default.FONT_INVERSE;
        this.textField = textfield_factory_2.default(this.game, options);
        this.textField.x = this.sprite.x;
        this.textField.y = this.sprite.y + 6;
        this.textField.anchor.set(0.5, 0.5);
    };
    LobbyStatusBar.prototype.setText = function (text) {
        this.textField.text = text;
    };
    LobbyStatusBar.prototype.update = function () {
    };
    LobbyStatusBar.prototype.destroy = function () {
        this.group.destroy(true);
    };
    return LobbyStatusBar;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LobbyStatusBar;
