"use strict";
var BushSectionFactory_1 = require("./BushSectionFactory");
var Assets_1 = require("../../Assets");
var Bush = (function () {
    function Bush(game) {
        this.game = game;
    }
    Bush.prototype.create = function (xPos, yPos) {
        if (xPos === void 0) { xPos = 0; }
        if (yPos === void 0) { yPos = 0; }
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        var bushSectionFactory = new BushSectionFactory_1.default(this.game);
        this.leftBush = bushSectionFactory.create(0, 0);
        var mirror = true;
        this.rightBush = bushSectionFactory.create(0, 0, mirror);
        this.rightBush.x = this.game.world.width;
        this.rightBush.y = this.game.world.height;
        this.group.add(this.leftBush);
        this.group.add(this.rightBush);
        this.originalLeftX = this.leftBush.x;
        this.originalLeftY = this.leftBush.y;
        this.originalRightX = this.rightBush.x;
        this.originalRightY = this.rightBush.y;
    };
    Bush.prototype.open = function (onComplete) {
        this.leftBush.x = this.originalLeftX;
        this.rightBush.x = this.originalRightX;
        var leftXPos = (this.originalLeftX - this.leftBush.width);
        var rightXPos = (this.originalRightX + -(this.rightBush.width));
        var onOpened = function () {
            onComplete();
        };
        this.tween(leftXPos, rightXPos, onOpened);
    };
    Bush.prototype.close = function (onComplete) {
        var _this = this;
        this.leftBush.x = (this.originalLeftX - this.leftBush.width);
        this.rightBush.x = (this.originalRightX + -(this.rightBush.width));
        var leftXPos = this.originalLeftX;
        var rightXPos = this.originalRightX;
        var onClose = function () {
            _this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUSH_COLLIDE, 1, false)
                .play();
            onComplete();
        };
        this.tween(leftXPos, rightXPos, onClose);
    };
    Bush.prototype.tween = function (leftX, rightX, onComplete) {
        var leftTween = this.game.add
            .tween(this.leftBush)
            .to({ x: leftX }, 600, Phaser.Easing.Quartic.InOut, false);
        var rightTween = this.game.add
            .tween(this.rightBush)
            .to({ x: rightX }, 600, Phaser.Easing.Quartic.InOut, false);
        leftTween.onComplete.add(onComplete);
        leftTween.start();
        rightTween.start();
    };
    Bush.prototype.getSprite = function () {
        return this.group;
    };
    Bush.prototype.destroy = function () {
        this.group.destroy(true);
    };
    return Bush;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Bush;
