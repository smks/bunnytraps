"use strict";
var Assets_1 = require("../../Assets");
var BushSectionFactory = (function () {
    function BushSectionFactory(game) {
        this.game = game;
    }
    BushSectionFactory.prototype.create = function (xPos, yPos, mirror) {
        if (mirror === void 0) { mirror = false; }
        var group = this.game.add.group();
        group.x = xPos;
        group.y = yPos;
        this.portion1 = this.game.add.sprite(-309, -131, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion2 = this.game.add.sprite(-218, 244, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion3 = this.game.add.sprite(-126, 666, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion4 = this.game.add.sprite(-35, -131, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion5 = this.game.add.sprite(57, 244, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion6 = this.game.add.sprite(148, 666, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion7 = this.game.add.sprite(239, -131, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion8 = this.game.add.sprite(331, 244, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        this.portion9 = this.game.add.sprite(422, 666, Assets_1.default.BUSH_ASSETS, Assets_1.default.GRAPHIC_BUSH, group);
        if (mirror) {
            group.scale.set(-1, -1);
        }
        return group;
    };
    return BushSectionFactory;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BushSectionFactory;
