"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require('./../../helpers/textfield-factory');
var textfield_factory_2 = require('./../../helpers/textfield-factory');
var InstructionsBanner = (function () {
    function InstructionsBanner(game, xPos, yPos) {
        this.SPEED = 400;
        this.messages = new Array();
        this.game = game;
        this.xPos = xPos;
        this.yPos = yPos;
    }
    InstructionsBanner.prototype.create = function (instructionMessage) {
        this.group = this.game.add.group();
        this.group.x = this.xPos;
        this.group.y = this.yPos;
        this.sprite = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_INSTRUCTIONS_BANNER);
        this.sprite.alpha = 0;
        this.group.add(this.sprite);
        var options = textfield_factory_2.getDefaultTextFieldOptions(instructionMessage);
        options.fontSize = 40;
        options.parent = this.group;
        this.textField = textfield_factory_1.default(this.game, options);
        this.textField.x = this.sprite.x + this.sprite.width / 2;
        this.textField.y = (this.sprite.y + this.sprite.height / 2) - 12;
        this.textField.anchor.set(0.5, 0.5);
        this.group.add(this.textField);
    };
    InstructionsBanner.prototype.changeMessage = function (newInstructionsMessage) {
        var _this = this;
        var changeTween = this.game.add.tween(this.textField.scale)
            .to({ y: 0 }, this.SPEED, Phaser.Easing.Circular.InOut, false);
        changeTween.onComplete.add(function () {
            _this.textField.text = newInstructionsMessage;
            _this.game.add.tween(_this.textField.scale)
                .to({ y: 1 }, _this.SPEED, Phaser.Easing.Circular.InOut, true);
        });
        changeTween.start();
    };
    InstructionsBanner.prototype.destroy = function () {
        this.group.destroy(true);
        this.sprite.destroy(true);
        this.textField.destroy(true);
    };
    InstructionsBanner.prototype.addMessages = function (messages) {
        this.messages = messages;
    };
    InstructionsBanner.prototype.changeRandomMessage = function () {
        var randomMessage = Phaser.ArrayUtils.getRandomItem(this.messages);
        this.changeMessage(randomMessage);
    };
    return InstructionsBanner;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = InstructionsBanner;
