"use strict";
var textfield_factory_1 = require('../../helpers/textfield-factory');
var textfield_factory_2 = require("../../helpers/textfield-factory");
var PercentageLoader = (function () {
    function PercentageLoader(game) {
        this.game = game;
    }
    PercentageLoader.prototype.create = function (xPos, yPos) {
        var options = this.getTextFieldOptions();
        options.xPos = xPos;
        options.yPos = yPos;
        options.fontSize = 70;
        options.parent = this.game.world;
        this.percentageTextField = textfield_factory_1.default(this.game, options);
    };
    PercentageLoader.prototype.getTextFieldOptions = function () {
        var options = textfield_factory_2.getDefaultTextFieldOptions('0%');
        return options;
    };
    PercentageLoader.prototype.setPercentage = function (percentage) {
        this.percentage = percentage;
        this.percentageTextField.text = percentage.toString() + '%';
        console.log('setting percentage: ' + percentage);
    };
    PercentageLoader.prototype.destroy = function () {
        this.percentageTextField.destroy(true);
    };
    return PercentageLoader;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PercentageLoader;
