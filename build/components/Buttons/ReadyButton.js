"use strict";
var Assets_1 = require("../../Assets");
var ReadyButton = (function () {
    function ReadyButton(game, playerNumber, inputDown) {
        if (playerNumber === void 0) { playerNumber = 1; }
        this.game = game;
        this.playerNumber = playerNumber;
        this.inputDown = inputDown;
    }
    ReadyButton.prototype.create = function (xPos, yPos, inputDown) {
        if (xPos === void 0) { xPos = 0; }
        if (yPos === void 0) { yPos = 0; }
        this.button = this.game.add.sprite(xPos, yPos, Assets_1.default.LOBBY_ASSETS, Assets_1.default.GRAPHIC_LOBBY_READY_BUTTON.replace(/\{id\}/, this.playerNumber.toString()));
        this.button.anchor.set(0.5, 0.5);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
    };
    ReadyButton.prototype.addOnPress = function (inputDown) {
        this.inputDown = inputDown;
    };
    ReadyButton.prototype.onInputOut = function (e) {
        e.scale.set(1, 1);
    };
    ReadyButton.prototype.onInputOver = function (e) {
        e.scale.set(1.05, 1.05);
    };
    ReadyButton.prototype.onInputDown = function (e) {
        var button = e;
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        if (this.inputDown) {
            this.inputDown();
        }
    };
    ReadyButton.prototype.movePositionByPlayerNumber = function (playerNumber) {
        switch (playerNumber) {
            case 1:
                this.button.x = 570.5;
                break;
            case 2:
                this.button.x = 830.5;
                break;
            case 3:
                this.button.x = 1090.5;
                break;
            case 4:
                this.button.x = 1350.5;
                break;
        }
        this.button.y = 410;
    };
    ReadyButton.prototype.destroy = function () {
        this.button.destroy(true);
    };
    ReadyButton.prototype.setEnabled = function (isEnabled) {
        this.button.inputEnabled = isEnabled;
    };
    ReadyButton.prototype.setDisabled = function () {
        this.button.alpha = 0.2;
    };
    ReadyButton.prototype.setPressedState = function () {
        this.button.frameName = Assets_1.default.GRAPHIC_LOBBY_READY_BUTTON_ACTIVE.replace(/\{id\}/, this.playerNumber.toString());
    };
    ReadyButton.prototype.setInactive = function () {
        this.button.frameName = Assets_1.default.GRAPHIC_LOBBY_READY_BUTTON.replace(/\{id\}/, this.playerNumber.toString());
    };
    return ReadyButton;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ReadyButton;
