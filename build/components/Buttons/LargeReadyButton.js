"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require('./../../helpers/textfield-factory');
var textfield_factory_2 = require('./../../helpers/textfield-factory');
var LargeReadyButton = (function () {
    function LargeReadyButton(game, inputDown) {
        this.game = game;
        this.inputDown = inputDown;
    }
    LargeReadyButton.prototype.setOnClick = function (inputDown) {
        this.inputDown = inputDown;
    };
    LargeReadyButton.prototype.create = function (xPos, yPos) {
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.button = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUTTONS_LARGE, this.group);
        this.button.anchor.set(0.5, 1);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
        var options = textfield_factory_2.getDefaultTextFieldOptions('READY');
        options.fontSize = 60;
        options.font = Assets_1.default.FONT_BUTTONS;
        options.parent = this.group;
        this.textField = textfield_factory_1.default(this.game, options);
        this.textField.x = this.button.x;
        this.textField.y = (this.button.y - (this.button.height / 2)) - 10;
        this.textField.anchor.set(0.5, 0.5);
        this.group.add(this.textField);
    };
    LargeReadyButton.prototype.onInputOut = function (button) {
        button.scale.set(1, 1);
        this.textField.scale.set(1, 1);
    };
    LargeReadyButton.prototype.onInputOver = function (button) {
        button.scale.set(1, 1.05);
        this.textField.scale.set(1, 1.05);
    };
    LargeReadyButton.prototype.onInputDown = function (e) {
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        this.inputDown();
    };
    LargeReadyButton.prototype.destroy = function () {
        this.button.destroy(true);
    };
    LargeReadyButton.prototype.setEnabled = function (isEnabled) {
        this.button.inputEnabled = isEnabled;
        if (!isEnabled) {
            this.button.alpha = 0.5;
            this.textField.alpha = 0.5;
        }
        else {
            this.button.alpha = 1;
            this.textField.alpha = 1;
        }
    };
    return LargeReadyButton;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LargeReadyButton;
