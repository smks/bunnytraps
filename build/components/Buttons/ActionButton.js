"use strict";
var Assets_1 = require("../../Assets");
var ActionButton = (function () {
    function ActionButton(game, inputDown) {
        this.game = game;
        this.inputDown = inputDown;
    }
    ActionButton.prototype.create = function (xPos, yPos) {
        this.button = this.game.add.sprite(xPos, yPos, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUTTONS_SMALL);
        this.button.anchor.set(0.5, 0.5);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
    };
    ActionButton.prototype.onInputOut = function (e) {
        e.scale.set(1, 1);
    };
    ActionButton.prototype.onInputOver = function (e) {
        e.scale.set(1.05, 1.05);
    };
    ActionButton.prototype.onInputDown = function (e) {
        var button = e;
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        this.inputDown();
    };
    ActionButton.prototype.destroy = function () {
        this.button.destroy(true);
    };
    return ActionButton;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ActionButton;
