"use strict";
var Assets_1 = require("../../Assets");
var LargeReadOnlyButton = (function () {
    function LargeReadOnlyButton(game) {
        this.game = game;
    }
    LargeReadOnlyButton.prototype.create = function (xPos, yPos) {
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.button = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUTTONS_RESULTS, this.group);
        this.button.anchor.set(0.5, 1);
        this.button.inputEnabled = false;
    };
    LargeReadOnlyButton.prototype.destroy = function () {
        this.button.destroy(true);
    };
    return LargeReadOnlyButton;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LargeReadOnlyButton;
