"use strict";
var Assets_1 = require("../../Assets");
var SoundButton = (function () {
    function SoundButton(game, inputDown) {
        this.game = game;
        this.inputDown = inputDown;
    }
    SoundButton.prototype.create = function (xPos, yPos) {
        this.button = this.game.add.sprite(xPos, yPos, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_BUTTONS_SMALL_SOUND);
        this.button.anchor.set(0.5, 0.5);
        this.button.inputEnabled = true;
        this.button.events.onInputOver.add(this.onInputOver, this);
        this.button.events.onInputOut.add(this.onInputOut, this);
        this.button.events.onInputDown.add(this.onInputDown, this);
    };
    SoundButton.prototype.onInputOut = function (e) {
        e.scale.set(1, 1);
    };
    SoundButton.prototype.onInputOver = function (e) {
        e.scale.set(1.05, 1.05);
    };
    SoundButton.prototype.onInputDown = function (e) {
        var button = e;
        this.game.add.sound(Assets_1.default.AUDIO_KEY_AUDIO_BUTTON_PRESS, 1, false)
            .play();
        this.inputDown();
    };
    SoundButton.prototype.setOff = function () {
        this.button.alpha = 0.4;
    };
    SoundButton.prototype.setOn = function () {
        this.button.alpha = 1;
    };
    SoundButton.prototype.destroy = function () {
        this.button.destroy(true);
    };
    SoundButton.prototype.addOnPress = function (inputDown) {
        this.inputDown = inputDown;
    };
    return SoundButton;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SoundButton;
