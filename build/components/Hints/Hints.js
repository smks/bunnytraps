"use strict";
var Model_1 = require("./Model");
var View_1 = require("./View");
var Hints = (function () {
    function Hints(game, options, timeToWait) {
        this.model = new Model_1.default(options);
        this.view = new View_1.default(game, this.model);
        this.view.create(timeToWait);
    }
    Hints.prototype.destroy = function () {
        this.model = null;
        this.view.destroy();
    };
    return Hints;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Hints;
