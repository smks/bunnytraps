"use strict";
var Group = Phaser.Group;
var textfield_factory_1 = require('../../helpers/textfield-factory');
var textfield_factory_2 = require('../../helpers/textfield-factory');
var View = (function () {
    function View(game, model) {
        this.game = game;
        this.model = model;
    }
    View.prototype.create = function (timeToWait) {
        this.group = new Group(this.game);
        var options = this.getTextFieldOptions();
        options.parent = this.group;
        this.hintTextField = textfield_factory_1.default(this.game, options);
        this.group.add(this.hintTextField);
        this.loopTimer = this.game.time.events.loop(timeToWait, this.setRandomHint, this);
    };
    View.prototype.getTextFieldOptions = function () {
        var options = textfield_factory_2.getDefaultTextFieldOptions(this.model.getRandomHint());
        options.xPos = this.model.getX();
        options.yPos = this.model.getY();
        options.font = this.model.getFont();
        options.fontSize = this.model.getFontSize();
        options.align = this.model.getAlign();
        options.anchorX = this.model.getAnchorX();
        options.anchorY = this.model.getAnchorY();
        return options;
    };
    View.prototype.setRandomHint = function () {
        var nextHint = this.model.getRandomHint();
        if (nextHint === this.hintTextField.text) {
            this.game.time.events.remove(this.loopTimer);
        }
        this.hintTextField.text = this.model.getRandomHint();
    };
    View.prototype.destroy = function () {
        this.hintTextField.destroy(true);
        this.game.time.events.remove(this.loopTimer);
        this.group = null;
    };
    return View;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = View;
