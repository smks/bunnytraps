"use strict";
var ArrayUtils = Phaser.ArrayUtils;
var Model = (function () {
    function Model(options) {
        this.x = options.xPos;
        this.y = options.yPos;
        this.font = options.font;
        this.fontSize = options.fontSize;
        this.align = options.align;
        this.anchorX = options.anchorX;
        this.anchorY = options.anchorY;
        this.hints = this.createHints();
        this.currentHint = '';
    }
    Model.prototype.createHints = function () {
        return [
            'This is hint 1',
            'This is hint 2',
            'This is hint 3',
            'This is hint 4',
            'This is hint 5',
            'This is hint 6',
            'This is hint 7',
            'This is hint 8',
            'This is hint 9',
            'This is hint 10',
            'This is hint 11',
            'This is hint 12',
            'This is hint 13',
            'This is hint 14',
            'This is hint 15',
            'This is hint 16',
            'This is hint 17',
            'This is hint 18',
            'This is hint 19',
            'This is hint 20'
        ];
    };
    Model.prototype.getRandomHint = function () {
        if (this.hints.length == 0) {
            return this.currentHint;
        }
        var hint = ArrayUtils.getRandomItem(this.hints, 0, this.hints.length);
        this.hints.splice(this.hints.indexOf(hint), 1);
        return this.currentHint = hint;
    };
    Model.prototype.getX = function () {
        return this.x;
    };
    Model.prototype.getY = function () {
        return this.y;
    };
    Model.prototype.getFont = function () {
        return this.font;
    };
    Model.prototype.getFontSize = function () {
        return this.fontSize;
    };
    Model.prototype.getAlign = function () {
        return this.align;
    };
    Model.prototype.getAnchorX = function () {
        return this.anchorX;
    };
    Model.prototype.getAnchorY = function () {
        return this.anchorY;
    };
    return Model;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Model;
