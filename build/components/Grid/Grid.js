"use strict";
var Assets_1 = require("../../Assets");
var Grid = (function () {
    function Grid(game, pressedTile, placementComplete, animateIn) {
        if (animateIn === void 0) { animateIn = true; }
        this.COLUMNS = 6;
        this.ROWS = 5;
        this.SIZE = 186;
        this.ANIMATE_IN = true;
        this.DELAY_INCREMENT = 150;
        this.delayOfPlacement = 0;
        this.game = game;
        this.pressedTile = pressedTile;
        this.placementComplete = placementComplete;
        this.ANIMATE_IN = animateIn;
        this.tileMap = new Array();
    }
    Grid.prototype.create = function (xPos, yPos) {
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.createColumns();
    };
    Grid.prototype.addInteraction = function () {
        if (this.tileMap.length == 0) {
            throw new Error('Create the grid first before adding interaction!');
        }
        for (var columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (var rowIndex = 0; rowIndex < this.ROWS; rowIndex++) {
                var option = this.tileMap[columnIndex][rowIndex];
                var tile = option.tile;
                this.addTileEvents(tile, { columnIndex: columnIndex, rowIndex: rowIndex });
            }
        }
    };
    Grid.prototype.removeInteraction = function () {
        for (var columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (var tileIndex = 0; tileIndex < this.ROWS; tileIndex++) {
                var option = this.tileMap[columnIndex][tileIndex];
                var tile = option.tile;
                this.removeTileEvents(tile);
            }
        }
    };
    Grid.prototype.shootUpTile = function (columnIndex, rowIndex) {
        var option = this.tileMap[columnIndex][rowIndex];
        var tile = option.tile;
        this.group.bringToTop(tile);
        var tween = this.game.add.tween(tile.scale)
            .to({ x: 1.2, y: 1.2 }, 100, Phaser.Easing.Exponential.Out, false, 1)
            .chain(this.game.add.tween(tile.scale)
            .to({ x: 1, y: 1 }, 100, Phaser.Easing.Exponential.InOut, false, 1));
        tween.start();
    };
    Grid.prototype.addTileEvents = function (tile, params) {
        tile.inputEnabled = true;
        tile.events.onInputOver.add(this.onInputOver, this);
        tile.events.onInputOut.add(this.onInputOut, this);
        tile.events.onInputDown.add(this.onInputDown, this, 0, params);
    };
    Grid.prototype.removeTileEvents = function (tile) {
        tile.inputEnabled = false;
        tile.events.onInputOver.remove(this.onInputOver, this);
        tile.events.onInputOut.remove(this.onInputOut, this);
        tile.events.onInputDown.remove(this.onInputDown, this);
    };
    Grid.prototype.createColumns = function () {
        for (var columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            this.tileMap[columnIndex] = [];
            this.delayOfPlacement += this.DELAY_INCREMENT;
            this.createRow(columnIndex);
        }
    };
    Grid.prototype.createRow = function (columnIndex) {
        for (var rowIndex = 0; rowIndex < this.ROWS; rowIndex++) {
            this.delayOfPlacement += this.DELAY_INCREMENT;
            var tileSprite = this.createTile();
            var xPos = 0;
            var yPos = 0;
            if (columnIndex > 0) {
                xPos = (columnIndex * this.SIZE);
            }
            if (rowIndex > 0) {
                yPos = (rowIndex * this.SIZE);
            }
            tileSprite.x = xPos;
            tileSprite.y = yPos;
            if (this.ANIMATE_IN) {
                this.animateIn(tileSprite);
            }
            this.tileMap[columnIndex][rowIndex] = {
                'tile': tileSprite,
                'isActive': false,
                'isHovered': false
            };
        }
        this.delayOfPlacement -= (this.DELAY_INCREMENT * this.ROWS);
    };
    Grid.prototype.animateIn = function (tileSprite) {
        tileSprite.alpha = 0;
        tileSprite.scale.set(1.5, 1.5);
        this.game.add.tween(tileSprite.scale)
            .to({ x: 1, y: 1 }, 500, Phaser.Easing.Circular.InOut, true, this.delayOfPlacement);
        this.game.add.tween(tileSprite)
            .to({ alpha: 1 }, 250, Phaser.Easing.Circular.InOut, true, this.delayOfPlacement);
    };
    Grid.prototype.createTile = function () {
        var tile = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_GRID_TILE, this.group);
        tile.anchor.set(0.5, 0.5);
        return tile;
    };
    Grid.prototype.onInputOut = function (tile) {
        tile.scale.set(1, 1);
    };
    Grid.prototype.onInputOver = function (tile) {
        this.group.bringToTop(tile);
        this.game.tweens.remove(this.tileTween);
        this.tileTween = this.game.add.tween(tile.scale).to({ x: 1.1, y: 1.1 }, 500, Phaser.Easing.Exponential.Out, true);
    };
    Grid.prototype.onInputDown = function (e) {
        var tile = e;
        var args = arguments[2];
        this.game.tweens.remove(this.tileTween);
        tile.scale.set(1, 1);
        this.pressedTile.dispatch({
            columnIndex: args.columnIndex,
            rowIndex: args.rowIndex,
            tileXPos: this.group.x + tile.x,
            tileYPos: this.group.y + tile.y
        });
    };
    Grid.prototype.destroy = function () {
        for (var columnIndex = 0; columnIndex < this.COLUMNS; columnIndex++) {
            for (var tileIndex = 0; tileIndex < this.ROWS; tileIndex++) {
                var option = this.tileMap[columnIndex][tileIndex];
                var tile = option.tile;
                this.removeTileEvents(tile);
                tile.destroy(true);
            }
        }
    };
    Grid.prototype.getCoordinatesByIndex = function (columnIndex, rowIndex) {
        var option = this.tileMap[columnIndex][rowIndex];
        var tile = option.tile;
        return {
            tileXPos: this.group.x + tile.x,
            tileYPos: this.group.y + tile.y
        };
    };
    return Grid;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Grid;
