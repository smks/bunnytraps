"use strict";
var Assets_1 = require("../../Assets");
var textfield_factory_1 = require("../../helpers/textfield-factory");
var textfield_factory_2 = require('./../../helpers/textfield-factory');
var CountdownTimer = (function () {
    function CountdownTimer(game, minutesToCountFrom, secondsToCountFrom) {
        if (minutesToCountFrom === void 0) { minutesToCountFrom = 1; }
        if (secondsToCountFrom === void 0) { secondsToCountFrom = 30; }
        this.triggeredFinalSecondsMusic = false;
        this.game = game;
        this.minutesToCountFrom = minutesToCountFrom;
        this.secondsToCountFrom = secondsToCountFrom;
        this.currentCount = 0;
    }
    CountdownTimer.prototype.create = function (xPos, yPos, autoRun) {
        var _this = this;
        if (autoRun === void 0) { autoRun = false; }
        this.group = this.game.add.group();
        this.group.x = xPos;
        this.group.y = yPos;
        this.bar = this.game.add.sprite(0, 0, Assets_1.default.GAME_ASSETS, Assets_1.default.GRAPHIC_GAME_TIMER_INDICATOR, this.group);
        this.bar.anchor.set(0.5, 0.5);
        var options = textfield_factory_1.getDefaultTextFieldOptions('00:00');
        options.fontSize = 44;
        options.parent = this.group;
        this.textField = textfield_factory_2.default(this.game, options);
        this.textField.x = this.bar.x;
        this.textField.y = this.bar.y - 10;
        this.timer = this.game.time.create();
        this.timerEvent = this.timer.add(Phaser.Timer.MINUTE * this.minutesToCountFrom + Phaser.Timer.SECOND * this.secondsToCountFrom, function () {
            _this.timer.stop();
            _this.textField.text = '00:00';
        }, this);
        if (autoRun) {
            this.start();
        }
        this.finalSecondsTriggered = new Phaser.Signal();
        this.finalSecondIterationTriggered = new Phaser.Signal();
    };
    CountdownTimer.prototype.addFinalSecondsCallback = function (callback) {
        this.finalSecondsTriggered.add(callback);
    };
    CountdownTimer.prototype.addFinalSecondIteration = function (callback) {
        this.finalSecondIterationTriggered.add(callback);
    };
    CountdownTimer.prototype.start = function () {
        if (this.timer.running) {
            return;
        }
        this.timer.start();
    };
    CountdownTimer.prototype.stop = function (clearEvents) {
        if (clearEvents === void 0) { clearEvents = false; }
        this.timer.stop(clearEvents);
    };
    CountdownTimer.prototype.formatTime = function (s) {
        var minutes = '0' + Math.floor(s / 60);
        var seconds = '0' + (s - minutes * 60);
        return minutes.substr(-2) + ':' + seconds.substr(-2);
    };
    CountdownTimer.prototype.render = function () {
        if (this.timer.running) {
            var timeMs = this.timerEvent.delay - this.timer.ms;
            var time = Math.round(timeMs / 1000);
            if (this.triggeredFinalSecondsMusic === false && time === 10) {
                this.triggeredFinalSecondsMusic = true;
                this.onFinalSeconds();
            }
            var diff = timeMs % 1000;
            if (time === 0) {
                this.onFinalSecondIteration();
            }
            var formattedTime = this.formatTime(time);
            this.textField.text = formattedTime;
        }
    };
    CountdownTimer.prototype.onFinalSeconds = function () {
        this.countdownMusic = this.game.add.audio(Assets_1.default.AUDIO_KEY_AUDIO_CLOCK_TICK, 1, false);
        this.countdownMusic.play();
        this.finalSecondsTriggered.dispatch();
    };
    CountdownTimer.prototype.stopMusic = function () {
        if (this.countdownMusic) {
            this.countdownMusic.stop();
        }
    };
    CountdownTimer.prototype.onFinalSecondIteration = function () {
        this.finalSecondIterationTriggered.dispatch();
    };
    CountdownTimer.prototype.destroy = function () {
        this.timer.stop();
        this.timer.destroy();
        this.bar.destroy(true);
        this.finalSecondsTriggered.dispose();
        this.finalSecondIterationTriggered.dispose();
    };
    return CountdownTimer;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = CountdownTimer;
