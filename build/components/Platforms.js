"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Platforms = (function (_super) {
    __extends(Platforms, _super);
    function Platforms(game, save) {
        _super.call(this, game);
        this.COLUMNS = 6;
        this.ROWS = 5;
        this.SIZE = 185;
        this.GUTTER = 20;
        this.tileMap = [];
        this.bunnyPlacements = [];
        this.trapPlacements = [];
        this.placementsMade = 0;
        this.save = save;
    }
    Platforms.prototype.initPrepare = function (placedOnTile) {
        this.render();
        this.placedOnTile = placedOnTile;
        this.makeInteractive();
        this.placedOnTile.add(this.storeResult, this);
        window['tm'] = this.tileMap;
    };
    Platforms.prototype.initPlay = function () {
        this.render();
    };
    Platforms.prototype.render = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            this.tileMap[c] = [];
            this.createRow(c);
        }
    };
    Platforms.prototype.createRow = function (c) {
        for (var tile = 0; tile < this.ROWS; tile++) {
            var tileSprite = this.createTile();
            var xPos = 0;
            var yPos = 0;
            if (c > 0) {
                xPos = (c * (this.SIZE + this.GUTTER));
            }
            if (tile > 0) {
                yPos = (tile * (this.SIZE + (this.GUTTER / 6)));
            }
            tileSprite.x = xPos;
            tileSprite.y = yPos;
            this.tileMap[c][tile] = {
                'tile': tileSprite,
                'bunny': null,
                'trap': null,
                'active': false,
                'overlayTile': null
            };
        }
    };
    Platforms.prototype.createTile = function () {
        var tile = this.game.add.graphics(this.SIZE, this.SIZE, this);
        tile.lineStyle(1, 0xFF613c42);
        tile.beginFill(0xFF88545c);
        tile.drawRect(0, 0, this.SIZE, (this.SIZE * 0.9));
        return tile;
    };
    Platforms.prototype.makeInteractive = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            for (var t = 0; t < this.ROWS; t++) {
                var option = this.tileMap[c][t];
                var tile = option.tile;
                tile.alpha = 0.5;
                tile.inputEnabled = true;
                tile.events.onInputOver.add(this.onInputOver, this);
                tile.events.onInputOut.add(this.onInputOut, this);
                tile.events.onInputDown.add(this.onInputDown, this, 0, { col: c, tile: t });
            }
        }
    };
    Platforms.prototype.onInputOut = function (e) {
        e.alpha = 0.5;
    };
    Platforms.prototype.onInputOver = function (e) {
        e.alpha = 1;
    };
    Platforms.prototype.onInputDown = function (tile) {
        var args = arguments;
        var decision = args[2];
        tile.alpha = 0.5;
        tile.events.onInputOver.remove(this.onInputOver, this);
        tile.events.onInputOut.remove(this.onInputOut, this);
        if (this.placementsMade < 10) {
            this.placementsMade++;
            this.madeChoice(decision.col, decision.tile);
        }
    };
    Platforms.prototype.madeChoice = function (columnIndex, rowIndex) {
        if (this.placementsMade > 5) {
            this.addTrap(columnIndex, rowIndex);
        }
        else {
            this.addBunny(columnIndex, rowIndex);
        }
        if (this.placementsMade == 6) {
            this.hideBunnies();
        }
        if (this.placementsMade == 10) {
            this.hideTraps();
        }
        this.placedOnTile.dispatch({
            bunnies: this.bunnyPlacements,
            traps: this.trapPlacements,
            placementsMade: this.placementsMade
        });
    };
    Platforms.prototype.addTrap = function (col, row) {
        if (this.trapPlacements.length > 5) {
            throw Error('Only 5 traps should be set!');
        }
        this.trapPlacements.push({
            col: col,
            row: row
        });
        var indexedObject = this.tileMap[col][row];
        var tileLocation = indexedObject.tile;
        var trap = this.game.add.sprite(tileLocation.x, tileLocation.y, 'sprites', 'trap-1.png', this);
        this.setChildIndex(trap, 30);
        indexedObject.active = true;
        indexedObject.trap = trap;
    };
    Platforms.prototype.addBunny = function (col, row) {
        if (this.bunnyPlacements.length > 5) {
            throw Error('Only 5 traps should be set!');
        }
        this.bunnyPlacements.push({
            col: col,
            row: row
        });
        var indexedObject = this.tileMap[col][row];
        var tileLocation = indexedObject.tile;
        var bunny = this.game.add.sprite(tileLocation.x, tileLocation.y, 'sprites', 'bunny-1.png', this);
        bunny.anchor.set(0, 0.4);
        indexedObject.bunny = bunny;
        indexedObject.active = true;
        this.bounceDropBunny(bunny);
    };
    Platforms.prototype.bounceDropBunny = function (bunny) {
        var bounce = this.game.add.tween(bunny);
        var yStartPos = bunny.y - 100;
        var yFinishPos = bunny.y;
        bunny.y = yStartPos;
        bounce.to({ y: yFinishPos }, 500, Phaser.Easing.Elastic.Out);
        bounce.start();
    };
    Platforms.prototype.bounceDropTile = function (overlayTile, delay) {
        var drop = this.game.add.tween(overlayTile);
        var yStartPos = overlayTile.y - (overlayTile.height + this.game.height);
        var yFinishPos = overlayTile.y;
        overlayTile.y = yStartPos;
        drop.to({ y: yFinishPos }, delay, Phaser.Easing.Cubic.Out);
        drop.start();
    };
    Platforms.prototype.shutdown = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            for (var t = 0; t < this.ROWS; t++) {
                var option = this.tileMap[c][t];
                var tile = option.tile;
                var bunny = option.bunny;
                tile.inputEnabled = false;
                tile.events.onInputOver.remove(this.onInputOver, this);
                tile.events.onInputOut.remove(this.onInputOut, this);
                tile.events.onInputDown.remove(this.onInputDown, this);
                this.remove(bunny);
                this.tileMap[c][t] = null;
            }
        }
    };
    Platforms.prototype.disableInputs = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            for (var t = 0; t < this.ROWS; t++) {
                var option = this.tileMap[c][t];
                var tile = option.tile;
                tile.events.onInputOver.remove(this.onInputOver, this);
                tile.events.onInputOut.remove(this.onInputOut, this);
                tile.events.onInputDown.remove(this.onInputDown, this);
            }
        }
    };
    Platforms.prototype.hideBunnies = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            for (var t = 0; t < this.ROWS; t++) {
                var option = this.tileMap[c][t];
                var bunny = option.bunny;
                if (bunny) {
                    this.remove(bunny, true);
                }
            }
        }
    };
    Platforms.prototype.hideTraps = function () {
        for (var c = 0; c < this.COLUMNS; c++) {
            for (var t = 0; t < this.ROWS; t++) {
                var option = this.tileMap[c][t];
                var trap = option.trap;
                if (trap) {
                    this.remove(trap, true);
                }
            }
        }
    };
    Platforms.prototype.storeResult = function (options) {
        if (options == undefined || options.placementsMade < 10) {
            return;
        }
        var decisions = {
            bunnies: options.bunnies,
            traps: options.traps
        };
        this.save.storeDecisions('player1', decisions);
    };
    Platforms.prototype.showNextResult = function (playersTurn) {
    };
    Platforms.TYPE_PREPARE = 1;
    Platforms.TYPE_PLAY = 2;
    return Platforms;
}(Phaser.Group));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Platforms;
