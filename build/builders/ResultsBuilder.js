"use strict";
var bunny_id_by_player_1 = require('../helpers/bunny-id-by-player');
var ResultsMoveBuilder = (function () {
    function ResultsMoveBuilder(results) {
        this.numberOfPlayers = 2;
        this.currentPlayerIndex = 1;
        this.results = results;
        this.player1Results = results.player1Results;
        this.player2Results = results.player2Results;
        if (results.player3Results) {
            this.numberOfPlayers++;
            this.player3Results = results.player3Results;
        }
        if (results.player4Results) {
            this.numberOfPlayers++;
            this.player4Results = results.player4Results;
        }
        this.moves = [];
    }
    ResultsMoveBuilder.prototype.createMoves = function () {
        var placements = this.results[("player" + this.numberOfPlayers + "Results")].bunnyPlacements;
        var lengthOfResults = placements.length;
        while (placements.length > 0) {
            for (var i = 0; i < lengthOfResults; i++) {
                if (this.currentPlayerIndex > this.numberOfPlayers) {
                    this.currentPlayerIndex = 1;
                }
                var bunnyPositions = this[("player" + this.currentPlayerIndex + "Results")].bunnyPlacements;
                if (bunnyPositions.length == 0) {
                    continue;
                }
                var bunnyPosition = bunnyPositions.shift();
                var whoTrappedBunny = [];
                if (bunnyPosition) {
                    whoTrappedBunny = this.findPlayersWhoPlacedTrap(bunnyPosition);
                }
                var move = {
                    playerId: this.currentPlayerIndex,
                    bunnyId: bunny_id_by_player_1.default(this.currentPlayerIndex),
                    columnIndex: bunnyPosition.columnIndex,
                    rowIndex: bunnyPosition.rowIndex,
                    gotHitByPlayers: whoTrappedBunny
                };
                this.moves.push(move);
                this.currentPlayerIndex++;
            }
        }
        console.log(placements.length);
        console.log(this.moves);
    };
    ResultsMoveBuilder.prototype.getMoves = function () {
        return this.moves;
    };
    ResultsMoveBuilder.prototype.findPlayersWhoPlacedTrap = function (bunnyPosition) {
        var playersWhoTrappedBunny = [];
        var _loop_1 = function(key) {
            if (!this_1.results.hasOwnProperty(key)) {
                return "continue";
            }
            var playerDecisions = this_1.results[key];
            var playerId = playerDecisions.playerId;
            var trapPlacements = playerDecisions.trapPlacements;
            trapPlacements.map(function (trapPosition) {
                if (trapPosition.columnIndex == bunnyPosition.columnIndex &&
                    trapPosition.rowIndex == bunnyPosition.rowIndex) {
                    playersWhoTrappedBunny.push(playerId);
                }
            });
        };
        var this_1 = this;
        for (var key in this.results) {
            _loop_1(key);
        }
        console.log("Bunny Position: col - " + bunnyPosition.columnIndex + " row - " + bunnyPosition.rowIndex);
        console.log('playersWhoTrappedBunny');
        console.log(playersWhoTrappedBunny);
        return playersWhoTrappedBunny;
    };
    return ResultsMoveBuilder;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ResultsMoveBuilder;
