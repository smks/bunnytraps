let gulp = require('gulp'),
    ts = require('gulp-typescript'),
    tap = require('gulp-tap'),
    sass = require('gulp-sass'),
    path = require('path'),
    htmlmin = require('gulp-html-minifier'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    flatten = require('gulp-flatten'),
    browserify = require('browserify'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    del = require('del'),
    imageResize = require('gulp-image-resize'),
    util = require('gulp-util');
buffer = require('gulp-buffer');

var tsProject = ts.createProject('./tsconfig.json');

gulp.task('default', [
    'minify-html',
    'sass',
    'copy-libs',
    'copy-assets',
    'copy-icon',
    'scripts'
]);

gulp.task('watch', ['scripts'], function () {
    gulp.watch('src/typescript/**/*.ts', ['scripts']);
  });

gulp.task('minify-html', function () {
    return gulp.src('./src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('./dist'));
  });

gulp.task('sass', function () {
    return gulp.src('./src/sass/*.scss')
        .pipe(sass.sync()
            .on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'));
  });

gulp.task('copy-libs', function () {
    return gulp.src(['node_modules/phaser/build/phaser.min.js'])
        .pipe(flatten())
        .pipe(gulp.dest('dist/js'));
  });

gulp.task('copy-assets', function () {
    return gulp.src('src/assets/**/*')
        .pipe(gulp.dest('dist/assets'))
  });

gulp.task('copy-icon', function () {
    return gulp.src('src/icon/**/*')
        .pipe(gulp.dest('dist/icon'));
});

gulp.task('add-demo', function () {
    return gulp.src('dist/**/*')
        .pipe(gulp.dest('./../bunnytrap-demo'));
  });

gulp.task('scripts', function () {

    var tsResult = gulp.src('src/typescript/**/*') // or tsProject.src()
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('./build/'))
        .pipe(concat('game.js'))

        .pipe(tap(function (file) {
            file.contents = browserify(file.path, { debug: true }).bundle();
          }))

        .pipe(buffer())

        .pipe(sourcemaps.init({ loadMaps: true }))

        .pipe(uglify())

        .pipe(sourcemaps.write('./'))

        .pipe(gulp.dest('dist/js'));
  });

let srcRenaming = {
    './src/assets/original-images/bunnies/{id}/body_*': 'body',
    './src/assets/original-images/bunnies/{id}/ears*': 'ear',
    './src/assets/original-images/bunnies/{id}/eyes01*': 'eyes-open',
    './src/assets/original-images/bunnies/{id}/eyes02*': 'eyes-happy',
    './src/assets/original-images/bunnies/{id}/eyes03*': 'eyes-closed',
    './src/assets/original-images/bunnies/{id}/eyes04*': 'eyes-sad',
    './src/assets/original-images/bunnies/{id}/eyes06*': 'eyes-worry',
    './src/assets/original-images/bunnies/{id}/eyes_hit*': 'eyes-hit',
    './src/assets/original-images/bunnies/{id}/mouth.png': 'mouth-open',
    './src/assets/original-images/bunnies/{id}/mouthclosed*': 'mouth-closed',
    './src/assets/original-images/bunnies/{id}/mouth_hit2*': 'mouth-hit-2.',
    './src/assets/original-images/bunnies/{id}/mouth_hit.*': 'mouth-hit.',
    './src/assets/original-images/bunnies/{id}/mouth_jijay1*': 'mouth-fear-1',
    './src/assets/original-images/bunnies/{id}/mouth_jijay2*': 'mouth-fear-2',
    './src/assets/original-images/bunnies/{id}/mouthpanic.*': 'mouth-panic.',
    './src/assets/original-images/bunnies/{id}/mouthpanicclosed*': 'mouth-panic-closed',
    './src/assets/original-images/bunnies/{id}/hand*': 'paw',
    './src/assets/original-images/bunnies/{id}/legs*': 'foot',
    './src/assets/original-images/bunnies/{id}/eyebrow*': 'brow',
    './src/assets/original-images/bunnies/{id}/cheek*': 'cheek',
    './src/assets/original-images/bunnies/{id}/face*': 'face',
  };

let newFiles = [
    './src/assets/original-images/bunnies/{id}/body.png',
    './src/assets/original-images/bunnies/{id}/ear.png',
    './src/assets/original-images/bunnies/{id}/eyes-open.png',
    './src/assets/original-images/bunnies/{id}/eyes-happy.png',
    './src/assets/original-images/bunnies/{id}/eyes-closed.png',
    './src/assets/original-images/bunnies/{id}/mouth-open.png',
    './src/assets/original-images/bunnies/{id}/mouth-closed.png',
    './src/assets/original-images/bunnies/{id}/mouth-panic.png',
    './src/assets/original-images/bunnies/{id}/mouth-panic-closed.png',
    './src/assets/original-images/bunnies/{id}/paw.png',
    './src/assets/original-images/bunnies/{id}/foot.png',
    './src/assets/original-images/bunnies/{id}/brow.png',
    './src/assets/original-images/bunnies/{id}/cheek.png',
    './src/assets/original-images/bunnies/{id}/face.png',
];

gulp.task('bunny', ['bunny-move-files', 'bunny-rename-files']);

gulp.task('bunny-rename-files', function () {

    let index = 1;
    const bunnies = 8;

    while (index <= bunnies) {

      for (var key in srcRenaming) {
        // skip loop if the property is from prototype
        if (!srcRenaming.hasOwnProperty(key)) continue;

        const fileToFind = key.replace('{id}', index);
        const fileToBeRenamedTo = srcRenaming[key];

        util.log(`Replacing ${fileToFind} for ${fileToBeRenamedTo}`);

        const destination = './src/assets/single/bunnies/{id}'.replace('{id}', index);

        gulp.src(fileToFind)
            .pipe(rename(function (path) {
                path.basename = fileToBeRenamedTo;
              }))
            .pipe(gulp.dest(destination))
            .on('end', function () {
                const file = destination + '/' + fileToBeRenamedTo + '.png';

                gulp.src(file)
                    .pipe(imageResize({
                        width: '25%', // a further 25% reduced as sprite grid
                        height: '25%',
                        crop: false,
                        upscale: false,
                      }))
                    .pipe(gulp.dest(destination))
                    .on('end', function () {
                        'use strict';
                        util.log(`Re-sized ${file}`);
                      });
              });
      }

      index++;
    }

  });

gulp.task('bunny-move-files', function () {

    let index = 1;
    const bunnies = 8;

    while (index <= bunnies) {

      newFiles.map((file) => {
          'use strict';

          const originalFile = file.replace('{id}', index);
          const newDestination = originalFile.substr(0, originalFile.indexOf((index) + '/') + 1).replace('original-images', 'single');

          util.log(`Moving ${originalFile} to ${newDestination}`);

          gulp.src(file.replace('{id}', index))
              .pipe(gulp.dest(newDestination));
        });

      index++;
    }

  });
